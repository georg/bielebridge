cmake_minimum_required(VERSION 3.18)
project(bielebridge LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_custom_target(version
  ${CMAKE_COMMAND} -D SRC=${CMAKE_SOURCE_DIR}/src/version.cmake.hpp.in
                   -D DST=${CMAKE_SOURCE_DIR}/src/version.cmake.hpp
                   -P ${CMAKE_SOURCE_DIR}/GenerateVersion.cmake
)

find_package(OpenGL REQUIRED)
find_package(PkgConfig REQUIRED)
pkg_check_modules(SDL2 REQUIRED sdl2 SDL2_ttf SDL2_gfx SDL2_image)
find_package(Lua REQUIRED)
include_directories(${SDL2_INCLUDE_DIRS} ${LUA_INCLUDE_DIR})

add_executable(${PROJECT_NAME}
    src/benchmark.cpp
    src/game.cpp
    src/gui_button.cpp
    src/gui_component.cpp
    src/gui_editor.cpp
    src/gui_editorsimulator.cpp
    src/gui_editor_undomanager.cpp
    src/gui_manager.cpp
    src/gui_menu_layouts.cpp
    src/gui_simulator.cpp
    src/hash.cpp
    src/level.cpp
    src/main.cpp
    src/math_utils.cpp
    src/particle.cpp
    src/physics.cpp
    src/physics_test.cpp
    src/sdl_draw.cpp
    src/sdl_utils.cpp
    src/settings.cpp
    src/spring.cpp
    src/train.cpp
)
add_dependencies(${PROJECT_NAME} version)

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/src/configure.cmake.hpp.in
	${CMAKE_CURRENT_SOURCE_DIR}/src/configure.cmake.hpp)

target_link_libraries(bielebridge ${LUA_LIBRARY} ${OPENGL_LIBRARIES} ${SDL2_LIBRARIES} )
target_compile_options(bielebridge PRIVATE -O2 -g -Wall -Wextra)

