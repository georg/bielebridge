//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <SDL2/SDL.h>
#include <lua.hpp>
#include <lualib.h>
#include <lauxlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <fstream>
#include <iostream>

#include "settings.hpp"
#include "error.hpp"
#include "hash.hpp"
#include "configure.cmake.hpp"

Settings::Settings()
{
	// init default (if no file is found)
	for(int i=0; i<= PREDEFINED_LEVELS ; i++)
	{
		levelstars[i] = 0;
		levelpassed[i] = false;
	}
	// search for /usr/share and home folder
	this->init_paths();
	this->find_bgimage();


	// check hash
	FILE* file = fopen(full_path_to_config_file.c_str(), "r+");
	if( ! file )
	{
		ERROR_PRINT("Error: Can not open file %s to load checksum!", full_path_to_config_file.c_str());
		return;
	}

	// get file size
	fseek(file, 0L, SEEK_END);
	long int file_size = ftell(file);
	if (file_size == -1L)
    {
		ERROR_PRINT("ftell() failed for file %s\n", full_path_to_config_file.c_str());
		fclose(file);
		return;
    }
	uint8_t* file_buffer;
	file_buffer = new uint8_t[file_size];

	// seek back to the beginning:
	fseek(file, 0L, SEEK_SET);
	// copy file to buffer
	std::string hash_value_read;
	unsigned char ch;
	size_t count = 0;
	while(fread(&ch, 1, 1, file) == 1)
	{
		if(count >= 3 && count < (64+3) && file_buffer[0] == '-' && file_buffer[1] == '-' && file_buffer[2] == ' ')
		{
			file_buffer[count] = '0';
			hash_value_read = hash_value_read + char(ch);
		}
		else
		{
			file_buffer[count] = ch;
		}
		count++;
	}

	// hash file buffer
	Hash* hash = new Hash();
	int hash_return_value;
	uint8_t file_hash[32];
	size_t data_bit_len = (size_t) file_size * 8;
	hash_return_value = (int) hash->Hash_256(256, (const uint8_t*) file_buffer, data_bit_len, (uint8_t*) file_hash);
	if(hash_return_value != SKEIN_SUCCESS)
	{
		ERROR_PRINT("hash function error: returned %d\n", hash_return_value);
		delete file_buffer;
		return;
	}

	// convert hash to string
	std::string file_hash_str;
	char buf[5];
	for (int i=0;i < 32;i++)
	{
		snprintf(&buf[0], 5, "%02x", file_hash[i]);
		file_hash_str = file_hash_str + buf;
	}
	if(file_hash_str.compare(hash_value_read))
	{
		ERROR_PRINT("%s\n","Config file hash mismatch; load defaults.");
		delete file_buffer;
		return;
	}
	delete file_buffer;
	///////////////////////////////////////////////////////////////////////


	// load lua config file ...
	struct lua_State* L = luaL_newstate();
	if(luaL_dofile(L, full_path_to_config_file.c_str()))
	{
		ERROR_PRINT("luaL_dofile: %s\n", lua_tostring(L, -1));
		lua_close(L);
		return;
	}
	if( ! lua_istable(L, -1))
	{
		ERROR_PRINT("Table expected, got `%s`\n", lua_typename(L, lua_type(L, -1)));
		lua_close(L);
		return;
	}

	lua_getfield(L, -1, "window_width");
	int window_width_from_file = (int) lua_tointeger(L, -1);
	if(window_width_from_file < 0 || window_width_from_file > 7680*8) // 8K-UHD * 8
		ERROR_PRINT("window width from file '%s' is negative or sure too big. Got %d pixels.\n", full_path_to_config_file.c_str(), window_width_from_file );
	else
		window_width = window_width_from_file;
	lua_pop(L, 1);

	lua_getfield(L, -1, "window_height");
	int window_height_from_file = (int) lua_tointeger(L, -1);
	if(window_height_from_file < 0 || window_height_from_file > 4320*8) // 8K-UHD * 8
		ERROR_PRINT("window height from file '%s' is negative or sure too big. Got %d pixels.\n", full_path_to_config_file.c_str(), window_height_from_file );
	else
		window_height = window_height_from_file;
	lua_pop(L, 1);

	lua_getfield(L, -1, "sdl_fullscreen");
	int sdl_fullscreen_from_file = (int) lua_tointeger(L, -1);
	if(sdl_fullscreen_from_file < 0 || sdl_fullscreen_from_file > 1)
		ERROR_PRINT("sdl_fullscreen from file '%s' not as expected. Got value %d.\n", full_path_to_config_file.c_str(), sdl_fullscreen_from_file );
	else
		sdl_fullscreen = (bool) sdl_fullscreen_from_file;
	lua_pop(L, 1);

	lua_getfield(L, -1, "fps_target");
	int fps_target_from_file = (int) lua_tointeger(L, -1);
	if(fps_target_from_file < 0 || fps_target_from_file > 255)
		ERROR_PRINT("fps_target from file '%s' is negative or sure too big. Got value %d.\n", full_path_to_config_file.c_str(), fps_target_from_file );
	else
		fps_target = (uint8_t) fps_target_from_file;
	lua_pop(L, 1);

	int num;
#define FOR_ALL_ITEMS_IN(table) \
	lua_getfield(L, -1, # table);\
	num = 1;\
	lua_pushnumber(L, num);\
	lua_gettable(L, -2);\
	while(lua_type(L, -1))

	// get all particles
	FOR_ALL_ITEMS_IN(levels_done)
	{
		int levelid=0, passed=0, stars=0;
		for( int item=1; item <=3 ; ++item)
		{
			lua_pushnumber(L, item);
			lua_gettable(L, -2);
			switch(item)
			{
				case 1: levelid = (int) lua_tointeger(L, -1); break;
				case 2: passed  = (int) lua_tointeger(L, -1); break;
				case 3: stars = (int) lua_tointeger(L, -1); break;
			}
			lua_pop(L, 1);
		}
		// some checks
		bool ignore_line = false;
		if(levelid < 0 || levelid > PREDEFINED_LEVELS)
		{
			ERROR_PRINT("level id %d is out of bounds; will ignore this line\n",levelid);
			ignore_line = true;
		}
		if(passed != 0 && passed != 1)
		{
			ERROR_PRINT("passed %d is out of bounds; will ignore this line\n",levelid);
			ignore_line = true;
		}
		if(stars < 0 || stars > 3)
		{
			ERROR_PRINT("stars %d is out of bounds; will ignore this line\n",levelid);
			ignore_line = true;
		}
		if(ignore_line == false)
		{
			levelstars[levelid] = stars;
			if(passed == 1)
				set_level_passed(levelid);
		}
		lua_pop(L, 1);

		lua_pushnumber(L, ++num);
		lua_gettable(L, -2);
	}
	lua_pop(L, 2);

	lua_close(L);
}

// Singleton for global settings
Settings* Settings::instance =0;

Settings* Settings::singleton()
{
	if(instance == 0)
		return (instance = new Settings());
	else
		return instance;
}

void Settings::init_paths()
{
	// find ttf font
	std::string test_path = DATADIR "/"+ font_file;
	if(access(test_path.c_str(), F_OK) != -1 )
	{
		full_path_to_font_file = test_path;
	}
	else if(access(("data/"+ font_file).c_str(), F_OK) != -1 )
	{
		full_path_to_font_file = "data/"+ font_file;
	}
	else if(access(("../data/"+ font_file).c_str(), F_OK) != -1 )
	{
		full_path_to_font_file = "../data/"+ font_file;
	}
	else if(access(("/usr/share/fonts/dejavu/" + font_file).c_str(), F_OK) != -1 )
	{
		full_path_to_font_file = "/usr/share/fonts/dejavu/" + font_file;
	}
	else if(access(("/usr/share/fonts/truetype/dejavu/" + font_file).c_str(), F_OK) != -1 )
	{
		full_path_to_font_file = "/usr/share/fonts/truetype/dejavu/" + font_file;
	}
	else
	{
		ERROR_PRINT("no ttf font file found :-(\n%s\n%s\n%s\n", full_path_to_config_file.c_str(),
				("resources/"+ font_file).c_str(),
				("/usr/share/fonts/dejavu/" + font_file).c_str());
		SDL_Event sdlevent;
        sdlevent.type = SDL_QUIT;
        SDL_PushEvent(&sdlevent);
	}

	// get home folder
	const char* home = "HOME";
	char *user_home = getenv(home);
	if (!user_home)
	{
		INFO_PRINT("No path $%s; load / save local.\n",home);
		return;
	}

	std::string game_home(user_home);
	game_home.append("/" + game_folder_name);
	// game home folder exists?
	if( access( game_home.c_str(), F_OK ) != -1 )
	{
		game_home_path = game_home;
		// path ok, set full name of config file
		full_path_to_config_file = game_home + "/" + config_filename;
		return;
	}

	// else
	INFO_PRINT("game folder \"%s\" not found; create it\n", game_home.c_str());

	// create game home dir
	if(mkdir(game_home.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
	{
		ERROR_PRINT("mkdir failed to create \"%s\"; load / save local.\n", game_home.c_str());
		game_home = ".";
	}
	else
	{
		full_path_to_config_file = game_home + "/" + config_filename;
		// create levels dir
		if(mkdir((game_home + "/" + dirname_levels).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
		{
			ERROR_PRINT("mkdir failed to create \"%s\";\n", game_home.c_str());
		}
		else
		{
			// copy empty levels
			DIR *dir;
			struct dirent *ent;
			std::string data_dir(DATADIR);
			int copy_file_count = 0;
			bool dir_found = false;
			if((dir = opendir(data_dir.c_str())) == NULL)
			{
				// could not open directory
				ERROR_PRINT("could not open dir %s\n", data_dir.c_str());

				// try current dir (.)
				data_dir = "./" + dirname_levels;
				if((dir = opendir(data_dir.c_str())) == NULL)
				{
					ERROR_PRINT("could not open dir %s\n", data_dir.c_str());
					// try current dir with data (.)
					data_dir = "./data/";
					if((dir = opendir(data_dir.c_str())) == NULL)
					{
						ERROR_PRINT("could not open dir %s\n", data_dir.c_str());
					}
					else
						dir_found = true;
				}
				else
					dir_found = true;
			}
			else
				dir_found = true;

			if(dir_found)
			{
				/* print all the files and directories within directory */
				while ((ent = readdir(dir)) != NULL) {
					const std::string filename = std::string(ent->d_name);
					const std::string f_ext = fname_ext_level;
					// copy only files with proper filename extension
					if((filename.length() > f_ext.length() && filename.compare(filename.length() - f_ext.length(), f_ext.length(), f_ext) == 0) ||
							filename.compare("benchmark.ben") == 0 )
					{
						//copy file
						std::ifstream src((data_dir +"/"+ filename).c_str(), std::ios::binary);
						std::ofstream dst(game_home +"/"+ dirname_levels+"/"+filename, std::ios::binary);
						dst << src.rdbuf();
						copy_file_count++;
					}
					// copy background image
					if(filename.compare(Settings::singleton()->fname_bgimage) == 0 )
					{
						//copy file
						std::ifstream src((data_dir +"/"+ filename).c_str(), std::ios::binary);
						std::ofstream dst(game_home +"/"+ filename, std::ios::binary);
						dst << src.rdbuf();
					}
				}
				closedir(dir);
				INFO_PRINT("copied %d levels\n",copy_file_count);
			}
		}
	}
	game_home_path = game_home;
}

void Settings::find_bgimage()
{
	// find background image
	std::string test_path = DATADIR "/"+ fname_bgimage;
	if(access(test_path.c_str(), F_OK) != -1 )
	{
		full_path_to_bgimage_file = test_path;
	}
	else if(access(("data/"+ fname_bgimage).c_str(), F_OK) != -1 )
	{
		full_path_to_bgimage_file = "data/"+ fname_bgimage;
	}
	else if(access(("../data/"+ fname_bgimage).c_str(), F_OK) != -1 )
	{
		full_path_to_bgimage_file = "../data/"+ fname_bgimage;
	}
	else
	{
		// get home folder
		const char* home = "HOME";
		char *user_home = getenv(home);
		if (user_home)
		{
			std::string game_home(user_home);
			game_home.append("/" + game_folder_name);
			// game home folder exists?
			if( access( game_home.c_str(), F_OK ) != -1 )
			{
				game_home_path = game_home;
				// path ok, set full name of config file
				full_path_to_bgimage_file = game_home + "/" + fname_bgimage;
			}
		}
	}
}

int Settings::save(){
	// save lua config file ...
	FILE* file = fopen(full_path_to_config_file.c_str(), "w");
	if( ! file )
	{
		ERROR_PRINT("Can not open file %s to save config!", full_path_to_config_file.c_str());
		return 1;
	}
	// place for checksum
	fprintf(file, "-- 0000000000000000000000000000000000000000000000000000000000000000\n");

	fprintf(file, "-- this file will be parsed by the Lua interpreter,\n-- so I do not have to care about this ;-)\n\n");
	fprintf(file, "return{\n");
	fprintf(file, "\twindow_width=%d,\n",window_width);
	fprintf(file, "\twindow_height=%d,\n",window_height);
	fprintf(file, "\tsdl_fullscreen=%d,\n",sdl_fullscreen ? 1 : 0);
	fprintf(file, "\tfps_target=%d,\n",fps_target);

	// levels_done table
	fprintf(file, "\tlevels_done={\n--\t\t{levelid, done, stars},\n");
	for (int i=1; i <= PREDEFINED_LEVELS; ++i)
	{
		int passed = 0;
		if( this->is_level_passed(i))
			passed = 1;
		fprintf(file, "\t\t{%d, %d, %d},\n", i, passed, this->get_level_stars(i));
	}
	fprintf(file, "\t},\n");

	fprintf(file, "}\n");
	fclose(file);

	// do checksum part here...
	// TODO: copied from level.cpp, make a filesystem class
	// reopen rw for reading and writing checksum
	// checksums (to prevent level renaming or budget fraud):  not used yet
	file = fopen(full_path_to_config_file.c_str(), "r+");
	if( ! file )
	{
		ERROR_PRINT("Error: Can not open file %s to save checksum!", full_path_to_config_file.c_str());
		return 1;
	}

	// get file size
	fseek(file, 0L, SEEK_END);
	long int file_size = ftell(file);
	if (file_size == -1L)
    {
		ERROR_PRINT("ftell() failed for file %s\n", full_path_to_config_file.c_str());
		fclose(file);
		return 1;
    }
	uint8_t* file_buffer;
	file_buffer = new uint8_t[file_size];

	// seek back to the beginning:
	fseek(file, 0L, SEEK_SET);
	// copy file to buffer
	unsigned char ch;
	size_t count = 0;
	while(fread(&ch, 1, 1, file) == 1)
	{
		file_buffer[count] = ch;
		count++;
	}

	// hash file buffer
	Hash* hash = new Hash();
	int hash_return_value;
	uint8_t hash_value[32];
	size_t data_bit_len = (size_t) file_size * 8;
	hash_return_value = (int) hash->Hash_256(256, (const uint8_t*) file_buffer, data_bit_len, (uint8_t*) hash_value);
	if(hash_return_value != SKEIN_SUCCESS)
	{
		ERROR_PRINT("hash function error: returned %d\n", hash_return_value);
	}

	// write hash to config file
	fseek(file, 0L, SEEK_SET);
	fprintf(file, "-- ");
	for (int i=0;i < 32;i++)
	{
		fprintf(file, "%02x",  hash_value[i]);
	}

	fclose(file);
	delete file_buffer;
	delete hash;

	return 0;
}

// set a level as passed
// works only on 31 Levels on 32Bit systems
void Settings::set_level_passed(unsigned int id){
	if (id > PREDEFINED_LEVELS)
		ERROR_PRINT("level ID %u is too big\n",id);
	else
		levelpassed[id] = true;
}

// return number of passed levels
// works only on 31 Levels on 32Bit systems
unsigned int Settings::number_of_levels_passed() const
{
	int num = 0;
	for( int i = 0; i <= PREDEFINED_LEVELS; ++i)
		if(levelpassed[i])
			num++;
	return num;
}

// return if a level has been passed
// works only on 31 Levels on 32Bit systems
bool Settings::is_level_passed(unsigned int id) const
{
	if (id > PREDEFINED_LEVELS)
	{
		ERROR_PRINT("level ID %u is too big; return level as not passed.\n", id);
		return false;
	}
	else
	{
		return levelpassed[id];
	}
}

unsigned int Settings::get_level_stars(unsigned int id) const
{
	if( id > PREDEFINED_LEVELS )
	{
		ERROR_PRINT("Level ID %d is too big.\n", id);
		return 0;
	}
	else
		return levelstars[id];
}

void Settings::set_level_stars(unsigned int id, unsigned int stars)
{
	if( id > PREDEFINED_LEVELS )
		ERROR_PRINT("Level ID %d is too big; write 0.\n",id);
	else
	{
		if(levelstars[id] < stars)
			levelstars[id] = stars;
	}
	return;
}

Settings::~Settings() {
}

