//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <ctype.h>
#include <stdlib.h>
#include <unistd.h> // getopt
#include <string>
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <GL/gl.h>
#include <getopt.h>

#include "error.hpp"
#include "game.hpp"
#include "color4f.hpp"
#include "settings.hpp"
#include "sdl_draw.hpp"
#include "benchmark.hpp"
#include "physics_test.hpp"
#include "version.cmake.hpp"

void sdl_info(SDL_Renderer *renderer)
{
	SDL_version version_info;
	SDL_RendererInfo renderer_info;

	SDL_GetVersion(&version_info);
	SDL_GetRendererInfo(renderer, &renderer_info);
	std::cout << "Video information: " << std::endl
         << "System:            " << SDL_GetPlatform() << std::endl
         << "Backend:           " << "SDL " <<(int) version_info.major << "." << (int) version_info.minor << "." << (int) version_info.patch << std::endl
         << "Video driver used: " << SDL_GetCurrentVideoDriver() << std::endl
         << "Renderer used:     " << renderer_info.name << std::endl
         << "HW Accelerated:    " << ((renderer_info.flags & SDL_RENDERER_ACCELERATED) == SDL_RENDERER_ACCELERATED ? "Yes" : "No") << std::endl
         << "Render to texture: " << ((renderer_info.flags & SDL_RENDERER_TARGETTEXTURE) == SDL_RENDERER_TARGETTEXTURE ? "Yes" : "No") << std::endl;
}

// Print usage information
static void display_help_and_exit(void)
{
	std::cout	<< PACKAGE_NAME << " " << PACKAGE_VERSION << std::endl
				<< "A free software bridge construction game\n"
				<< "Usage: " << PACKAGE_NAME << " [OPTION]...\n\n"

				<< "-b [SEC] --benchmark [SEC] make a benchmark for SEC seconds (default: 5)\n"
				<< "-h       --help            print this help\n"
				<< "-s MODE  --sdl=MODE        use MODE for SDL video output (default: opengl)\n"
				<< "                           options are: \"hardware\", \"software\", \"opengl\"\n"
				<< "-t       --test-physics    test physic engine (infinite loop)\n"
				<< "-v       --verbose         output verbose informations\n"
				<< "-V       --version         print version and exit\n"
				<< std::endl;
	exit(0);
}

// FIXME return error (retry without multisample)
static void init_opengl(SDL_Window *window)
{
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

	SDL_GLContext glcontext = SDL_GL_CreateContext(window);
	if(glcontext == nullptr)
	{
		SDLERROR_PRINT("%s\n", "Couldn't create GLContext");
	}
	else
	{
		/// as everything is drawen in order no need of depth
		// glDepthFunc(GL_LESS);
		// glEnable(GL_DEPTH_TEST);
		glEnable(GL_MULTISAMPLE);
		glShadeModel(GL_SMOOTH);

		int window_w = Settings::singleton()->window_width;
		int window_h = Settings::singleton()->window_height;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glViewport(0,0,window_w, window_h);
		glOrtho(0,window_w,window_h,0,0,128);
	}
}

struct options {
  int benchmark_sec = 0;
  bool verbose = false;
  bool test_physics = false;
};

static void process_options(int argc, char* argv[], options& opts)
{
	int option_index = 0;
	bool set_sdl_mode = false;

	int error = 0;
	for(;;)
	{
		/** Options for getopt */
		static struct option long_options[] =
		{
			{"benchmark", optional_argument, NULL, 'b'},
			{"help", no_argument, NULL, 'h'},
			{"sdl", required_argument, NULL, 's'},
			{"test-physics", no_argument, NULL, 't'},
			{"verbose", no_argument, NULL, 'v'},
			{"version", no_argument, NULL, 'V'},
			{NULL, 0, NULL, 0}
		};
		int c = getopt_long(argc, argv, "b::hs:tvV", long_options, &option_index);
		if (c == -1)
			break;
		switch (c)
		{
			case 'b':
				if (optarg != NULL)
					opts.benchmark_sec = atoi(optarg);
				else if (optind<argc && atoi(argv[optind]))
					opts.benchmark_sec = atoi(argv[optind]);
				else
					opts.benchmark_sec = 5;
				break;
			case 'h': error = 1; break;
			case 's':
				if (optarg != NULL)
				{
					if(std::string(optarg) == "hardware")
					{
						Settings::singleton()->video_mode = Settings::HARDWARE_SDL;
						set_sdl_mode = true;
					}
					else if(std::string(optarg) == "software")
					{
						Settings::singleton()->video_mode = Settings::SOFTWARE_SDL;
						set_sdl_mode = true;
					}
					else if(std::string(optarg) == "opengl")
					{
						Settings::singleton()->video_mode = Settings::OPENGL_SDL;
						set_sdl_mode = true;
					}
					else
					{
						std::cerr << "ERROR: Unrecognized SDL mode " << optarg << std::endl;
						error = 1;
					}
				}
				else
				{
					std::cerr << "ERROR: Missing SDL mode"  << std::endl;
					error = 1;
				}
				break;
			case 't': opts.test_physics = true; break;
			case 'v': Settings::singleton()->verbose_cli_output = true; break;
			case 'V': printf("%s %s ", PACKAGE_NAME, PACKAGE_VERSION);
					  printf("(compiled at %s %s)\n", __DATE__, __TIME__);
					  exit(0); break;
			default:
					  error = 1;

		}
	}

	if (opts.benchmark_sec > 0 && set_sdl_mode )
	{
		std::cerr << "ERROR: setting SDL mode is meaningless, if benchmark is selected" << std::endl;
	}
	if (opts.benchmark_sec < 0 )
	{
		std::cerr << "ERROR: negative number of seconds" << std::endl;
		error = 1;
	}
	if (opts.benchmark_sec > 30 )
	{
		std::cerr << 	"WARNING: running benchmarks longer than 30 seconds is only usefull on very slow systems;\n"
						"otherwise this will generate strange results."<< std::endl;
	}

	if (error)
		display_help_and_exit();
}


int main(int argc, char *argv[])
{
	options opts;
	process_options(argc, argv, opts);

	if(opts.benchmark_sec > 0)
	{
		// do benchmark and exit
		Benchmark* bench = new Benchmark();
		unsigned long iter_per_sec = bench->run(opts.benchmark_sec);
		std::cout << "Result: " << iter_per_sec << " interations/sec" << std::endl;
		delete bench;
		return 0;
	}
	if(opts.test_physics)
	{
		Physic_Test* test = new Physic_Test();
		test->run_test_for_SIGSEGV();
		delete test;
		return 0;
	}

	//Start up SDL and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0)
	{
		ERROR_PRINT("SDL_Init Error: %s\n", SDL_GetError());
		return 1;
	}

	//Also need to init SDL_ttf
	if (TTF_Init() != 0)
	{
		ERROR_PRINT("TTF_Init Error: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	//Setup window and renderer
	Uint32 flags = SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE;
	if(Settings::singleton()->video_mode == Settings::OPENGL_SDL)
	{
		flags |= SDL_WINDOW_OPENGL;
	}

	SDL_Window* window = SDL_CreateWindow(PACKAGE_NAME " " PACKAGE_VERSION, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			Settings::singleton()->window_width, Settings::singleton()->window_height, flags);
	if(!window)
	{
		ERROR_PRINT("SDL_CreateWindow Error: %s\n", SDL_GetError());
		TTF_Quit();
		SDL_Quit();
		return 1;
	}

	Settings::singleton()->sdl_window = window;

	SDL_Renderer *renderer = NULL;
	if(Settings::singleton()->video_mode == Settings::OPENGL_SDL)
	{
		INFO_PRINT("%s\n", "create OpenGL window.");
		init_opengl(window);
	}
	else
	{
		if(Settings::singleton()->video_mode == Settings::HARDWARE_SDL)
			flags = SDL_RENDERER_ACCELERATED;
		if(Settings::singleton()->video_mode == Settings::SOFTWARE_SDL)
			flags = SDL_RENDERER_SOFTWARE;

		renderer = SDL_CreateRenderer(window, -1, flags);
		if(!renderer)
		{
			ERROR_PRINT("SDL_CreateRenderer Error: %s\n", SDL_GetError());
			SDL_DestroyWindow(window);
			TTF_Quit();
			SDL_Quit();
			return 1;
		}
		if(Settings::singleton()->verbose_cli_output)
			sdl_info(renderer);
	}

	// make window fullscreen if in config file
	if(Settings::singleton()->sdl_fullscreen)
	{
		int retval = SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
		if(retval !=0)
			SDLERROR_PRINT("%s\n", "Couldn't set fullscreen");
	}

	if(Settings::singleton()->video_mode == Settings::OPENGL_SDL)
		Settings::singleton()->sdl_display = new SDL_opengl_draw();
	else
		Settings::singleton()->sdl_display = new SDL_draw(renderer);

	// Main loop
    Game game(renderer, window);
    game.loop();

	//Clean up
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	TTF_Quit();
	SDL_Quit();

	return 0;
}

