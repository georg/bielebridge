//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <ctime>
#include <sys/time.h>
#include <cstdio>
#include <csignal>
#include <iostream>
#include <fstream>
#include "vector2d.hpp"
#include "level.hpp"
#include "sdl_draw.hpp"
#include "benchmark.hpp"
#include "train.hpp"
#include "error.hpp"
#include "spring.hpp"
#include "particle.hpp"
#include "configure.cmake.hpp"

// global var and handler for
// virtual timer (cpu time) alarm
bool stop_benchmark;
void timer_handler(int sig)
{
	stop_benchmark = true;
}

Benchmark::Benchmark()
{
	// find background image
	std::string full_path_to_benchmark_file = "";
	std::string test_path = DATADIR "/"+ fname_benchmark;
	if(access(test_path.c_str(), F_OK) != -1 )
	{
		full_path_to_benchmark_file = test_path;
	}
	else if(access(("data/"+ fname_benchmark).c_str(), F_OK) != -1 )
	{
		full_path_to_benchmark_file = "data/"+ fname_benchmark;
	}
	else if(access(("../data/"+ fname_benchmark).c_str(), F_OK) != -1 )
	{
		full_path_to_benchmark_file = "../data/"+ fname_benchmark;
	}
	else
	{
		// get home folder
		const char* home = "HOME";
		char *user_home = getenv(home);
		if (user_home)
		{
			std::string game_home(user_home);
			game_home.append("/" + Settings::singleton()->game_folder_name);
			// game home folder exists?
			if( access( game_home.c_str(), F_OK ) != -1 )
			{
				// path ok, set full name of config file
				full_path_to_benchmark_file = game_home + "/" + Settings::singleton()->dirname_levels + "/" + fname_benchmark;
			}
		}
	}

	level = new Level(0, "benchmark", full_path_to_benchmark_file.c_str());
	if(!level->load_failed)
		INFO_PRINT("Loaded benchmark level: %s\n", full_path_to_benchmark_file.c_str());
	else
		ERROR_PRINT("failed to load benchmark level: %s\n", full_path_to_benchmark_file.c_str());

	level->copy_for_simulation();
	physics = new Physics(level);
}

void Benchmark::print_cpu_info()
{
	std::ifstream procfile;
	procfile.open("/proc/cpuinfo");
	if(procfile.is_open())
	{
		const std::string line_begin = "model name";
		for (std::string line; std::getline(procfile, line); )
		{
			if(line.compare(0, 10, "model name") == 0)
			{
				size_t cutindex = line.find(":");
				if( cutindex != std::string::npos)
					line.erase(0, cutindex+2);
				printf("(%s)", line.c_str());
				return;
			}
		}
	}
}

// runs about "sec_to_run" seconds
// return iterations per second
unsigned long Benchmark::run(const unsigned int sec_to_run)
{
	if(level->load_failed)
	{
		ERROR_PRINT("Couldn't load benchmark level \"%s\". Nothing to do.\n", fname_benchmark.c_str());
		return 0;
	}

	// some debug output
	printf("Make a %d sec benchmark ", sec_to_run);
	this->print_cpu_info();
	printf("\n");
	size_t total_level_size = level->ground_len*sizeof(float_32)
								+ level->sim_particles.size()*sizeof(Particle)
								+ level->sim_springs.size()*sizeof(Spring)
								+ level->sim_deck_springs_i.size()*sizeof(uint_fast32_t);
	uint32_t total_level_size_kb = uint32_t(total_level_size / 1024);
	printf( "benchmark level contains of: "
			"%d ground points (%d bytes), %d particles (%d bytes), "
			"%d springs (%d bytes), %d deck spring indices (%d bytes);"
			"\nSimulator data size: %u KiB\n",
			(int)level->ground_len,               (int) sizeof(float_32),
			(int)level->sim_particles.size(),     (int) sizeof(Particle),
			(int)level->sim_springs.size(),       (int) sizeof(Spring),
			(int)level->sim_deck_springs_i.size(),(int) sizeof(uint_fast32_t),
			total_level_size_kb);
	// set up signal handler
	struct sigaction sigact;
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = 0;
	sigact.sa_handler = timer_handler;
	sigaction(SIGVTALRM, &sigact, NULL);

	uint32_t count_all_iter = 0;
	// dummy vector
	std::vector<Spring*> broken_springs;

	stop_benchmark = false;

	// setup alarm timer
	struct itimerval itval;
	itval.it_interval.tv_sec = 0;
	itval.it_interval.tv_usec = 0;
	itval.it_value.tv_sec = sec_to_run;
	itval.it_value.tv_usec = 0;
	// use a ITIMER_VIRTUAL (counts processor time used by the process)
	// TODO: this will fail with multi core usage (OpenMP)
	setitimer(ITIMER_VIRTUAL, &itval, 0);

	printf("Benchmark running\n");
	fflush(stdout);
	while(!stop_benchmark){
		physics->update(broken_springs);
		if( (count_all_iter++ % 2048) == 0)
		{
			if( count_all_iter > 294904 )
			{
				ERROR_PRINT("%s\n", "Everyting past this iteration will be a missleading result. Please choose a shorter benchmark time. Your CPU is too fast ;-)");
			}
			else
			{
				if(! Settings::singleton()->verbose_cli_output)
				{
					printf(".");
					fflush(stdout);
				}
			}
		}
	}
	printf(" %u iterations.\n", count_all_iter);

	return count_all_iter / sec_to_run;
}

Benchmark::~Benchmark()
{
	delete physics;
	delete level;
}

