//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_GUI_MANAGER_H
#define HEADER_GUI_MANAGER_H

/** switches the different GUI-Layouts like GUIMENU_START, GUIMENU_LEVELS, GUIEDITOR,... */

#include <string>

class GUI_Menu_Start;
class GUI_Menu_About;
class GUI_Menu_Levels;
class GUI_Menu_Options;
class GUI_Simulator;
class GUI_Editor;
class Settings;
class Level;

class GUI_Manager
{
private:
	static GUI_Manager* instance;

	GUI_Menu_Start* gui_menu_start;
	GUI_Menu_About* gui_menu_about;
	GUI_Menu_Levels* gui_menu_levels;
	GUI_Menu_Options* gui_menu_options;
	GUI_Simulator* gui_simulator;
	GUI_Editor* gui_editor;

public:
	enum Layout {GUIMENU_START, GUIMENU_OPTIONS, GUIMENU_ABOUT, GUIMENU_LEVELS, GUIEDITOR, GUISIMULATOR};
	Layout currentGUI;

	int window_width;
	int window_height;

	GUI_Manager();
	~GUI_Manager();
	void draw();
	void update();
	void event_mouse_click(int button, int button_state, int x, int y);
	void event_keyboard(const char* utf8_key);
	void event_mouse_wheel(int32_t y);
	void event_resize();
	void quit();
	void user_pressed_help();

	void createGUI_Editor(const int level_id, const std::string& level_name, const std::string& level_full_path);
	void createGUI_Simulator(Level* level);

};
#endif

