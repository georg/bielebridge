//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//  Copyright (C) 2002 Ingo Ruhnke <grumbel@gmx.de>
//
//  This file is part of bielebridge  but was at least
//  partially developed for construo by Ingo Ruhnke.
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_GUI_COMPONENT_H
#define HEADER_GUI_COMPONENT_H

#include <string>

enum Alignment {LEFT_UPPER, CENTER_UPPER, RIGHT_UPPER,
				LEFT_CENTER,CENTER_CENTER, RIGHT_CENTER,
				LEFT_LOWER, CENTER_LOWER, RIGHT_LOWER,
				RES_INDEPENDENT};
// RES_INDEPENDENT is special and means "resolution independent"
// x, y, width, height are stored in permille (parts of 1000) of screen width/height

/** encapsulate the basic gui things like position and move on resize */
class GUI_Component
{
protected:
	int pos_x;
	int pos_y;
	int width;
	int height;

	bool visible = true;

	// to move buttons after window resizing
	Alignment alignment;
	int alignment_x_offset;
	int alignment_y_offset;
	void recalc_offsets(const Alignment a, const int pos_x, const int pos_y, int& alignment_x_offset, int& alignment_y_offset);
public:
	GUI_Component (int x, int y, int width, int height, Alignment alignment);
	virtual ~GUI_Component() {}

	/** @return true if the component is present at the given location */
	virtual bool is_at(int x, int y);

	/** @return text that is beeing displayed */
	virtual std::string get_text() =0;

	void set_position (int x, int y) { pos_x = x, pos_y = y; }
	void set_width(int w) { width = w; }
	void set_height(int h) { height = h; }

	int get_x_pos();
	int get_y_pos();
	// important for RES_INDEPENDENT mode to get pixel perfect output (without rounding errors)
	int get_x_plus_width_pos();
	int get_y_plus_height_pos();
	int get_width();
	int get_height();

	void on_window_resize ();

	void set_visible(bool v){ visible = v; }
	bool get_visible(){return visible; }
};


#endif

