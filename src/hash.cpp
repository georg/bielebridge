//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.


#include <cstdio>
#include <cstring>
#include "hash.hpp"

Hash::Hash()
{
}

Hash::~Hash()
{
}

// Derived from the public domain Skein reference implementation
// (credits to the Skein team: see http://www.skein-hash.info/ )
// DO NOT COPY THIS CODE AND USE IT FOR CRYPTOGRAPHIC PURPOSE
// THIS VERSION HAS BEEN SIMPLIFIED AND IS PROBABLY INSECURE
// It is meant to prevent cheating by editing budget, levels or config

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* init the context for a straight hashing operation */
int Hash::Skein_256_Init(Skein_256_Ctxt_t *ctx, size_t hashBitLen)
{
	union
	{
		uint8_t  b[SKEIN_256_STATE_BYTES];
		uint64_t  w[SKEIN_256_STATE_WORDS];
	} cfg;                                  /* config block */

	Skein_Assert(hashBitLen > 0,SKEIN_BAD_HASHLEN);

	/* build/process config block for hashing */
	ctx->h.hashBitLen = hashBitLen;             /* output hash byte count */
	Skein_Start_New_Type(ctx,CFG_FINAL);        /* set tweaks: T0=0; T1=CFG | FINAL */

	memset(&cfg.w,0,sizeof(cfg.w));             /* pre-pad cfg.w[] with zeroes */
	cfg.w[0] = 0;                               /* this is not skein any more */
	cfg.w[1] = hashBitLen;                      /* hash result length in bits */
	cfg.w[2] = 0;

	/* compute the initial chaining values from config block */
	memset(ctx->X,0,sizeof(ctx->X));            /* zero the chaining variables */
	Skein_256_Process_Block(ctx,cfg.b,1,SKEIN_CFG_STR_LEN);

	/* The chaining vars ctx->X are now initialized for the given hashBitLen. */
	/* Set up to process the data message portion of the hash (default) */
	Skein_Start_New_Type(ctx,MSG);              /* T0=0, T1= MSG type, h.bCnt=0 */

	return SKEIN_SUCCESS;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* process the input bytes */
int Hash::Skein_256_Update(Skein_256_Ctxt_t *ctx, const uint8_t *msg, size_t msgByteCnt)
{
	size_t n;

	Skein_Assert(ctx->h.bCnt <= SKEIN_256_BLOCK_BYTES,SKEIN_FAIL);     /* catch uninitialized context */

	/* process full blocks, if any */
	if (msgByteCnt + ctx->h.bCnt > SKEIN_256_BLOCK_BYTES)
	{
		if (ctx->h.bCnt)                              /* finish up any buffered message data */
		{
			n = SKEIN_256_BLOCK_BYTES - ctx->h.bCnt;  /* # bytes free in buffer b[] */
			if (n)
			{
				Skein_assert(n < msgByteCnt);         /* check on our logic here */
				memcpy(&ctx->b[ctx->h.bCnt],msg,n);
				msgByteCnt  -= n;
				msg         += n;
				ctx->h.bCnt += n;
			}
			Skein_assert(ctx->h.bCnt == SKEIN_256_BLOCK_BYTES);
			Skein_256_Process_Block(ctx,ctx->b,1,SKEIN_256_BLOCK_BYTES);
			ctx->h.bCnt = 0;
		}
		/* now process any remaining full blocks, directly from input message data */
		if (msgByteCnt > SKEIN_256_BLOCK_BYTES)
		{
			n = (msgByteCnt-1) / SKEIN_256_BLOCK_BYTES;   /* number of full blocks to process */
			Skein_256_Process_Block(ctx,msg,n,SKEIN_256_BLOCK_BYTES);
			msgByteCnt -= n * SKEIN_256_BLOCK_BYTES;
			msg        += n * SKEIN_256_BLOCK_BYTES;
		}
		Skein_assert(ctx->h.bCnt == 0);
	}

	/* copy any remaining source message data bytes into b[] */
	if (msgByteCnt)
	{
		Skein_assert(msgByteCnt + ctx->h.bCnt <= SKEIN_256_BLOCK_BYTES);
		memcpy(&ctx->b[ctx->h.bCnt],msg,msgByteCnt);
		ctx->h.bCnt += msgByteCnt;
	}

	return SKEIN_SUCCESS;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* finalize the hash computation and output the result */
int Hash::Skein_256_Final(Skein_256_Ctxt_t *ctx, uint8_t *hashVal)
{
	size_t i,n,byteCnt;
	uint64_t X[SKEIN_256_STATE_WORDS];
	Skein_Assert(ctx->h.bCnt <= SKEIN_256_BLOCK_BYTES,SKEIN_FAIL);    /* catch uninitialized context */

	ctx->h.T[1] |= SKEIN_T1_FLAG_FINAL;        /* tag as the final block */
	if (ctx->h.bCnt < SKEIN_256_BLOCK_BYTES)   /* zero pad b[] if necessary */
		memset(&ctx->b[ctx->h.bCnt],0,SKEIN_256_BLOCK_BYTES - ctx->h.bCnt);
	Skein_256_Process_Block(ctx,ctx->b,1,ctx->h.bCnt);    /* process the final block */

	/* now output the result */
	byteCnt = (ctx->h.hashBitLen + 7) >> 3;    /* total number of output bytes */

	/* run Threefish in "counter mode" to generate output */
	memset(ctx->b,0,sizeof(ctx->b));  /* zero out b[], so it can hold the counter */
	memcpy(X,ctx->X,sizeof(X));       /* keep a local copy of counter mode "key" */
	for (i=0;i*SKEIN_256_BLOCK_BYTES < byteCnt;i++)
	{
		((uint64_t *)ctx->b)[0] = (uint64_t) i; /* build the counter block */
		Skein_Start_New_Type(ctx,OUT_FINAL);
		Skein_256_Process_Block(ctx,ctx->b,1,sizeof(uint64_t)); /* run "counter mode" */
		n = byteCnt - i*SKEIN_256_BLOCK_BYTES;   /* number of output bytes left to go */
		if (n >= SKEIN_256_BLOCK_BYTES)
			n  = SKEIN_256_BLOCK_BYTES;
		Skein_Put64_LSB_First(hashVal+i*SKEIN_256_BLOCK_BYTES,ctx->X,n);   /* "output" the ctr mode bytes */
		memcpy(ctx->X,X,sizeof(X));   /* restore the counter mode key for next time */
	}
	return SKEIN_SUCCESS;
}

void Hash::Skein_256_Process_Block(Skein_256_Ctxt_t *ctx,const uint8_t *blkPtr,size_t blkCnt,size_t byteCntAdd)
{ /* do it in C */
	enum
	{
		WCNT     = SKEIN_256_STATE_WORDS
	};
	size_t  i,r;
	uint64_t  ts[3];                            /* key schedule: tweak */
	uint64_t  ks[WCNT+1];                       /* key schedule: chaining vars */
	uint64_t  X [WCNT];                         /* local copy of context vars */
	uint64_t  w [WCNT];                         /* local copy of input block */

	Skein_assert(blkCnt != 0);                /* never call with blkCnt == 0! */
	do  {
		/* this implementation only supports 2**64 input bytes (no carry out here) */
		ctx->h.T[0] += byteCntAdd;            /* update processed length */

		/* precompute the key schedule for this block */
		ks[WCNT] = ((0x55555555) + (((uint64_t) (0x55555555)) << 32));
		for (i=0;i < WCNT; i++)
		{
			ks[i]     = ctx->X[i];
			ks[WCNT] ^= ctx->X[i];            /* compute overall parity */
		}
		ts[0] = ctx->h.T[0];
		ts[1] = ctx->h.T[1];
		ts[2] = ts[0] ^ ts[1];

		Skein_Get64_LSB_First(w,blkPtr,WCNT); /* get input block in little-endian format */
		for (i=0;i < WCNT; i++)               /* do the first full key injection */
		{
			X[i]  = w[i] + ks[i];
		}
		X[WCNT-3] += ts[0];
		X[WCNT-2] += ts[1];

		for (r=1;r <= SKEIN_256_ROUNDS_TOTAL/8; r++)
		{ /* unroll 8 rounds */
			X[0] += X[1]; X[1] = RotL_64(X[1],R_256_0_0); X[1] ^= X[0];
			X[2] += X[3]; X[3] = RotL_64(X[3],R_256_0_1); X[3] ^= X[2]; // Skein_Show_Round(BLK_BITS,&ctx->h,8*r-7,X);
			X[0] += X[3]; X[3] = RotL_64(X[3],R_256_1_0); X[3] ^= X[0];
			X[2] += X[1]; X[1] = RotL_64(X[1],R_256_1_1); X[1] ^= X[2]; // Skein_Show_Round(BLK_BITS,&ctx->h,8*r-6,X);
			X[0] += X[1]; X[1] = RotL_64(X[1],R_256_2_0); X[1] ^= X[0];
			X[2] += X[3]; X[3] = RotL_64(X[3],R_256_2_1); X[3] ^= X[2]; // Skein_Show_Round(BLK_BITS,&ctx->h,8*r-5,X);
			X[0] += X[3]; X[3] = RotL_64(X[3],R_256_3_0); X[3] ^= X[0];
			X[2] += X[1]; X[1] = RotL_64(X[1],R_256_3_1); X[1] ^= X[2]; // Skein_Show_Round(BLK_BITS,&ctx->h,8*r-4,X);
			InjectKey(2*r-1);

			X[0] += X[1]; X[1] = RotL_64(X[1],R_256_4_0); X[1] ^= X[0];
			X[2] += X[3]; X[3] = RotL_64(X[3],R_256_4_1); X[3] ^= X[2]; // Skein_Show_Round(BLK_BITS,&ctx->h,8*r-3,X);
			X[0] += X[3]; X[3] = RotL_64(X[3],R_256_5_0); X[3] ^= X[0];
			X[2] += X[1]; X[1] = RotL_64(X[1],R_256_5_1); X[1] ^= X[2]; // Skein_Show_Round(BLK_BITS,&ctx->h,8*r-2,X);
			X[0] += X[1]; X[1] = RotL_64(X[1],R_256_6_0); X[1] ^= X[0];
			X[2] += X[3]; X[3] = RotL_64(X[3],R_256_6_1); X[3] ^= X[2]; // Skein_Show_Round(BLK_BITS,&ctx->h,8*r-1,X);
			X[0] += X[3]; X[3] = RotL_64(X[3],R_256_7_0); X[3] ^= X[0];
			X[2] += X[1]; X[1] = RotL_64(X[1],R_256_7_1); X[1] ^= X[2]; // Skein_Show_Round(BLK_BITS,&ctx->h,8*r  ,X);
			InjectKey(2*r);
		}
		/* do the final "feedforward" xor, update context chaining vars */
		for (i=0;i < WCNT;i++)
			ctx->X[i] = X[i] ^ w[i];

		Skein_Clear_First_Flag(ctx->h);		/* clear the start bit */
		blkPtr += SKEIN_256_BLOCK_BYTES;
	}
	while (--blkCnt);
}

void    Hash::Skein_Put64_LSB_First(uint8_t *dst,const uint64_t *src,size_t bCnt)
{ /* this version is fully portable (big-endian or little-endian), but slow */
	size_t n;

	for (n=0;n<bCnt;n++)
		dst[n] = (uint8_t) (src[n>>3] >> (8*(n&7)));
}

void    Hash::Skein_Get64_LSB_First(uint64_t *dst,const uint8_t *src,size_t wCnt)
{ /* this version is fully portable (big-endian or little-endian), but slow */
	size_t n;

	for (n=0;n<8*wCnt;n+=8)
		dst[n/8] = (((uint64_t) src[n  ])      ) +
			(((uint64_t) src[n+1]) <<  8) +
			(((uint64_t) src[n+2]) << 16) +
			(((uint64_t) src[n+3]) << 24) +
			(((uint64_t) src[n+4]) << 32) +
			(((uint64_t) src[n+5]) << 40) +
			(((uint64_t) src[n+6]) << 48) +
			(((uint64_t) src[n+7]) << 56) ;
}

/* 64-bit rotate left */
uint64_t Hash::RotL_64(uint64_t x,unsigned int N)
{
	return (x << (N & 63)) | (x >> ((64-N) & 63));
}

/******************************************************************/
/*     AHS API code                                               */
/******************************************************************/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* select the context size and init the context */
HashReturn Hash::Init(hashState *state, int hashbitlen)
{
	Skein_Assert(hashbitlen > 0,BAD_HASHLEN);
	state->statebits = 64*SKEIN_256_STATE_WORDS;
	return (HashReturn) Skein_256_Init(&state->u.ctx_256,(size_t) hashbitlen);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* process data to be hashed */
HashReturn Hash::Update(hashState *state, const uint8_t *data, size_t databitlen)
{
	/* only the final Update() call is allowed do partial bytes, else assert an error */
	Skein_Assert((state->u.h.T[1] & SKEIN_T1_FLAG_BIT_PAD) == 0 || databitlen == 0, FAIL);

	Skein_Assert(state->statebits % 256 == 0 && (state->statebits-256) < 1024,FAIL);
	if ((databitlen & 7) == 0)  /* partial bytes? */
	{
		switch ((state->statebits >> 8) & 3)
		{
			case 1:  return (HashReturn) Skein_256_Update(&state->u.ctx_256,data,databitlen >> 3);
			default: return FAIL;
		}
	}
	else
	{   /* handle partial final byte */
		size_t bCnt = (databitlen >> 3) + 1;                  /* number of bytes to handle (nonzero here!) */
		uint8_t b,mask;

		mask = (uint8_t) (1u << (7 - (databitlen & 7)));       /* partial byte bit mask */
		b    = (uint8_t) ((data[bCnt-1] & (0-mask)) | mask);   /* apply bit padding on final byte */

		switch ((state->statebits >> 8) & 3)
		{
			case 1:  Skein_256_Update(&state->u.ctx_256,data,bCnt-1); /* process all but the final byte    */
					 Skein_256_Update(&state->u.ctx_256,&b  ,  1   ); /* process the (masked) partial byte */
					 break;
			default: return FAIL;
		}
		Skein_Set_Bit_Pad_Flag(state->u.h);                    /* set tweak flag for the final call */

		return SUCCESS;
	}
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* finalize hash computation and output the result (hashbitlen bits) */
HashReturn Hash::Final(hashState *state, uint8_t *hashval)
{
	Skein_Assert(state->statebits % 256 == 0 && (state->statebits-256) < 1024,FAIL);
	switch ((state->statebits >> 8) & 3)
	{
		case 1:  return (HashReturn) Skein_256_Final(&state->u.ctx_256,hashval);
		default: return FAIL;
	}
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* all-in-one hash function */
HashReturn Hash::Hash_256(int hashbitlen, const uint8_t *data, /* all-in-one call */
		size_t databitlen,uint8_t *hashval)
{
	hashState  state;
	HashReturn r = Init(&state,hashbitlen);
	if (r == SUCCESS)
	{ /* these calls do not fail when called properly */
		r = Update(&state,data,databitlen);
		Final(&state,hashval);
	}
	return r;
}

