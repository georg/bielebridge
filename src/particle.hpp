//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//  Copyright (C) 2002 Ingo Ruhnke <grumbel@gmx.de>
//
//  This file is part of bielebridge  but was at least
//  partially developed for construo by Ingo Ruhnke.
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_PARTICLE_H
#define HEADER_PARTICLE_H

#include "vector2d.hpp"
#include "error.hpp"

class Particle
{
public:
	Particle();
	Particle(const int id_, const Vector2d& arg_pos, float_32 mass_, bool fixed_, bool used_in_train_);
	Particle(const Particle&);

#ifdef DEBUG_FORCES
	Vector2d debug_force_1;
	Vector2d debug_force_2;
#endif
	/** Id of the particle */
	int id;
//	uint_fast32_t id;

	/** position of the particle */
	Vector2d pos;

	/** velocity of the particle */
	Vector2d velocity;

	/** the mass of the particle */
	float_32 mass;
	float_32 inv_mass;

	/** If true the particle will have a fixed position which cannot be
	  changed, if false the particle reacts to forces as normal. */
	bool fixed;

	/** mark if a particle is used in the train, this will influence
	*  particle simulation and drawing */
	bool used_in_train;

	/** totale force acting on particle (used as temp-var in update() to
	  collect the forces)*/
	Vector2d totale_force;

	/** Number of connections this particle.hppas to other springs */
	int spring_links;

	/** The id of the particle, used for de/serialisation and copying of
	  the World. */
	inline int get_id () const {
		return id;
	}

	void add_force (const Vector2d& force)
	{
		if (!fixed)
		{
#ifdef DEBUG
			const float_32 force_max = 4000.0f;
			if(force.length_square() > force_max * force_max)
			{
				ERROR_PRINT("very big force was applyed: %f\n", force.length());
			}
#endif
			totale_force += force;

		}
	}

	void clear_force ()
	{
		totale_force = Vector2d ();
	}

	void set_fixed (const bool f)
	{
		fixed = f;
	}

	bool get_fixed () const
	{
		return fixed;
	}

	void update (const float_32 delta_t, const float_32 water_height);

};

#endif

