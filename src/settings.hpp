//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//  Copyright (C) 2002 Ingo Ruhnke <grumbel@gmx.de>
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_SETTINGS_H
#define HEADER_SETTINGS_H

#include <SDL2/SDL.h>
#include <string>

class IDraw_Move_Zoom;

// bielebridge used doubles some time ago, but:
// 1) some platforms have no double
// 2) precision is not needed (more iterations are better)
// 3) on most platforms float ist faster
//
// if this typedef is changed, physics will change,
// it might be that bridges fail after that change
typedef float float_32;

// Singleton for global game settings and variables
class Settings
{
// only needed by Settings itself
private:
	const std::string config_filename = "bielebridge.config";
	std::string full_path_to_config_file = "bielebridge.config";
	static Settings* instance;
	Settings();
// used all over
public:
	void init_paths();
	void find_bgimage();
	static Settings* singleton();
	~Settings();
	int save();
	void set_level_passed(unsigned int id);
	void set_level_stars(unsigned int id, unsigned int stars);
	unsigned int number_of_levels_passed() const;
	bool is_level_passed(unsigned int id) const;
	unsigned int get_level_stars(unsigned int id) const;

	////////////////////////////////////////////////////////////////////////////////
	// SDL, window and mouse variables
	// SDL_Renderer* renderer;
	SDL_Window* sdl_window = nullptr;
	bool sdl_fullscreen = false;// loaded from config file if present

	IDraw_Move_Zoom* sdl_display;
	// by default use sdl opengl (most performant)
	enum Video_Mode
	{
		OPENGL_SDL = 0,
		HARDWARE_SDL = 1,
		SOFTWARE_SDL = 2
	};
	Video_Mode video_mode = OPENGL_SDL;

	enum Video_Multisample
	{
		MULTISAMPLE_OFF = 0,
		MULTISAMPLE_2X = 1,
		MULTISAMPLE_4X = 2,
		MULTISAMPLE_8X = 3,
	};
	Video_Multisample video_multisample = MULTISAMPLE_4X;

	int window_width = 1024;   // loaded from config file if present
	int window_height = 768;   // loaded from config file if present

	char window_title[100];
	int mouse_x, mouse_y;
	bool mouse_left_down = false;
	bool mouse_right_down = false;


	/////////////////////////////////////////////////////////////////
	// Colors (each 0 - 255):                      R    G    B  Alpha
	const SDL_Color sdlc_background_menu      = {  0,   0,   0, 255};
	const SDL_Color sdlc_background_editor    = { 40,  40,  40, 255};
	const SDL_Color sdlc_background_simulator = { 80,  80, 120, 255};
	const SDL_Color sdlc_ground               = {100,  90,  80, 255};
	const SDL_Color sdlc_water                = { 40,  40, 110, 130};

	const SDL_Color sdlc_particle             = {200, 200, 200, 255};
	const SDL_Color sdlc_particle_fixed       = {255,  40,  40, 255};
	const SDL_Color sdlc_particle_highlight   = {255, 255,   0, 255};

	const SDL_Color sdlc_spring               = {153, 153, 153, 255};
	const SDL_Color sdlc_deck                 = {255, 255, 255, 255};
	const SDL_Color sdlc_spring_highlight     = {255, 255,   0, 255};

	const SDL_Color sdlc_grid_most            = { 55,  55,  55, 255};
	const SDL_Color sdlc_grid_some            = {  5,   5,   5, 255};

	const SDL_Color sdlc_train                = { 55,  40,  40, 255};

	const SDL_Color sdlc_red                  = {255,   0,   0, 255};
	const SDL_Color sdlc_green                = {  0, 255,   0, 255};
	const SDL_Color sdlc_blue                 = {  0,   0, 255, 255};
	const SDL_Color sdlc_yellow               = {255, 255,   0, 255};
	const SDL_Color sdlc_white                = {255, 255, 255, 255};
	const SDL_Color sdlc_black                = {  0,   0,   0, 255};

	const SDL_Color sdlc_button_top           = {255,   0,   0, 255};
	const SDL_Color sdlc_button_bottom        = {255, 255,   0, 255};


	// show particles / spring in grey in editing mode
	const SDL_Color sdlc_spring_inactive      = { 60,  60,  60, 255};
	const SDL_Color sdlc_particle_inactive    = {100, 100, 100, 255};
	const SDL_Color sdlc_deck_inactive        = { 60,  60,  60, 255};

	const SDL_Color sdlc_button               = { 20,  20,  20, 190};
	const SDL_Color sdlc_button_highlight     = { 60, 130,  60, 255};

	const SDL_Color sdlc_ground_edit_box      = {107, 107, 107,  30};
	const SDL_Color sdlc_ground_edit_box_hl   = {255, 255, 255, 255}; // highlight ground height

	/////////////////////////////////////////////////////////////////
	// Draw Sizes

	// Lines width in px
	const float_32 spring_line_width_max_stress_edit = 3.0f;
	const float_32 spring_line_width_edit = 2.0f;
	const float_32 spring_deck_line_width_edit = 3.0f;
	const float_32 spring_line_width_sim = 4.0f;
	const float_32 particle_radius_edit = 6.0f;

	const int button_font_size = 22;

	// even if this is about physics numbers (float_32)
	// only integer values should be used
	const int grid_spacing = 10;

	const float text_to_box_margin = 1.03f; // multiplier

	/////////////////////////////////////////////////////////////////
	// Paths and File Extensions
	// for loading levels
	const std::string fname_ext_level = ".blv"; // bielebridge level
	const std::string fname_ext_user = ".ulv";  // user level
	const std::string dirname_levels = "levels";

	// location there to store config and saved levels
	std::string game_home_path = "./";
	const std::string game_folder_name = ".bielebridge";

	// path to font file
	std::string font_file = "DejaVuSans-Bold.ttf";
	std::string full_path_to_font_file = "";

	// background image
	std::string fname_bgimage = "background.png";
	std::string full_path_to_bgimage_file = "";

	/////////////////////////////////////////////////////////////////
	// Physics
	const float_32 max_spring_len = 89.9f;

	const float_32 spring_stiffness = 1060.0f;
	const float_32 spring_damping = 1.5f;
	const float_32 spring_max_stretch = 0.03f; // warning: higher MAX_SPRING_SUBDIVS needed for bigger stretch!

	/////////////////////////////////////////////////////////////////
	// Misc
	uint8_t fps_target = 30;
	// max level width in pixel (prevent loading malicious level files)
	const int max_level_width = 100000;

	const int spring_cost = 10;

	// keep user setting of "Pause on Broken" over multiple simulator runs
	bool simulator_pause_on_broken = false;

#define PREDEFINED_LEVELS 16
	const unsigned int predefined_levels_num = PREDEFINED_LEVELS;
	unsigned int levelstars[PREDEFINED_LEVELS+1];
	bool levelpassed[PREDEFINED_LEVELS+1];

	const int copy_paste_in_editor_threshold = 5;
	const unsigned int level_editor_threshold = 15;

	bool verbose_cli_output = false;
};

#endif

