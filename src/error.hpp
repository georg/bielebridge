//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdio>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#ifndef HEADER_ERROR_H
#define HEADER_ERROR_H

// ANSI escape code colors
// Black        0;30     Dark Gray     1;30
// Blue         0;34     Light Blue    1;34
// Green        0;32     Light Green   1;32
// Cyan         0;36     Light Cyan    1;36
// Red          0;31     Light Red     1;31
// Purple       0;35     Light Purple  1;35
// Brown/Orange 0;33     Yellow        1;33
// Light Gray   0;37     White         1;37

#ifndef DEBUGPRINT
#define DEBUGPRINT false
#endif

#define ERROR_PRINT(fmt, ...) \
        do { fprintf(stderr, "\033[1;31mERROR\033[0m:%s:%d:%s(): " fmt, __FILE__, \
                                __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__); } while (0)

#define DEBUG_PRINT(fmt, ...) \
        do { if (DEBUGPRINT) fprintf(stdout, "\033[1;36mINFO\033[0m:%s:%d:%s(): " fmt, __FILE__, \
                                __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__); } while (0)

#define INFO_PRINT(fmt, ...) \
        do { fprintf(stdout, "\033[1;34mINFO\033[0m:%s:%d:%s(): " fmt, __FILE__, \
                                __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__); } while (0)

#define SDLERROR_PRINT(fmt, ...) \
        do { fprintf(stderr, "%s:%d:%s:%s(): " fmt, __FILE__, \
                                __LINE__, __PRETTY_FUNCTION__, SDL_GetError(), __VA_ARGS__); } while (0)

#endif
