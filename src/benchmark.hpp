//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_BENCHMARK_H
#define HEADER_BENCHMARK_H

#include "gui_editorsimulator.hpp"

#include "physics.hpp"

class Benchmark
{
private:
	Level* level;
	Physics* physics;
	const std::string fname_benchmark = "benchmark.ben";
	void print_cpu_info();
public:
	Benchmark();
	virtual ~Benchmark();
	unsigned long run(const unsigned int sec_to_run);
};

#endif

