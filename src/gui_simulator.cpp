//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <ctime>
#include <sys/time.h>
#include <iostream>
#include <cstdio>
#include <cfloat>

#include "gui_component.hpp"
#include "gui_editorsimulator.hpp"
#include "sdl_draw.hpp"
#include "gui_simulator.hpp"
#include "gui_button.hpp"
#include "gui_manager.hpp"
#include "vector2d.hpp"
#include "spring.hpp"
#include "particle.hpp"
#include "level.hpp"
#include "train.hpp"
#include "color4f.hpp"
#include "settings.hpp"
#include "error.hpp"


GUI_Simulator::GUI_Simulator(Level* l)
{
	level = l;
	sdl = Settings::singleton()->sdl_display;

	back_button = new GUI_Button("Back",     0,     0, 100, 30, RES_INDEPENDENT);
	buttons.push_back(back_button);
	very_fast_button = new GUI_Button("Very Fast", 900, 30, 100, 30, RES_INDEPENDENT);
	buttons.push_back(very_fast_button);
	faster_button = new GUI_Button("Faster",       900, 80, 100, 30, RES_INDEPENDENT);
	buttons.push_back(faster_button);
	slower_button = new GUI_Button("Slower",       900, 130, 100, 30, RES_INDEPENDENT);
	buttons.push_back(slower_button);
	pause_button = new GUI_Button("Pause",         900, 180, 100, 30, RES_INDEPENDENT);
	buttons.push_back(pause_button);
	pause_broken_button = new GUI_Button("Pause on Broken", 800, 970, 200, 30, RES_INDEPENDENT);
	buttons.push_back(pause_broken_button);

	level_name_button = new GUI_Button(level->get_name(), 300, 0, 400, 30, RES_INDEPENDENT);
	level_name_button->set_background(false);

	level_passed_button = new GUI_Button("LEVEL PASSED", 320, 350, 360, 80, RES_INDEPENDENT);
	//level_passed_button->set_background(false);

	next_level_button = new GUI_Button("Next Level", 400, 425, 200, 80, RES_INDEPENDENT);
	//next_level_button->set_background(false);

	action_cam_button = new GUI_Button("Action Cam", 400, 970, 200, 30, RES_INDEPENDENT);
	buttons.push_back(action_cam_button);

	pause_on_broken_spring = Settings::singleton()->simulator_pause_on_broken;

	animation_speed = 60;
	fps = 0;
	count_frames = 0;
	ips = 0;
	count_iter = 0;
	count_all_iter = 0;
	action_cam = false;

	struct timeval tv;
	gettimeofday(&tv, NULL);
	current_sec = tv.tv_sec;

	physics = new Physics(level);

#ifdef DEBUG_SIMULATOR
	snprintf(sim_footer,SIM_FOOTER_SIZE, "fps: %d  ips: %d", 0, 0);
#endif
}

GUI_Simulator::~GUI_Simulator()
{
	delete back_button;
	delete faster_button;
	delete very_fast_button;
	delete slower_button;
	delete level_name_button;
	delete pause_button;
	delete pause_broken_button;
	delete level_passed_button;
	delete next_level_button;
	delete action_cam_button;
	delete physics;
}

void GUI_Simulator::draw_broken_springs()
{
	for(size_t i = 0; i < broken_springs.size(); i++){
		Spring* s = broken_springs[i];
		sdl->draw_broken_spring(s->particles.first->pos.x, s->particles.first->pos.y, s->particles.second->pos.x, s->particles.second->pos.y, 2, Settings::singleton()->sdlc_black);
	}
}

void GUI_Simulator::physics_draw()
{
	// draw train //////////////////////////////////////////////////////////////////
	float_32 vx[Locomotive::points_num];
	float_32 vy[Locomotive::points_num];
	SDL_Color tc = Settings::singleton()->sdlc_train;
	for(int i=0; i <  physics->train->num_loc; ++i) {
		for(int j=0; j<Locomotive::points_num; ++j) {
			vx[j] = physics->train->locomotives[i]->points[j]->pos.x;
			vy[j] = physics->train->locomotives[i]->points[j]->pos.y;
		}
		sdl->draw_train(vx, vy, Locomotive::points_num, tc);
	}

	for(int i=0; i< physics->train->num_car; ++i) {
		for(int j=0; j<Carriage::points_num; ++j) {
			vx[j] =physics->train->carriages[i]->points[j]->pos.x;
			vy[j] =physics->train->carriages[i]->points[j]->pos.y;
		}
		sdl->draw_train(vx, vy, Carriage::points_num, tc);
	}

	// draw springs //////////////////////////////////////////////////////////////////
	float_32 line_width = Settings::singleton()->spring_line_width_sim;

	// draw all springs
	SDL_Color deck_outline_color = Settings::singleton()->sdlc_deck;
	for (Spring_iters i = level->sim_springs.begin(); i != level->sim_springs.end(); ++i)
	{
		if((i)->status == Spring::NORMAL)
		{
			// don't draw train parts
#ifndef DEBUG_SIMULATOR
			if ( (i)->type == Spring::BRIDGE || (i)->type == Spring::DECK)
#endif
			{
				// Draw deck with deck_color outline
				if( (i)->type == Spring::DECK )
				{
					// this does not work with big lines!
				//	sdl->draw_spring( (i)->particles.first->pos.x, (i)->particles.first->pos.y,
				//		(i)->particles.second->pos.x, (i)->particles.second->pos.y, line_width+line_width/2, deck_outline_color);
					const float ax = (i)->particles.first->pos.x;
					const float ay = (i)->particles.first->pos.y;
					const float bx = (i)->particles.second->pos.x;
					const float by = (i)->particles.second->pos.y;
							{ // FIXME!!!
								// draw two parallel lines
								float pixel_distance_between_lines = line_width * 0.5f;
								float_32 distance_in_world = float_32(pixel_distance_between_lines) / (sdl->get_view_zoom()) ;

								Vector2d line_vec = Vector2d(ax, ay) - Vector2d(bx, by);
								Vector2d normal = Math_utils::get_normal(line_vec);
								// get normal with lenght 1
								normal.normalize();
								// make normal to length of distance_in_world
								normal *= distance_in_world ;

								// first line (plus normal)
								float_32 px1 = ax + normal.x;
								float_32 py1 = ay + normal.y;
								float_32 px2 = bx + normal.x;
								float_32 py2 = by + normal.y;
							sdl->draw_spring(px1, py1, px2, py2, line_width, deck_outline_color);

								// second line (minus normal)
								float_32 mx1 = ax - normal.x;
								float_32 my1 = ay - normal.y;
								float_32 mx2 = bx - normal.x;
								float_32 my2 = by - normal.y;
							sdl->draw_spring(mx1, my1, mx2, my2, line_width, deck_outline_color);

							}
				}
				// coloring depends on pressure
				float_32 color = (i)->get_cur_color();
				SDL_Color sdl_color;
				if (color > 0.0f)
				{
					// pull color
					sdl_color = {0, Uint8((1.0f - color)*255.0f), Uint8(color*255.0f), 255};
				}
				else
				{
					// push color
					sdl_color = {Uint8(-color*255.0f), Uint8((1.0f + color)*255.0f), 0, 255};
				}
				sdl->draw_spring( (i)->particles.first->pos.x, (i)->particles.first->pos.y,
					(i)->particles.second->pos.x, (i)->particles.second->pos.y, line_width, sdl_color);
			}
		}
	}
}

void GUI_Simulator::debug_draw()
{
	// debug footer line (alpha 254 to prevent strange SDL flicker
	SDL_Color debug_color = {255, 255, 255, 254};
#ifdef DEBUG_SIMULATOR
	sdl->draw_string(5.0f, Settings::singleton()->window_height - 9, sim_footer, debug_color);
#endif

	// draw all particles ////////////////////////////////////////////////////////////////
	for (Particle_iters i = level->sim_particles.begin(); i != level->sim_particles.end(); ++i)
	{
#ifdef DEBUG_FORCES
		SDL_Color black = {0,0,0, 255};
		SDL_Color white = {255,255,255,255};
		sdl->draw_spring((i)->pos.x, (i)->pos.y,(i)->pos.x + (i)->debug_force_1.x ,(i)->pos.y + (i)->debug_force_1.y , 4, black);
		sdl->draw_spring((i)->pos.x, (i)->pos.y,(i)->pos.x + (i)->debug_force_2.x ,(i)->pos.y + (i)->debug_force_2.y , 2, white);
		(i)->debug_force_1 = {0, 0};
		(i)->debug_force_2 = {0, 0};
#endif
		float_32 radius = std::fmax(Settings::singleton()->particle_radius_edit / 2.0f, 2.0f);
		if((i)->fixed){
			SDL_Color fixed_particle_color = {Uint8(0.8f*255.0f), Uint8(0.4f*255.0f), Uint8(0.4f*255.0f), 255};
			sdl->draw_particle((i)->pos.x, (i)->pos.y, radius, fixed_particle_color);
		}
		else{
			SDL_Color particle_color = {Uint8(0.8f*255.0f), Uint8(0.8f*255.0f), Uint8(0.8f*255.0f), 255};
			sdl->draw_particle((i)->pos.x, (i)->pos.y, radius, particle_color );
		}
	}

	SDL_Color line_color = {0,0,0, 100};
	sdl->draw_spring(0, sdl->screen_to_world_y(0), 0, -200.0f, 1, line_color);
	sdl->draw_spring((float_32)level->width, sdl->screen_to_world_y(0), (float_32) level->width, -200.0f, 1, line_color);

// draw paricle properties under coursor
	const float_32 mouse_w_x = sdl->screen_to_world_x(Settings::singleton()->mouse_x);
	const float_32 mouse_w_y = sdl->screen_to_world_y(Settings::singleton()->mouse_y);

	const Vector2d mouse_pos = Vector2d(mouse_w_x, mouse_w_y);

	// find nearest particle
	Particle* nearest_particle = nullptr;
	float_32 min_dist_square = FLT_MAX;
	for(Particle_iters i = level->sim_particles.begin(); i != level->sim_particles.end(); ++i)
	{
		if( Vector2d::distance_square(mouse_pos, i->pos) < min_dist_square)
		{
			nearest_particle = &(*i);
			min_dist_square = Vector2d::distance_square(mouse_pos, i->pos);
		}
	}

	if( nearest_particle != nullptr) // only happens if level is without a particle
	{
		if(Vector2d::distance_square( mouse_pos, nearest_particle->pos) < 5 * 5)
		{
			const SDL_Color debug_color = {0, 0, 0, 254};
			char particle_id[20];
			std::snprintf(particle_id, 20, "id: %d", nearest_particle->id);
			sdl->draw_string(Settings::singleton()->mouse_x + 10 ,Settings::singleton()->mouse_y - 10, particle_id, debug_color);
			char links[20];
			std::snprintf(links, 20, "links: %d", nearest_particle->spring_links);
			sdl->draw_string(Settings::singleton()->mouse_x + 10 ,Settings::singleton()->mouse_y + 5, links, debug_color);
		}
	}
}

void GUI_Simulator::draw()
{
	sdl->draw_background(Settings::singleton()->sdlc_background_simulator);

	this->physics_draw();

	if(animation_speed == 0 && broken_springs.size() > 0)
		this->draw_broken_springs();

	sdl->draw_water(level->water_height, Settings::singleton()->sdlc_water);
	sdl->draw_ground(level->ground, level->width, Settings::singleton()->sdlc_ground);

	// draw all buttons
	sdl->draw_button(back_button);
	sdl->draw_button(very_fast_button);
	sdl->draw_button(faster_button);
	sdl->draw_button(slower_button);
	sdl->draw_button(level_name_button);
	sdl->draw_button(action_cam_button);

	if(animation_speed == 0) pause_button->set_highlight(true);
	else pause_button->set_highlight(false);
	sdl->draw_button(pause_button);

	if(pause_on_broken_spring) pause_broken_button->set_highlight(true);
	else pause_broken_button->set_highlight(false);
	sdl->draw_button(pause_broken_button);

	if(level->level_passed){
		level_passed_button->set_highlight(true);
		sdl->draw_button(level_passed_button);
		next_level_button->set_highlight(true);
		sdl->draw_button(next_level_button);
	}
#ifdef DEBUG_SIMULATOR
	this->debug_draw();
#endif
}

void GUI_Simulator::update()
{
	// to draw fps and ips if animation_speed == 0
	bool extra_draw = false;
	timeval tv;
	gettimeofday(&tv, NULL);
	if(current_sec != tv.tv_sec) {
		fps = count_frames;
		ips = count_iter;
		count_frames = 0;
		count_iter = 0;
		current_sec = tv.tv_sec;
		extra_draw = true;
	}

	// do multiple physics steps (with fixed timestep = deterministic)
	for( int i=0; i<animation_speed; ++i) {
		unsigned int return_val = physics->update(broken_springs);
#ifdef DEBUG_SIMULATOR
		if( pause_on_broken_spring == true && (return_val == physics->BROKEN_TRAIN_SPRING || return_val == physics->BROKEN_BRIDGE_SPRING ))
#else
		if( pause_on_broken_spring == true && return_val == physics->BROKEN_BRIDGE_SPRING )
#endif
		{
			prev_animation_speed = animation_speed;
			animation_speed = 0;
		}
		count_iter++;
		count_all_iter++;
	}

	if(animation_speed || extra_draw) {
		count_frames++;
	}
#ifdef DEBUG_SIMULATOR
	int train_speed = physics->train->get_speed();
	std::snprintf(sim_footer, SIM_FOOTER_SIZE, "fps %2d, ips %4d, iteration %7lu, p %4lu, s %4lu,  ani_speed %4d, train_speed%4d",
		fps, ips, count_all_iter, level->particles.size(), level->springs.size(), animation_speed, train_speed);
#endif
	if(action_cam){
		Vector2d pos = physics->train->get_pos_for_action_cam();
		sdl->move_to(pos.x, pos.y);
	}
}

void GUI_Simulator::event_mouse_click(GUI_Manager* guimanager, int button, int button_state, int x, int y)
{
	// button down event
	if(!button_state)
	{
		// click on a button?
		GUI_Component* component = 0;
		for (Component_List::iterator i = buttons.begin (); i != buttons.end (); ++i)
		{
			if ((*i)->is_at (x, y))
				component = *i;
		}

		if(component==back_button && button==0) // left click (first mouse button)
		{
			level->copy_max_colors_after_simulation();
			guimanager->currentGUI = GUI_Manager::GUIEDITOR;
			guimanager->event_resize();
		}
		if(component==slower_button && button==0) // left click (first mouse button)
			event_keyboard("-");
		if(component==faster_button && button==0) // left click (first mouse button)
			event_keyboard("+");
		if(component==very_fast_button && button==0) // left click (first mouse button)
			event_keyboard("f");
		if(component==pause_button && button==0)
			event_keyboard("Space"); 	// use keyboard pause
		if(component==action_cam_button && button==0)
			event_keyboard("c"); 	// use keyboard action cam
		if(component==pause_broken_button && button==0)
		{
			pause_on_broken_spring = !pause_on_broken_spring;
			Settings::singleton()->simulator_pause_on_broken = pause_on_broken_spring;
		}

		// not a GUI_Component (automatic draw and click)
		if((next_level_button->is_at(x,y) || level_passed_button->is_at(x,y)) && button==0 && level->level_passed ){
			if(! level->load_failed ) level->save_to_file();
			guimanager->currentGUI = guimanager->GUIMENU_LEVELS;
		}

#ifdef DEBUG_SIMULATOR
		if (button==2){
			ERROR_PRINT("create debug body at x: %f ; y: %f \n", sdl->screen_to_world_x(Settings::singleton()->mouse_x), sdl->screen_to_world_y(Settings::singleton()->mouse_y));
			physics->add_debug_body(sdl->screen_to_world_x(Settings::singleton()->mouse_x), sdl->screen_to_world_y(Settings::singleton()->mouse_y));
		}
#endif
	}
}

void GUI_Simulator::event_keyboard(const char* utf8_key)
{
	// move and zoom is similar for editor and simulator
	GUI_Editorsimulator::event_keyboard(utf8_key);

	std::string utf8_string = utf8_key;
	// animation speed: non linear speed up / down
	if( utf8_string.compare("+")==0) animation_speed++;
	if( utf8_string.compare("+")==0 && animation_speed > 5 ) animation_speed+=4;
	if( utf8_string.compare("+")==0 && animation_speed > 100 ) animation_speed+=95;

	if( utf8_string.compare("-")==0 && animation_speed > 0) animation_speed--;
	if( utf8_string.compare("-")==0 && animation_speed > 9) animation_speed-=4;
	if( utf8_string.compare("-")==0 && animation_speed > 200) animation_speed-=95;

	if( utf8_string.compare("Space")==0 && animation_speed != 0)
	{
		prev_animation_speed = animation_speed;
		animation_speed = 0;
	}
	else
	{
		if( utf8_string.compare("Space")==0 && animation_speed == 0)
		{
			animation_speed = prev_animation_speed;
		}
	}
	if( utf8_string.compare("c")==0) action_cam = !action_cam;
	if( utf8_string.compare("f")==0) animation_speed = 250;
}

void GUI_Simulator::event_window_resize()
{
	for (Component_List::iterator i = buttons.begin (); i != buttons.end (); ++i)
	{
		(*i)->on_window_resize();
	}
	level_name_button->on_window_resize();
	level_passed_button->on_window_resize();
	next_level_button->on_window_resize();
}
