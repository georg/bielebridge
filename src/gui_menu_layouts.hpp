//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_GUI_LAYOUTS_H
#define HEADER_GUI_LAYOUTS_H

class GUI_Component;
class GUI_Button;
class GUI_Manager;
class IDraw_Move_Zoom;

class GUI_Menu_Start
{
private:
	GUI_Button* name_button;
	GUI_Button* start_button;
	GUI_Button* options_button;
	GUI_Button* about_button;
	GUI_Button* quit_button;
	GUI_Button* benchmark_button;
	GUI_Button* benchmark_result_button;

	bool benchmark_scheduled = false;
	bool benchmark_info_drawn = false;

	typedef std::vector<GUI_Button*> Button_list;
	Button_list buttons;
public:
	GUI_Menu_Start();
	~GUI_Menu_Start();
	void draw();
	void event_mouse_click(GUI_Manager* guimanager, int a, int b, int c, int d);
	void event_window_resize();
};

class GUI_Menu_Options
{
private:
	GUI_Button* fullscreen_button;
	GUI_Button* fps_button;
	GUI_Button* fps_plus_button;
	GUI_Button* fps_minus_button;
	GUI_Button* back_button;

	typedef std::vector<GUI_Button*> Button_list;
	Button_list buttons;
public:
	GUI_Menu_Options();
	~GUI_Menu_Options();
	void draw();
	void event_mouse_click(GUI_Manager* guimanager, int a, int b, int c, int d);
	void event_window_resize();
};

class GUI_Menu_About
{
private:
	GUI_Button* background_button;
	GUI_Button* text_button;
	GUI_Button* text2Button;
	GUI_Button* text3Button;
	GUI_Button* text4Button;
	GUI_Button* text5Button;
	GUI_Button* text6Button;
	GUI_Button* text61Button;
	GUI_Button* text7Button;
	GUI_Button* back_button;

	typedef std::vector<GUI_Button*> Button_list;
	Button_list buttons;
public:
	GUI_Menu_About();
	~GUI_Menu_About();
	void draw();
	void event_mouse_click(GUI_Manager* guimanager, int a, int b, int c, int d);
	void event_window_resize();
};

class GUI_Menu_Levels
{
private:
	// varibles for generating level button list
	const int lvl_button_x_pos = 100;
	const int lvl_button_width = 110;
	const int lvl_button_height = 25;
	const int lvl_button_y_distance = 5;
	// for user levels
	const int lvl_button_x_pos_user = 600;
	const int lvl_button_width_user = 300;
	int lvl_button_cur_y_position;

	unsigned int predefined_levels_num = 0;
	unsigned int level_editor_threshold = 0;
	bool create_levels_unlocked = false;

	std::string new_lvl_name = "";

	std::string level_base_path = "./";

	GUI_Button* back_button;
	GUI_Button* new_level_button;
	GUI_Button* tooltip_text;
	GUI_Button* new_lvl_name_text;

	typedef std::vector<GUI_Button*> Button_list;
	Button_list buttons;
	void add_level_button(const std::string& lvl_name);
public:
	GUI_Menu_Levels();
	~GUI_Menu_Levels();
	void draw();
	void event_mouse_click(GUI_Manager* guimanager, int a, int b, int c, int d);
	void event_keyboard(const char* utf8_key);
	void event_window_resize();
};


#endif

