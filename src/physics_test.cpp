//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <ctime>
#include <sys/time.h>
#include <cstdio>
#include <csignal>
#include <iostream>
#include <fstream>
#include "vector2d.hpp"
#include "level.hpp"
#include "sdl_draw.hpp"
#include "physics_test.hpp"
#include "train.hpp"
#include "error.hpp"
#include "spring.hpp"
#include "particle.hpp"
#include "configure.cmake.hpp"

Physic_Test::Physic_Test()
{
	std::string full_path = DATADIR "/" + fname_benchmark;

	level = new Level(0, "physicstest", full_path.c_str());
	if(!level->load_failed)
		INFO_PRINT("Loaded physics_test level: %s\n", full_path.c_str());
	else
	{
		ERROR_PRINT("failed to load physics_test level: %s\n", full_path.c_str());

		// test local path
		full_path = "./data/" + fname_benchmark;
		if( access( full_path.c_str(), R_OK ) != -1 )
		{
			// file exists and is readable, try it
			delete level;
			level = new Level(0, "benchmark", full_path.c_str());
			if(!level->load_failed)
				INFO_PRINT("Loaded benchmark level: %s\n", full_path.c_str());
			else
				ERROR_PRINT("failed to load benchmark level: %s\n", full_path.c_str());
		}
	}

	level->copy_for_simulation();
	physics = new Physics(level);
}


// runs about "sec_to_run" seconds
// return iterations per second
void Physic_Test::run_test_for_SIGSEGV()
{
	if(level->load_failed)
	{
		ERROR_PRINT("Couldn't load physics_test level \"%s\". Nothing to do.\n", fname_benchmark.c_str());
		return;
	}

	// some debug output
	printf("Make a physic test...\n");
	unsigned long long count_all_iter = 0;
	unsigned int count_dbb = 0;
	// dummy vector
	std::vector<Spring*> broken_springs;

	srand(12345);
	while(true){
		physics->update(broken_springs);
		if( (count_all_iter++ % 1000) == 0)
		{
			printf("iteration: %llu debug bodies: %d\n", count_all_iter, count_dbb); // check train (intact carriages/springs)
		}

		if( (count_all_iter % 100) == 0)
		{
			int decx = rand() % 1000;
			int rx = (rand() % 1550) + 50;
			int decy = rand() % 1000;
			int ry = (rand() % 475) + 25;
			float_32 x = float_32(rx) + float_32(decx) / 1000.0f;
			float_32 y = float_32(ry) + float_32(decy) / 1000.0f;
			physics->add_debug_body( x, y);
			count_dbb++;
		}
	}
}

Physic_Test::~Physic_Test()
{
	delete physics;
	delete level;
}

