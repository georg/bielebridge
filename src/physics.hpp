//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HEADER_PHYSICS_H
#define HEADER_PHYSICS_H

#include <vector>
#include "vector2d.hpp"
#include "spring.hpp"
#include "particle.hpp"
#include "math_utils.hpp"
#include "error.hpp"

class Level;
class Spring;
class Train;
class Particle;
class GUI_Editorsimulator;

// Axis Aligned Bounding Box
class AABB
{
public:
	float_32 min_x;
	float_32 max_x;
	float_32 min_y;
	float_32 max_y;

	AABB(){}

	bool point_inside(const Vector2d& p) const
	{
		return min_x < p.x && p.x < max_x  &&  min_y < p.y && p.y < max_y;
	}

	// this function returns true for some lines with no point inside AABB
	bool line_inside(const Vector2d& p1, const Vector2d& p2) const
	{
		if( (p1.x > max_x && p2.x > max_x)    // both points right outside AABB
			|| (p1.x < min_x && p2.x < min_x) // both points left outside AABB
			|| (p1.y > max_y && p2.y > max_y) // both points above AABB
			|| (p1.y < min_y && p2.y < min_y))// both points below AABB
		{
			return false;
		}
		else
		{
			// maybe do some additional checks here to avoid false positivs
			return true;
		}
	}
};

class Physics
{
	friend class GUI_Editor;
	friend class GUI_Simulator;
	friend class GUI_Editorsimulator;
	friend class Benchmark;
	friend class Train;
	friend class Level;

private:
#ifdef DEBUG	// DEBUG
	unsigned int debug_col_count = 0;
	unsigned int debug_fastcol_count = 0;
	unsigned int debug_fastcol_ret[20];
#endif

	// maximal collisions with springs per carriage (have to be smaller as 256) at once
	static const int MAX_COL_SPRINGS = 64;

	/** 1.0 for no damping; 0.0 for perfect plastic  */
//	const float_32 particle_collision_damping = 0.8f;
	const Vector2d gravity_vec = {0.0f, -15.0f};
	typedef std::vector<Spring*>::iterator Spring_iter;
	typedef std::vector<Spring>::iterator Spring_iters;
	typedef std::vector<Particle*>::iterator Particle_iter;
	typedef std::vector<Particle>::iterator Particle_iters;

	// important: use a fixed TIME_STEP; this (hopefully) produces determinism
	// changing TIME_STEP will change physics simulation
	const float TIME_STEP = 0.001f;
	uint64_t iteration_count = 0;

	Level* level;
	// in simulation mode we need a train
	// only instanced by the copy constructor
	Train* train;

	bool check_level_passed();

	void collide_ground_points_inside_train();
	void collide_ground_with_polygon(Particle** train_points, int train_points_num, Spring** train_springs);

	////////////////////////////////////////////////////////////////////////////////
	// Collision train with deck
	// update(std::vec...) calls
	void collide_deck_with_train();
	// calls
	void aabb_col_deck_with_polygon(Particle** train_points, uint_fast8_t train_points_num, Spring** train_springs);
	// calls
	void col_deck_with_polygon(Particle** train_points, uint_fast8_t train_points_num, Spring** train_springs, Spring** col_springs, uint_fast8_t col_springs_num);
	// calls
	void create_subdiv_points(const Vector2d& base, const Vector2d& line, Vector2d subdiv_points[], int& subdiv_points_num) const;
	// and
	void calc_and_apply_collision_forces(Spring** train_springs, Particle** poly_particles, const int poly_vertices_num, const Vector2d& subdiv_point_inside, Spring* col_deck_spring);

	void bounce_particle(Particle& p);
	void train_add_forward_force();

	void get_nearest_ground_line_segment(const Vector2d point, Vector2d& ground_left, Vector2d& ground_right);


public:
	// collision constants:
	//   distance between subdivision collision points (large for speed, small for accuracy)
	// const float_32 SUBDIV_LEN = 1.0f; // is now implicit

	/// warning this puts an array on the stack (that will be corrupted if choosen too small)
	// at least ((settings->max_spring_len / subdiv_len) + 2) * ( 1 + settings->spring_max_stretch)
	static const int MAX_SPRING_SUBDIVS = 96;

	// Bitmask values for update() return value
	const unsigned int BROKEN_BRIDGE_SPRING = 1<<1;
	const unsigned int BROKEN_TRAIN_SPRING  = 1<<2;

#ifdef DEBUG
	// to draw collision points
	Vector2d* collision_points;
	int collision_points_num;
#endif

	Physics(Level* level);
	~Physics();
	unsigned int update(std::vector<Spring*>& broken_springs);
	void add_debug_body(float_32 x, float_32 y);
};

#endif
