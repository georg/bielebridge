//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

// opengl headers
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_image.h>

#include "settings.hpp"
#include "sdl_draw.hpp"
#include "sdl_utils.hpp"
#include "error.hpp"
#include "level.hpp"
#include "math_utils.hpp"
#include "vector2d.hpp"
#include "gui_button.hpp"

void SDL_draw::draw_background(SDL_Color c)
{
	SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
	// clear screen with color c
	SDL_RenderClear(renderer);
}

void SDL_draw::draw_background_image(std::string filename)
{
	// not implemented yet
	SDL_Color c = {0, 0, 0, 0xff};
	this->draw_background(c);
}

void SDL_draw::draw_particle(float_32 x, float_32 y, float radius , SDL_Color c)
{
	// paticle ist intentionaly not round

	// calculate screen coordinates
	int sx = world_to_screen_x(x);
	int sy = world_to_screen_y(y);
	int sr = int( radius * 0.0006f * get_view_zoom() + 2.0f);

	// do not draw particles outside of window
	int wwidth = Settings::singleton()->window_width;
	int wheight = Settings::singleton()->window_height;
	if(sx < -sr || sy < -sr || sx > wwidth + sr || sy > wheight + sr)
		return;

	// set color
	SDL_SetRenderDrawColor(renderer, (Uint8) c.r, (Uint8) c.g, (Uint8) c.b, (Uint8) c.a);

	SDL_Rect p_rect;
	p_rect.x = sx - sr;
	p_rect.y = sy - sr;
	p_rect.w = 2 * sr;
	p_rect.h = 2 * sr;

	SDL_RenderFillRect(renderer, &p_rect);
}

void SDL_draw::draw_spring(float_32 ax, float_32 ay, float_32 bx, float_32 by, float line_width, SDL_Color c)
{
	// calculate screen coordinates
	Sint16 sx1 = (Sint16) world_to_screen_x(ax);
	Sint16 sy1 = (Sint16) world_to_screen_y(ay);
	Sint16 sx2 = (Sint16) world_to_screen_x(bx);
	Sint16 sy2 = (Sint16) world_to_screen_y(by);

	// do not draw lines outside of window
	int wwidth = Settings::singleton()->window_width;
	int wheight = Settings::singleton()->window_height;
	if((sx1 < 0 && sx2 < 0) || (sy1 < 0 && sy2 < 0) || (sx1 > wwidth && sx2 > wwidth) || (sy1 > wheight && sy2 > wheight))
		return;

	if( line_width < 2.0f ) {
		aalineRGBA(renderer, sx1, sy1, sx2, sy2, (Sint8) c.r, (Uint8) c.g, (Uint8) c.b, (Uint8) c.a);
	}
	else {
		thickLineRGBA(renderer, sx1, sy1, sx2, sy2, (Uint8) line_width,
					(Uint8) c.r, (Uint8) c.g, (Uint8) c.b, (Uint8) c.a);
	}
}

void SDL_draw::draw_broken_spring(float_32 ax, float_32 ay, float_32 bx, float_32 by, float line_width, SDL_Color c)
{
	// draw two parallel lines
	int pixel_distance_between_lines = int(line_width * 2);
	float_32 distance_in_world = float_32(pixel_distance_between_lines) / (get_view_zoom()) ;

	Vector2d line_vec = Vector2d(ax, ay) - Vector2d(bx, by);
	Vector2d normal = Math_utils::get_normal(line_vec);
	// get normal with length 1
	normal.normalize();
	// make normal to length of distance_in_world
	normal *= distance_in_world ;

	// first line (plus normal)
	float_32 px1 = ax + normal.x;
	float_32 py1 = ay + normal.y;
	float_32 px2 = bx + normal.x;
	float_32 py2 = by + normal.y;
	Sint16 sx1 = (Sint16) world_to_screen_x(px1);
	Sint16 sy1 = (Sint16) world_to_screen_y(py1);
	Sint16 sx2 = (Sint16) world_to_screen_x(px2);
	Sint16 sy2 = (Sint16) world_to_screen_y(py2);

	// second line (minus normal)
	float_32 mx1 = ax - normal.x;
	float_32 my1 = ay - normal.y;
	float_32 mx2 = bx - normal.x;
	float_32 my2 = by - normal.y;
	Sint16 tx1 = (Sint16) world_to_screen_x(mx1);
	Sint16 ty1 = (Sint16) world_to_screen_y(my1);
	Sint16 tx2 = (Sint16) world_to_screen_x(mx2);
	Sint16 ty2 = (Sint16) world_to_screen_y(my2);


	if( line_width < 2.0f ) {
		aalineRGBA(renderer, sx1, sy1, sx2, sy2, (Sint8) c.r, (Uint8) c.g, (Uint8) c.b, (Uint8) c.a);
		aalineRGBA(renderer, tx1, ty1, tx2, ty2, (Sint8) c.r, (Uint8) c.g, (Uint8) c.b, (Uint8) c.a);
	}
	else {
		thickLineRGBA(renderer, sx1, sy1, sx2, sy2, (Uint8) line_width, (Uint8) c.r, (Uint8) c.g, (Uint8) c.b, (Uint8) c.a);
		thickLineRGBA(renderer, tx1, ty1, tx2, ty2, (Uint8) line_width, (Uint8) c.r, (Uint8) c.g, (Uint8) c.b, (Uint8) c.a);
	}
}

void SDL_draw::draw_ground(const float_32 ground[], int level_width, SDL_Color c)
{
	Sint16* vx_ground;
	Sint16* vy_ground;
	vx_ground = new Sint16[(level_width/5) + 6];
	vy_ground = new Sint16[(level_width/5) + 6];
	// calculate screen coordinates
	int i, g = 0;
	for( i = 0; i <  level_width; i+=5, ++g ) {
		vx_ground[g] = (Sint16) world_to_screen_x(float_32(i));
		vy_ground[g] = (Sint16) world_to_screen_y(ground[g]);
	}

	// end of level at 0 height
	vx_ground[g] = (Sint16) world_to_screen_x((float_32) i );
	vy_ground[g] = (Sint16) world_to_screen_y(0);

	// 0 height at right edge
	vx_ground[g+1] = (Sint16) Settings::singleton()->window_width;
	vy_ground[g+1] = (Sint16) world_to_screen_y(0);
	//ERROR_PRINT("g: [%d,%d]  g+1: [%d,%d]\n", vx_ground[g], vy_ground[g], vx_ground[g+1], vy_ground[g+1]);

	// vertex rigth bottom edge
	vx_ground[g+2] = (Sint16) Settings::singleton()->window_width;
	vy_ground[g+2] = (Sint16) Settings::singleton()->window_height;

	// vertex left bottom
	vx_ground[g+3] = 0;
	vy_ground[g+3] = (Sint16) Settings::singleton()->window_height;

	// left of level edge: draw line at height 0
	vx_ground[g+4] = 0;
	vy_ground[g+4] = (Sint16) world_to_screen_y(0);

	// draw line at height 0 to level begin (ground[0])
	vx_ground[g+5] = (Sint16) world_to_screen_x(-5.0f);
	vy_ground[g+5] = (Sint16) world_to_screen_y(0);

	// this is a hack to get at least a bit of anti-aliasing
	// first draw an aapolygon (not filled) and after that a filled polygon
	//
	// hint: this SDL2 gfx function uses internal _aalineRGBA
	// which accepts not closed polygons
	aapolygonRGBA(renderer, vx_ground, vy_ground, g+4, c.r, c.g, c.b, c.a); // only g+4 because of SDL2_gfx bug?

	int failure = filledPolygonRGBA(renderer, vx_ground, vy_ground, g+6, c.r, c.g, c.b, c.a);
	if( failure ) {
		ERROR_PRINT("Error: filledPolygonRGBA failed with return value %d\n", failure);
	}
	delete vx_ground;
	delete vy_ground;
}

void SDL_draw::draw_water(float_32 water_height, SDL_Color c)
{
	// calculate screen coordinates
	SDL_Rect water_rect;
	water_rect.x = 0;
	water_rect.y = world_to_screen_y(water_height);
	water_rect.w = Settings::singleton()->window_width;
	water_rect.h = Math_utils::max(0, Settings::singleton()->window_height - water_rect.y);

	// set blend mode (transparency)
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

	// water color
	SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);

	SDL_RenderFillRect(renderer, &water_rect);
}

void SDL_draw::draw_ground_editbox(float_32 x, float width, float_32 ground_height, SDL_Color box_c, SDL_Color ground_c)
{
	// calculate screen coordinates
	int pos_x1 = world_to_screen_x(x);
	int pos_x2 = world_to_screen_x(x + width);

	// box full height
	SDL_SetRenderDrawColor(renderer, box_c.r, box_c.g, box_c.b, box_c.a);
	SDL_Rect box;
	box.x = pos_x1;
	box.y = 0;
	box.w = pos_x2 - pos_x1;
	box.h = Settings::singleton()->window_height;
	SDL_RenderFillRect(renderer, &box);

	// box ground height
	SDL_SetRenderDrawColor(renderer, ground_c.r, ground_c.g, ground_c.b, ground_c.a);
	box.x = pos_x1;
	box.y = world_to_screen_y(ground_height);
	box.w = pos_x2 - pos_x1;
	box.h = Settings::singleton()->window_height - world_to_screen_y(ground_height);
	SDL_RenderFillRect(renderer, &box);
}

void SDL_draw::draw_string(int x, int y, const char* str, SDL_Color c)
{
	stringRGBA(renderer, (Sint16) x, (Sint16) y, str, (Uint8) c.r, (Uint8) c.g, (Uint8) c.b, (Uint8) c.a);
}

void SDL_draw::draw_grid(const SDL_Color c_most, const SDL_Color c_some)
{
	int x1, y1, x2, y2;
	// space between lines (in world size); world_to_screen() will scale with correct zoom
	const int GRID_SPACE = Settings::singleton()->grid_spacing;

	// set color for most grid lines
	SDL_SetRenderDrawColor(renderer, c_most.r, c_most.g, c_most.b, c_most.a);

	// vertical lines
	int xstart = Math_utils::round_to(float_32(- get_view_x_offset()), GRID_SPACE);
	int xstop = int( screen_to_world_x(Settings::singleton()->window_width));
	for(int x = xstart; x <= xstop; x+= GRID_SPACE)
	{
		if(x % 80 == 0)
			SDL_SetRenderDrawColor(renderer, c_some.r, c_some.g, c_some.b, c_some.a);

		x1 = world_to_screen_x(float_32(x));
		y1 = 0;
		x2 = world_to_screen_x(float_32(x));
		y2 = Settings::singleton()->window_height;
		SDL_RenderDrawLine(renderer, x1, y1, x2, y2);

		// set color back to most
		if(x % 80 == 0)
			SDL_SetRenderDrawColor(renderer, c_most.r, c_most.g, c_most.b, c_most.a);

	}

	// horizontal lines
	int ystart = Math_utils::round_to(float_32(get_view_y_offset()), GRID_SPACE);
	int ystop = int( screen_to_world_y(Settings::singleton()->window_height));
	for(int y = ystart; y >= ystop; y-= GRID_SPACE)
	{
		if(y % 80 == 0)
			SDL_SetRenderDrawColor(renderer, c_some.r, c_some.g, c_some.b, c_some.a);

		x1 = 0;
		y1 = world_to_screen_y(float_32(y));
		x2 = Settings::singleton()->window_width;
		y2 = world_to_screen_y(float_32(y));
		SDL_RenderDrawLine(renderer, x1, y1, x2, y2);

		if(y % 80 == 0)
			SDL_SetRenderDrawColor(renderer, c_most.r, c_most.g, c_most.b, c_most.a);
	}

	//additional small lines between black ones
	SDL_SetRenderDrawColor(renderer, c_some.r, c_some.g, c_some.b, c_some.a);
	for(int x = xstart; x <= xstop; x+= GRID_SPACE)
	{
		for(int y = ystart; y >= ystop; y-= GRID_SPACE)
		{
			if((x + 40) % 80 == 0 && y % 80 == 0) {
				x1 = world_to_screen_x(float_32(x));
				y1 = world_to_screen_y(float_32(y-2));
				x2 = world_to_screen_x(float_32(x));
				y2 = world_to_screen_y(float_32(y+2));
				SDL_RenderDrawLine(renderer, x1, y1, x2, y2);

				x1 = world_to_screen_x(float_32(x+40-2));
				y1 = world_to_screen_y(float_32(y-40));
				x2 = world_to_screen_x(float_32(x+40+2));
				y2 = world_to_screen_y(float_32(y-40));
				SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
			}
		}
	}
}

void SDL_draw::draw_train(const float_32 *vx, const float_32 *vy, const int n, const SDL_Color tc)
{
	Sint16* sint_vx = new Sint16[n];
	Sint16* sint_vy = new Sint16[n];

	for(int i=0; i<n; ++i) {
		sint_vx[i] = (Sint16) this->world_to_screen_x(vx[i]);
		sint_vy[i] = (Sint16) this->world_to_screen_y(vy[i]);
	}

	int failure = filledPolygonRGBA(renderer, sint_vx, sint_vy, n, tc.r, tc.g, tc.b, tc.a);
	if( failure ) {
		ERROR_PRINT("filledPolygonRGBA failed with %d\n", failure);
	}

	delete[] sint_vx;
	delete[] sint_vy;
}

void SDL_draw::draw_button(GUI_Button* button)
{
	if( ! button->get_visible())
		return;

	SDL_Rect rect = { button->get_x_pos(), button->get_y_pos(), button->get_width(), button->get_height() };
	std::string text = button->get_text().c_str();

	// if button has a background
	if(button->get_background())
	{
		// draw Box
		SDL_Color color;
		if(button->get_highlight())
			color = button->get_highlight_color();
		else
			color = Settings::singleton()->sdlc_button;

		SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
		SDL_RenderFillRect(renderer, &rect);
	}

	// Draw Text
	//
	// is texture already rendered?
	SDL_Texture* text_as_image;
	if(button->get_sdl_texture() == nullptr || button->get_need_rerender())
	{
		// free old texture if present
		if(button->get_sdl_texture() != nullptr)
			SDL_DestroyTexture(button->get_sdl_texture());
		// render it
		int font_size = Settings::singleton()->button_font_size;
		// TODO search for font, if not in given path
		std::string font_filename(Settings::singleton()->full_path_to_font_file);
		text_as_image = this->render_text(text, font_filename, button->get_text_color(), font_size);
		if (text_as_image != nullptr)
		{
			// save texture
			button->set_sdl_texture(text_as_image);
			button->set_need_rerender(false);
		}
	}
	else
	{
		text_as_image = button->get_sdl_texture();
	}

	if (text_as_image != nullptr)
	{
		int image_width, image_height;
		//Get the texture w/h so we can center it in the screen
		SDL_QueryTexture(text_as_image, NULL, NULL, &image_width, &image_height);

		// FIXME: this is a hack to force SDL_RENDERER_SOFTWARE to nice looking text
		SDL_SetTextureAlphaMod(text_as_image, 254);

		SDL_Rect dst;
		dst.x = rect.x - image_width/2 + rect.w/2;
		dst.y = rect.y - image_height/2 + rect.h/2;
		dst.w = image_width;
		dst.h = image_height;
		int sdlret = SDL_RenderCopy(renderer, text_as_image, NULL, &dst);
		if (sdlret != 0)
		{
			SDLERROR_PRINT("Couldn't copy texture from button: %s\n",text.c_str());
		}
	}

#ifdef DEBUG
	int circle_radius_in_px = 3;
	SDL_Color colord = {200, 10, 10, 255};
	filledCircleRGBA(renderer, (Sint16) rect.x, (Sint16) rect.y, (Sint16) circle_radius_in_px,
			colord.r, colord.b, colord.g, colord.a);
#endif
}

void SDL_draw::draw_rect(const float_32 x, const float_32 y, const float_32 width, const float_32 height, const SDL_Color c)
{
	SDL_Rect rect;
	rect.x = this->world_to_screen_x(x);
	rect.y = this->world_to_screen_y(y);
	rect.w = int(width * this->get_view_zoom());
	rect.h = -int(height * this->get_view_zoom());
	SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
	SDL_RenderDrawRect(renderer, &rect);
}

void SDL_draw::draw_pentagram(const float_32 x, const float_32 y, const float_32 width, const SDL_Color c)
{
	Vector2d center = Vector2d(x, y + width/2.0f);
#define PI 3.141592653589793f
	Sint16* sint_vx = new Sint16[20];
	Sint16* sint_vy = new Sint16[20];

	const float size = (float) width/4.5f;
	const float length = size*2.0f;  // shrunken pentagon; regular with 2.618f
	const float radian = 2.0f*PI/5.0f;
	// don't start at 0 (rotation)
	int n = 0;
	for(double i = -0.75f; i < 4.25f; i += 1.0f )
	{
		sint_vx[n] = (Sint16) (center.x + size*cos(i*radian));
		sint_vy[n] = (Sint16) (center.y + size*sin(i*radian));
		n++;
		sint_vx[n] = (Sint16) (center.x + length*cos(i*radian + PI/5));
		sint_vy[n] = (Sint16) (center.y + length*sin(i*radian + PI/5));
		n++;
		sint_vx[n] = (Sint16) (center.x + size*cos((i+1)*radian));
		sint_vy[n] = (Sint16) (center.y + size*sin((i+1)*radian));
		n++;
		sint_vx[n] = (Sint16) (center.x + length*cos(i*radian + PI/5));
		sint_vy[n] = (Sint16) (center.y + length*sin(i*radian + PI/5));
		n++;
	}
	int failure = filledPolygonRGBA(renderer, sint_vx, sint_vy, n, c.r, c.g, c.b, c.a);
	if( failure ) {
		ERROR_PRINT("filledPolygonRGBA failed with %d\n", failure);
	}
	delete[] sint_vx;
	delete[] sint_vy;
}

SDL_Texture* SDL_draw::render_text(const std::string& text, const std::string& font_filename, const SDL_Color c, const int font_size)
{
	TTF_Font *ttf_font = TTF_OpenFont(font_filename.c_str(), font_size);
	if (ttf_font == nullptr)
	{
		// FIXME test this with wrong font file...
		SDLERROR_PRINT("Couldn't open font file \'%s\'\n", font_filename.c_str());
		return nullptr;
	}

	// first get a raster graphics in RAM (surface)
	SDL_Surface *tmp_s = TTF_RenderUTF8_Blended(ttf_font, text.c_str(), c);
	if (tmp_s == nullptr){
		TTF_CloseFont(ttf_font);
		SDLERROR_PRINT("Couldn't render text: %s\n", text.c_str());
		return nullptr;
	}

	// get texture (Graphics Cards Memory)
	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, tmp_s);
	if (texture == nullptr){
		SDLERROR_PRINT("Couldn't create texture: %s\n", text.c_str());
	}

	// Clean up
	SDL_FreeSurface(tmp_s);
	TTF_CloseFont(ttf_font);

	return texture;
}

////////////////////////////////////////////////////////////////////////////////
// OpenGL draw class ///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void SDL_opengl_draw::draw_background(SDL_Color c)
{
	// background color
	glClearColor(float(c.r)/255.0f, float(c.g)/255.0f, float(c.b)/255.0f, float(c.a)/255.0f);
	// paint it
	glClear(GL_COLOR_BUFFER_BIT );
}

void SDL_opengl_draw::draw_background_image(std::string filename)
{
	if( ! this->opengl_bg_texture)
	{
		// SDL_Texture * IMG_LoadTexture(SDL_Renderer *renderer, const char *file);
		SDL_Surface* tmp_surface = IMG_Load(filename.c_str());
		if (!tmp_surface)
		{
			ERROR_PRINT("IMG_LoadTexture: Couldn't open image '%s'\n", filename.c_str());
			// just in case: draw a black background first
			SDL_Color black = {0, 0, 0, 0};
			this->draw_background(black);
			return;
		}
		else
		{
			GLuint return_tex = 0;
			// create new texture, with default filtering state (==mipmapping on)
			glGenTextures( 1, &return_tex );
			glBindTexture( GL_TEXTURE_2D, return_tex );

			// disable midmapping on the new texture
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, tmp_surface->w, tmp_surface->h, 0, GL_RGB, GL_UNSIGNED_BYTE, tmp_surface->pixels );

			this->opengl_bg_texture = return_tex;
			SDL_FreeSurface(tmp_surface);
		}
	}

	// background image w/h in pixels
	float bg_img_w = 2400.0f;
	float bg_img_h = 1350.0f;
	// draw to screen
	float x = 0;
	float y = 0;
	float xw = (float) Settings::singleton()->window_width;
	float yh = (float) Settings::singleton()->window_height;
	float screen_ratio = float(xw) / float(yh);
	float bg_img_ratio = bg_img_w / bg_img_h;

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	// set texture color
	SDL_Color color = {0xff, 0xff, 0xff, 0xff};
	glColor4ub(color.r, color.g, color.b, color.a);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, this->opengl_bg_texture );
	glBegin(GL_QUADS);
		// screen is wider than 16:9 ... rare condition
		if( screen_ratio > bg_img_ratio )
		{
			// for full screen image squeezed to screen ratio
			glTexCoord2f( 0.0f, 0.0f ); glVertex2f(x,  y );
			glTexCoord2f( 1.0f, 0.0f ); glVertex2f(xw, y );
			glTexCoord2f( 1.0f, 1.0f ); glVertex2f(xw, yh);
			glTexCoord2f( 0.0f, 1.0f ); glVertex2f(x,  yh);
		}
		else
		{
			float zoom_factor = bg_img_h / yh;
			// stable ratio, center placement (crop left and right)
			glTexCoord2f( (bg_img_w/2.0f - xw * zoom_factor / 2.0f) / bg_img_w, 0.0f ); glVertex2f(x,  y );
			glTexCoord2f( (bg_img_w/2.0f + xw * zoom_factor / 2.0f) / bg_img_w, 0.0f ); glVertex2f(xw, y );
			glTexCoord2f( (bg_img_w/2.0f + xw * zoom_factor / 2.0f) / bg_img_w, 1.0f ); glVertex2f(xw, yh);
			glTexCoord2f( (bg_img_w/2.0f - xw * zoom_factor / 2.0f) / bg_img_w, 1.0f ); glVertex2f(x,  yh);
		}
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

void SDL_opengl_draw::draw_particle(float_32 x, float_32 y, float radius , SDL_Color c)
{
	glColor4ub(c.r, c.g, c.b, c.a);

	GLUquadricObj* qobj = gluNewQuadric ();
	gluQuadricDrawStyle(qobj, GLU_FILL);

	glPushMatrix();
	glTranslatef ((float) world_to_screen_x(x), (float) world_to_screen_y(y), 0);
	gluDisk (qobj, 0, float_32(radius) * 0.0006f * get_view_zoom() + 2, 8 , 1);

	glPopMatrix ();
	gluDeleteQuadric (qobj);
}

void SDL_opengl_draw::draw_spring(float_32 ax, float_32 ay, float_32 bx, float_32 by, float line_width, SDL_Color c)
{
	glLineWidth (line_width);
	glColor4ub(c.r, c.g, c.b, c.a);
	glBegin (GL_LINES);
		glVertex2f ((float) this->world_to_screen_x(ax), (float) this->world_to_screen_y(ay));
		glVertex2f ((float) this->world_to_screen_x(bx), (float) this->world_to_screen_y(by));
	glEnd ();
}

void SDL_opengl_draw::draw_broken_spring(float_32 ax, float_32 ay, float_32 bx, float_32 by, float line_width, SDL_Color c)
{
#define PI 3.141592653589793f
	// draw a cirlce instead of two lines
	// to keep the API do some math here

	// get center
	Vector2d line_vec = Vector2d(ax, ay) - Vector2d(bx, by);
	Vector2d center = Vector2d(bx, by) + line_vec * 0.5f;

	float radius = 0.7f * line_vec.length();

	glColor4ub(c.r, c.g, c.b, c.a);
	glLineWidth (line_width);
	glBegin(GL_LINES);
	const int segments = 14;
	for(double i = 0; i < 2 * PI; i += PI / segments)
	{
		float world_x = (float) cos(i) * radius + center.x;
		float world_y = (float) sin(i) * radius + center.y;
 		glVertex2f((float) world_to_screen_x(world_x), (float) world_to_screen_y(world_y));
	}
	glEnd();
}

void SDL_opengl_draw::draw_ground(const float_32 ground[], int level_width, SDL_Color c)
{
	glColor4ub(c.r, c.g, c.b, c.a);
	glBegin( GL_QUAD_STRIP );
		glVertex2f(0, (float) Settings::singleton()->window_height);
		glVertex2f(0, (float) world_to_screen_y(0));

		glVertex2f((float) world_to_screen_x(-5.0f), (float) Settings::singleton()->window_height);
		glVertex2f((float) world_to_screen_x(-5.0f), (float) world_to_screen_y(0));

		int i, g = 0;
		for(i = 0; i < level_width; i+=5, ++g)
		{
			glVertex2f((float) world_to_screen_x(float(i)), (float) std::fmax(Settings::singleton()->window_height, world_to_screen_y(ground[g])));
			glVertex2f((float) world_to_screen_x(float(i)), (float) world_to_screen_y(ground[g]));
		}
		glVertex2f((float) world_to_screen_x(float(i)), (float) Settings::singleton()->window_height);
		glVertex2f((float) world_to_screen_x(float(i)), (float) world_to_screen_y(0));

		glVertex2f((float) Settings::singleton()->window_width, (float) Settings::singleton()->window_height);
		glVertex2f((float) Settings::singleton()->window_width, (float) world_to_screen_y(0));
	glEnd();

	// as GL_QUAD_STRIP only draw the inside of polygon
	// very sharp edges must be drawn with lines
	glLineWidth (0.5f);
	glBegin (GL_LINES);
		i = 0;
		g = 0;
		for(i = 0; i < level_width-5; i+=5, ++g)
		{
			glVertex2f((float) world_to_screen_x(float(i)), (float) world_to_screen_y(ground[g]));
			glVertex2f((float) world_to_screen_x(float(i+5)), (float) world_to_screen_y(ground[g+1]));
		}
	glEnd ();
}

void SDL_opengl_draw::draw_water(float_32 water_height, SDL_Color c)
{
	const float x = 0;
	const float y = (float) world_to_screen_y(water_height);
	const float xw = (float) Settings::singleton()->window_width;
	const float yh = (float) Settings::singleton()->window_height;
	glColor4ub(c.r, c.g, c.b, c.a);
	glBegin (GL_QUADS);
		glVertex2f (x, y);
		glVertex2f (xw, y);
		glVertex2f (xw, yh);
		glVertex2f (x, yh);
	glEnd ();
}

void SDL_opengl_draw::draw_ground_editbox(float_32 x, float width, float_32 ground_height, SDL_Color box_c, SDL_Color ground_c)
{
	float pos_x1 = float(world_to_screen_x(x));
	float pos_x2 = float(world_to_screen_x(x + width));
	float height_y = (float) Settings::singleton()->window_height;
	glColor4f (0.5f, 0.5f, 0.5f, 0.2f);
	glBegin (GL_QUADS);
		glVertex2f (pos_x1, 0);
		glVertex2f (pos_x2, 0);
		glVertex2f (pos_x2, height_y);
		glVertex2f (pos_x1, height_y);
	glEnd ();

	glColor4f (1.0f, 1.0f, 1.0f, 0.7f);
	glBegin (GL_QUADS);
		glVertex2f (pos_x1, float(world_to_screen_y(ground_height)));
		glVertex2f (pos_x2, float(world_to_screen_y(ground_height)));
		glVertex2f (pos_x2, height_y);
		glVertex2f (pos_x1, height_y);
	glEnd ();
}

void SDL_opengl_draw::draw_string(int x, int y, const char* str, SDL_Color c)
{
	glLineWidth (1.0f);

	glColor4f(float(c.r)/255.0f, float(c.g)/255.0f, float(c.b)/255.0f, 1.0);
	glPushMatrix();
	glTranslatef (float(x), float(y), 0);
	glScalef (.07f, -.07f, 0);


///////////////////////////////////////////////////////
//	SDL_Rect rect = { button->get_x_pos(), button->get_y_pos(), button->get_width(), button->get_height() };
//	if(button->get_opengl_texture() == 0 || button->get_need_rerender())
//	{
//		std::string font_filename(Settings::singleton()->full_path_to_font_file);
//		int font_size = Settings::singleton()->button_font_size;
//		tex_num = this->text_to_texture_bicolor(text, font_filename,
//								Settings::singleton()->sdlc_button_top,
//								Settings::singleton()->sdlc_button_bottom,
//								font_size, button->get_width(), button->get_height(),
//								result_image_width, result_image_height);
//		if(tex_num != 0)
//		{
//			button->set_opengl_texture(tex_num);
//			button->set_need_rerender(false);
//			button->set_image_width(result_image_width);
//			button->set_image_height(result_image_height);
//		}
//	}
//	else
//	{
//		tex_num = button->get_opengl_texture();
//		result_image_width = button->get_image_width();
//		result_image_height = button->get_image_height();
//		color = button->get_text_color();
//	}
//	bool stretch = true;
//	if(stretch)
//	{
//		x = float(rect.x) - float(result_image_width/2) + float(rect.w/2);
//		y = float(rect.y) - float(result_image_height/2) + float(rect.h/2);
//		xw = x + float(result_image_width);
//		yh = y + float(result_image_height);
//	}
//
//	glEnable( GL_BLEND );
//	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
//
//	// set texture color
//	glColor4ub(color.r, color.g, color.b, color.a);
//	glEnable(GL_TEXTURE_2D);
//	glBindTexture(GL_TEXTURE_2D, tex_num );
//	glBegin(GL_QUADS);
//		glTexCoord2f( 0.0f, 0.0f ); glVertex2f(x,  y );
//		glTexCoord2f( 1.0f, 0.0f ); glVertex2f(xw, y );
//		glTexCoord2f( 1.0f, 1.0f ); glVertex2f(xw, yh);
//		glTexCoord2f( 0.0f, 1.0f ); glVertex2f(x,  yh);
//	glEnd();
//	glDisable(GL_TEXTURE_2D);
///////////////////////////////////////////////////////



//	glRasterPos3f(x, y, 0);
	for(unsigned int i=0; i<strlen(str); ++i)
	{
//		glutStrokeCharacter  (GLUT_STROKE_MONO_ROMAN, str[i]);
	}
	glPopMatrix();
}

void SDL_opengl_draw::draw_grid(const SDL_Color c_most, const SDL_Color c_some)
{
	float x1, y1, x2, y2;
	const int GRID_SPACE = Settings::singleton()->grid_spacing;

	glColor4ub(c_most.r, c_most.g, c_most.b, c_most.a);
	glLineWidth (1.0f);
	glBegin(GL_LINES);
		// vertical lines
		int xstart = Math_utils::round_to(float_32(- get_view_x_offset()), GRID_SPACE);
		int xstop = int( screen_to_world_x(Settings::singleton()->window_width));
		for(int x = xstart; x <= xstop; x+= GRID_SPACE)
		{
			if(x % 80 == 0)
				glColor4ub(c_some.r, c_some.g, c_some.b, c_some.a);
			//if((x+2*80) % (8*80) == 0)   // draw blue lines with big distance
			//	glColor4ub(50, 250, 250, 255);

			x1 = (float) world_to_screen_x(float_32(x));
			y1 = 0;
			glVertex2f(x1, y1);
			x2 = (float) world_to_screen_x(float_32(x));
			y2 = (float) Settings::singleton()->window_height;
			glVertex2f(x2, y2);

			if(x % 80 == 0)
				glColor4ub(c_most.r, c_most.g, c_most.b, c_most.a);
		}

		// horizontal lines
		int ystart = Math_utils::round_to(float_32(get_view_y_offset()), GRID_SPACE);
		int ystop = int( screen_to_world_y(Settings::singleton()->window_height));
		for(int y = ystart; y >= ystop; y-= GRID_SPACE)
		{
			if(y % 80 == 0)
				glColor4ub(c_some.r, c_some.g, c_some.b, c_some.a);

			x1 = (float) 0;
			y1 = (float) world_to_screen_y(float_32(y));
			glVertex2f(x1, y1);
			x2 = (float) Settings::singleton()->window_width;
			y2 = (float) world_to_screen_y(float_32(y));
			glVertex2f(x2, y2);

			if(y % 80 == 0)
				glColor4ub(c_most.r, c_most.g, c_most.b, c_most.a);
		}
		//additional small lines between black ones
		glColor4ub(c_some.r, c_some.g, c_some.b, c_some.a);
		for(int x = xstart; x <= xstop; x+= GRID_SPACE)
		{
			for(int y = ystart; y >= ystop; y-= GRID_SPACE)
			{
				if((x + 40) % 80 == 0 && y % 80 == 0) {
					x1 = (float) world_to_screen_x(float_32(x));
					y1 = (float) world_to_screen_y(float_32(y-2));
					glVertex2f(x1, y1);
					x2 = (float) world_to_screen_x(float_32(x));
					y2 = (float) world_to_screen_y(float_32(y+2));
					glVertex2f(x2, y2);

					x1 = (float) world_to_screen_x(float_32(x+40-2));
					y1 = (float) world_to_screen_y(float_32(y-40));
					glVertex2f(x1, y1);
					x2 = (float) world_to_screen_x(float_32(x+40+2));
					y2 = (float) world_to_screen_y(float_32(y-40));
					glVertex2f(x2, y2);
				}
			}
		}
	glEnd();
}

void SDL_opengl_draw::draw_train(const float_32 *vx, const float_32 *vy, const int n, const SDL_Color tc)
{
	glColor4ub(tc.r, tc.g, tc.b, tc.a);
	glBegin( GL_POLYGON );
		for(int i=0; i < n; ++i)
			glVertex2f((float) world_to_screen_x(vx[i]), (float) world_to_screen_y(vy[i]));
	glEnd();
}

void SDL_opengl_draw::draw_button(GUI_Button* button)
{
	if( ! button->get_visible())
		return;

	SDL_Rect rect = { button->get_x_pos(), button->get_y_pos(), button->get_width(), button->get_height() };
	std::string text = button->get_text().c_str();

	float x = float( rect.x);
	float y = float( rect.y);
	// for pixel perfect drawing don't use get_y_pos + height (this would mess up rounding of RES_INDEPENDENT buttons)
	float xw = float( button->get_x_plus_width_pos());
	float yh = float( button->get_y_plus_height_pos());

	// if button has a background
	if(button->get_background())
	{
		// draw Box
		SDL_Color color;
		if(button->get_highlight())
			color = Settings::singleton()->sdlc_button_highlight;
		else
			color = Settings::singleton()->sdlc_button;

		this->draw_button_background(color, x, y, xw, yh);
	}

	// Draw Text
	if(button->get_text() == "")
		return;

	// is texture already rendered? has windowsize changed (and button is resolution independent)
	GLuint tex_num;
	int result_image_width, result_image_height;
	SDL_Color color;
	if(button->get_opengl_texture() == 0 || button->get_need_rerender())
	{
		std::string font_filename(Settings::singleton()->full_path_to_font_file);
		int font_size = Settings::singleton()->button_font_size + 140;
		tex_num = this->text_to_texture_bicolor(text, font_filename,
								Settings::singleton()->sdlc_button_top,
								Settings::singleton()->sdlc_button_bottom,
								font_size, button->get_width(), button->get_height(),
								result_image_width, result_image_height);
		if(tex_num != 0)
		{
			button->set_opengl_texture(tex_num);
			button->set_need_rerender(false);
			button->set_image_width(result_image_width);
			button->set_image_height(result_image_height);
			button->set_last_window_h(Settings::singleton()->window_height);
			button->set_last_window_w(Settings::singleton()->window_width);
		}
	}
	else
	{
		tex_num = button->get_opengl_texture();
		result_image_width = button->get_image_width();
		result_image_height = button->get_image_height();
		color = button->get_text_color();
	}
	bool stretch = true;
	if(stretch)
	{
		x = float(rect.x) - float(result_image_width/2) + float(rect.w/2);
		y = float(rect.y) - float(result_image_height/2) + float(rect.h/2);
		xw = x + float(result_image_width);
		yh = y + float(result_image_height);
	}

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	// set texture color
	glColor4ub(color.r, color.g, color.b, color.a);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, tex_num );
	glBegin(GL_QUADS);
		glTexCoord2f( 0.0f, 0.0f ); glVertex2f(x,  y );
		glTexCoord2f( 1.0f, 0.0f ); glVertex2f(xw, y );
		glTexCoord2f( 1.0f, 1.0f ); glVertex2f(xw, yh);
		glTexCoord2f( 0.0f, 1.0f ); glVertex2f(x,  yh);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

void SDL_opengl_draw::draw_button_background(SDL_Color c, float x, float y, float x2, float y2)
{
#define PI 3.141592653589793f
	float radius = float(Settings::singleton()->window_width) * 0.01f;
	// avoid draw error for too small boxes
	if(radius * 2.0f > fabs(x2 -x))
		radius = fabs(x2 -x)/2.0f;
	if(radius * 2.0f > fabs(y2 -y))
		radius = fabs(y2 -y)/2.0f;

	glColor4ub(c.r, c.g, c.b, c.a);
	glBegin(GL_POLYGON);
		// top-left corner
		this->draw_rounded_corner(x, y + radius, 3*PI/2, PI/2, radius);
		// top-right
		this->draw_rounded_corner(x2 - radius, y, 0.0, PI/2, radius);
		// bottom-right
		this->draw_rounded_corner(x2, y2 - radius, PI/2, PI/2, radius);
		// bottom-left
		this->draw_rounded_corner(x + radius, y2, PI, PI/2, radius);
	glEnd();
}

void SDL_opengl_draw::draw_rounded_corner(float x, float y, float sa, float arc, float r)
{
#define PI 3.141592653589793f
	const float rounding_pieces = 16.0f;

    // centre of the arc, for clockwise sense
    float cent_x = x + r * cos(sa + PI / 2);
    float cent_y = y + r * sin(sa + PI / 2);

    // build up piecemeal including end of the arc
    int n = int(ceil(rounding_pieces * arc / PI * 2));
    for (int i = 0; i <= n; i++) {
        double ang = sa + arc * (double)i  / (double)n;

        // compute the next point
        float next_x = cent_x + r * float( sin(ang));
        float next_y = cent_y - r * float( cos(ang));
        glVertex2f(next_x, next_y);
    }
}

GLuint SDL_opengl_draw::text_to_texture(const std::string& text, const std::string& font_filename, const SDL_Color c, const int font_size, int& w, int& h)
{
	GLuint return_tex = 0;
	w = 0;
	h = 0;
	TTF_Font *font = TTF_OpenFont(font_filename.c_str(), font_size);
	if( font == nullptr)
	{
		SDLERROR_PRINT("Couldn't open font file: %s\n", font_filename.c_str());
	}
	else
	{
		SDL_Surface* tmp_surface = TTF_RenderUTF8_Blended( font, text.c_str(), c);
		if(tmp_surface == nullptr)
		{
			ERROR_PRINT("Couldn't render to surface this text: '%s'\n", text.c_str());
		}
		else
		{
			// create new texture, with default filtering state (==mipmapping on)
			glGenTextures( 1, &return_tex );
			glBindTexture( GL_TEXTURE_2D, return_tex );

			// disable midmapping on the new texture
			glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
			glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, tmp_surface->w, tmp_surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, tmp_surface->pixels );

			w = tmp_surface->w;
			h = tmp_surface->h;
			SDL_FreeSurface(tmp_surface);
		}
		TTF_CloseFont(font);
	}
    return return_tex;
}

GLuint SDL_opengl_draw::text_to_texture_bicolor(const std::string& text, const std::string& font_filename,
			const SDL_Color c1, const SDL_Color c2, const int req_font_size,
			const int button_w, const int button_h, int& result_w, int& result_h)
{
	GLuint return_tex = 0;
	result_w = 0;
	result_h = 0;

	int font_size = SDL_utils::test_for_font_size(font_filename, text, button_w, button_h, req_font_size);
	if(font_size < 0)
	{
		ERROR_PRINT("Couldn't test font size for text: '%s'\n", text.c_str());
		return 0;
	}

	TTF_Font *font = TTF_OpenFont(font_filename.c_str(), font_size);
	if( font == nullptr)
	{
		SDLERROR_PRINT("Couldn't open font file: %s\n", font_filename.c_str());
	}
	else
	{
		SDL_Surface* tmp_surface = TTF_RenderUTF8_Blended( font, text.c_str(), c1);
		if(tmp_surface == nullptr)
		{
			ERROR_PRINT("Couldn't render to surface this text: '%s'\n", text.c_str());
		}
		else
		{
			SDL_Surface* tmp_surface2 = TTF_RenderUTF8_Blended( font, text.c_str(), c2);
			if(tmp_surface2 == nullptr)
			{
				ERROR_PRINT("Couldn't render to surface2 this text: '%s'\n", text.c_str());
			}
			else
			{
				SDL_utils::create_color_gradient(tmp_surface, tmp_surface2);

				// create new texture, with default filtering state (==mipmapping on)
				glGenTextures( 1, &return_tex );
				glBindTexture( GL_TEXTURE_2D, return_tex );

				// alpha
				GLenum textFormat;
				if(tmp_surface->format->Rmask == 0x000000ff)
					textFormat = GL_RGBA;
				else
					textFormat = GL_BGRA_EXT;

				// disable midmapping on the new texture
				glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
				glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

				// do not trust surface->w: use surface->pitch / surface->format->BytesPerPixel
				glTexImage2D( GL_TEXTURE_2D, 0, tmp_surface2->format->BytesPerPixel,
					tmp_surface2->pitch / tmp_surface2->format->BytesPerPixel, tmp_surface2->h,
					0, textFormat, GL_UNSIGNED_BYTE, tmp_surface2->pixels );

				result_w = tmp_surface2->w;
				result_h = tmp_surface2->h;
				SDL_FreeSurface(tmp_surface2);
			}
			SDL_FreeSurface(tmp_surface);
		}
		TTF_CloseFont(font);
	}
	return return_tex;
}

void SDL_opengl_draw::draw_rect(const float_32 x, const float_32 y, const float_32 width, const float_32 height, const SDL_Color c)
{
	const float rect_x = float( this->world_to_screen_x(x));
	const float rect_y = float( this->world_to_screen_y(y));
	const float rect_w = float(width * this->get_view_zoom());
	const float rect_h = float(height * this->get_view_zoom());
	glColor4ub(c.r, c.g, c.b, c.a);
	glBegin (GL_QUADS);
		glVertex2f (rect_x         ,  rect_y);
		glVertex2f (rect_x + rect_w,  rect_y);
		glVertex2f (rect_x + rect_w,  rect_y- rect_h);
		glVertex2f (rect_x         ,  rect_y- rect_h);
	glEnd ();
}

void SDL_opengl_draw::draw_pentagram(const float_32 x, const float_32 y, const float_32 width, const SDL_Color c)
{
#define PI 3.141592653589793f
	Vector2d center = Vector2d(x, y + width/2.0f);
	glColor4ub(c.r, c.g, c.b, c.a);
	glBegin(GL_POLYGON);
	const float size = (float) width/4.5f;
	const float length = size*2.0f;  // shrunken pentagon; regular with 2.618f

	const float radian = 2.0f*PI/5.0f;
	// don't start at 0 (rotation)
	for(double i = -0.75f; i < 4.25f; i += 1.0f )
	{
		// inner lines (pentagon)
		//glVertex2f(center.x + size*cos((i*radian)), center.y + size*sin(i*radian));
		//glVertex2f(center.x + size*cos((i+1)*radian),center.y + size*sin((i+1)*radian));

		glVertex2f(float(center.x + size*cos(i*radian)), float(center.y + size*sin(i*radian)));
		glVertex2f(float(center.x + length*cos(i*radian + PI/5)), float(center.y + length*sin(i*radian + PI/5)));

		glVertex2f(float(center.x + size*cos((i+1)*radian)), float(center.y + size*sin((i+1)*radian)));
		glVertex2f(float(center.x + length*cos(i*radian + PI/5)), float(center.y + length*sin(i*radian + PI/5)));
	}
	glEnd();
}
