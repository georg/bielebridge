//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <SDL2/SDL.h>
#include <vector>
#include <string>

// for file handling:
#include <stddef.h>
#include <cstdio>
#include <sys/types.h>
#include <dirent.h>

#include "gui_component.hpp"
#include "gui_menu_layouts.hpp"
#include "gui_button.hpp"
#include "gui_manager.hpp"
#include "benchmark.hpp"
#include "settings.hpp"
#include "error.hpp"
#include "level.hpp"
#include "sdl_draw.hpp"
#include "version.cmake.hpp"

GUI_Menu_Start::GUI_Menu_Start()
{
	name_button = new GUI_Button("bielebridge",200, 100, 600, 200, RES_INDEPENDENT);
	//name_button->set_background(false);

	start_button = new GUI_Button("Start",     300, 300, 400, 100, RES_INDEPENDENT);
	buttons.push_back(start_button);

	options_button = new GUI_Button("Options", 300, 400, 400, 100, RES_INDEPENDENT);
	buttons.push_back(options_button);

	about_button = new GUI_Button("About",     300, 500, 400, 100, RES_INDEPENDENT);
	buttons.push_back(about_button);

	benchmark_button = new GUI_Button("Benchmark", 300, 600, 400, 100, RES_INDEPENDENT);
	buttons.push_back(benchmark_button);

	benchmark_result_button = new GUI_Button("running... 5 sec", 700, 600, 250, 100, RES_INDEPENDENT);
	benchmark_result_button->set_visible(false);
	buttons.push_back(benchmark_result_button);
	benchmark_scheduled = false;
	benchmark_info_drawn = false;

	quit_button = new GUI_Button("Quit",       300, 700, 400, 100, RES_INDEPENDENT);
	buttons.push_back(quit_button);
}

GUI_Menu_Start::~GUI_Menu_Start()
{
	delete name_button;
	delete start_button;
	delete about_button;
	delete options_button;
	delete quit_button;
	delete benchmark_button;
	delete benchmark_result_button;
}

void GUI_Menu_Start::draw()
{
	event_window_resize();
	IDraw_Move_Zoom* sdl = Settings::singleton()->sdl_display;
	sdl->draw_background_image(Settings::singleton()->full_path_to_bgimage_file);

	// draw name
	sdl->draw_button(name_button);
	// draw all buttons
	for (Button_list::iterator b = buttons.begin (); b != buttons.end (); ++b)
		sdl->draw_button(*b);

	if(benchmark_scheduled)
	{
		if(benchmark_info_drawn)
		{
			// start benchmark (5 sec long)
			// below 2 sec CPU speed stepping affects result
			const int runtime_seconds = 5;
			Benchmark* bench = new Benchmark();
			unsigned long iter_per_sec = bench->run(runtime_seconds);
			fprintf(stderr, "interations per sec: %lu\n", iter_per_sec);

			std::string res_string = std::to_string(iter_per_sec) + " iter/s";
			benchmark_result_button->set_new_text(res_string);
			benchmark_result_button->set_visible(true);

			delete bench;

			// reset to start values
			benchmark_scheduled = false;
			benchmark_info_drawn = false;
		}
		else
		{
			benchmark_info_drawn = true;
		}
	}
}

void GUI_Menu_Start::event_mouse_click(GUI_Manager* guimanager, int button, int button_state, int x, int y)
{
	if(!button_state && button == 0)
	{
		GUI_Component* component = 0;
		for (Button_list::iterator i = buttons.begin (); i != buttons.end (); ++i)
		{
			if ((*i)->is_at (x, y)) component = *i;
		}

		if(component==start_button)
			guimanager->currentGUI = guimanager->GUIMENU_LEVELS;

		if(component==about_button)
			guimanager->currentGUI = guimanager->GUIMENU_ABOUT;

		if(component==options_button)
			guimanager->currentGUI = guimanager->GUIMENU_OPTIONS;

		if(component==quit_button){
			// quit the game (in game main loop)
			SDL_Event sdlevent;
    		sdlevent.type = SDL_QUIT;

    		SDL_PushEvent(&sdlevent);
		}
		if(component==benchmark_button){
			benchmark_scheduled = true;
			benchmark_result_button->set_new_text("running... 5 sec");
			benchmark_result_button->set_visible(true);
		}
		if(component==benchmark_result_button){
			benchmark_result_button->set_visible(false);
		}

		guimanager->event_resize();
	}
}

void GUI_Menu_Start::event_window_resize()
{
	name_button->on_window_resize();
	for (Button_list::iterator i = buttons.begin (); i != buttons.end (); ++i)
	{
		(*i)->on_window_resize();
	}
}


////////////////////////////////////////////////////////////////////////////////
// options menu

GUI_Menu_Options::GUI_Menu_Options()
{
	fullscreen_button = new GUI_Button("Toggle Fullscreen", 300, 30, 400, 100, RES_INDEPENDENT);
	buttons.push_back(fullscreen_button);
	if(Settings::singleton()->sdl_fullscreen)
		fullscreen_button->set_highlight(true);

	fps_button = new GUI_Button("FPS Target: 0", 350, 140, 300, 100, RES_INDEPENDENT);
	buttons.push_back(fps_button);
	// set current FPS Target
	std::string fps_string = "FPS Target: " + std::to_string(Settings::singleton()->fps_target);
	fps_button->set_new_text(fps_string);

	fps_minus_button = new GUI_Button("-", 300, 140, 50, 100, RES_INDEPENDENT);
	buttons.push_back(fps_minus_button);

	fps_plus_button = new GUI_Button("+", 650, 140, 50, 100, RES_INDEPENDENT);
	buttons.push_back(fps_plus_button);

	back_button = new GUI_Button("Save & Back", 400, 950, 200, 50, RES_INDEPENDENT);
	buttons.push_back(back_button);
}

GUI_Menu_Options::~GUI_Menu_Options()
{
	delete fullscreen_button;
	delete fps_button;
	delete fps_plus_button;
	delete fps_minus_button;
	delete back_button;
}

void GUI_Menu_Options::draw()
{
//	event_window_resize();
	IDraw_Move_Zoom* sdl = Settings::singleton()->sdl_display;
	sdl->draw_background_image(Settings::singleton()->full_path_to_bgimage_file);

	// draw all buttons
	for (Button_list::iterator b = buttons.begin (); b != buttons.end (); ++b)
		sdl->draw_button(*b);
}

void GUI_Menu_Options::event_mouse_click(GUI_Manager* guimanager, int button, int button_state, int x, int y)
{
	if(!button_state && button == 0)
	{
		GUI_Component* component = 0;
		for (Button_list::iterator i = buttons.begin (); i != buttons.end (); ++i)
		{
			if ((*i)->is_at (x, y)) component = *i;
		}

		if(component==back_button)
			guimanager->currentGUI = guimanager->GUIMENU_START;

		if(component==fps_minus_button && Settings::singleton()->fps_target > 2)
		{
			Settings::singleton()->fps_target--;
			std::string fps_string = "FPS Target: " + std::to_string(Settings::singleton()->fps_target);
			fps_button->set_new_text(fps_string);
		}
		if(component==fps_plus_button && Settings::singleton()->fps_target < 255)
		{
			Settings::singleton()->fps_target++;
			std::string fps_string = "FPS Target: " + std::to_string(Settings::singleton()->fps_target);
			fps_button->set_new_text(fps_string);
		}

		if(component==fullscreen_button)
		{
			if(! Settings::singleton()->sdl_fullscreen)
			{
				int retval = SDL_SetWindowFullscreen(Settings::singleton()->sdl_window, SDL_WINDOW_FULLSCREEN_DESKTOP);
				if(retval !=0)
				{
					SDLERROR_PRINT("%s\n", "Couldn't set fullscreen.");
					fullscreen_button->set_highlight(false);
				}
				else
				{
					Settings::singleton()->sdl_fullscreen = true;
					fullscreen_button->set_highlight(true);
				}
				guimanager->event_resize();
			}
			else
			{
				int retval = SDL_SetWindowFullscreen(Settings::singleton()->sdl_window, 0);
				if(retval !=0)
				{
					SDLERROR_PRINT("%s\n", "Couldn't leave fullscreen mode.");
					fullscreen_button->set_highlight(true);
				}
				else
				{
					Settings::singleton()->sdl_fullscreen = false;
					fullscreen_button->set_highlight(false);
				}
				guimanager->event_resize();
			}
		}
	}
}

void GUI_Menu_Options::event_window_resize()
{
	for (Button_list::iterator i = buttons.begin (); i != buttons.end (); ++i)
	{
		(*i)->on_window_resize();
	}
}


////////////////////////////////////////////////////////////////////////////////
// about menu

GUI_Menu_About::GUI_Menu_About()
{
	background_button = new GUI_Button(" ", 50, 30, 900, 900, RES_INDEPENDENT);
	buttons.push_back(background_button);

	text_button = new GUI_Button(PACKAGE_NAME " " PACKAGE_VERSION, 100, 30, 800, 100, RES_INDEPENDENT);
	buttons.push_back(text_button);
	text_button->set_background(false);

	text2Button = new GUI_Button("A Free Software Bridge Construction Game", 100, 130, 800, 100, RES_INDEPENDENT);
	buttons.push_back(text2Button);
	text2Button->set_background(false);

	text3Button = new GUI_Button("by", 100, 240, 800, 40, RES_INDEPENDENT);
	buttons.push_back(text3Button);
	text3Button->set_background(false);

	text4Button = new GUI_Button("Copyright (C) 2019 Georg Gottleuber", 100, 300, 800, 50, RES_INDEPENDENT);
	buttons.push_back(text4Button);
	text4Button->set_background(false);

	text5Button = new GUI_Button("using some simulation code from Construo", 100, 500, 800, 40, RES_INDEPENDENT);
	buttons.push_back(text5Button);
	text5Button->set_background(false);

	text6Button = new GUI_Button("Copyright (C) 2002 Ingo Ruhnke (author of Construo)", 100, 550, 800, 40, RES_INDEPENDENT);
	buttons.push_back(text6Button);
	text6Button->set_background(false);

	text61Button = new GUI_Button("https://construo.github.io/", 100, 600, 800, 40, RES_INDEPENDENT);
	buttons.push_back(text61Button);
	text61Button->set_background(false);

	text7Button = new GUI_Button("This game is Free Software (GPLv3)", 100, 800, 800, 50, RES_INDEPENDENT);
	buttons.push_back(text7Button);
	text7Button->set_background(false);

	back_button = new GUI_Button("Back", 400, 950, 200, 50, RES_INDEPENDENT);
	buttons.push_back(back_button);
}

GUI_Menu_About::~GUI_Menu_About()
{
	delete background_button;
	delete text_button;
	delete text2Button;
	delete text3Button;
	delete text4Button;
	delete text5Button;
	delete text6Button;
	delete text61Button;
	delete text7Button;
	delete back_button;
}

void GUI_Menu_About::draw()
{
//	event_window_resize();
	IDraw_Move_Zoom* sdl = Settings::singleton()->sdl_display;
	sdl->draw_background_image(Settings::singleton()->full_path_to_bgimage_file);

	// draw all buttons
	for (Button_list::iterator b = buttons.begin (); b != buttons.end (); ++b)
		sdl->draw_button(*b);
}

void GUI_Menu_About::event_mouse_click(GUI_Manager* guimanager, int button, int button_state, int x, int y)
{
	if(!button_state && button == 0)
	{
		if( back_button->is_at(x,y))
		{
			guimanager->currentGUI = guimanager->GUIMENU_START;
		}
	}
}

void GUI_Menu_About::event_window_resize()
{
	for (Button_list::iterator i = buttons.begin (); i != buttons.end (); ++i)
	{
		(*i)->on_window_resize();
	}
}

////////////////////////////////////////////////////////////////////////////////
// levels menu

GUI_Menu_Levels::GUI_Menu_Levels()
{
	int window_w = Settings::singleton()->window_width;
	int window_h = Settings::singleton()->window_height;

	predefined_levels_num = Settings::singleton()->predefined_levels_num;
	level_editor_threshold = Settings::singleton()->level_editor_threshold;

	// important: push default level buttons first in buttons vector
	// set first position below the "Back" Button
	lvl_button_cur_y_position = 150;
	int lower_coordinate_of_back_button = int(float(Settings::singleton()->window_height) *0.08 );
	if(lvl_button_cur_y_position < lower_coordinate_of_back_button )
		lvl_button_cur_y_position = lower_coordinate_of_back_button + 30;

	for(unsigned int i = 0; i < predefined_levels_num; ++i)
	{
		std::string name = "Level " + std::to_string(i+1);
		GUI_Button* tmp_button = new GUI_Button(name, lvl_button_x_pos, lvl_button_cur_y_position, lvl_button_width, lvl_button_height);
		buttons.push_back(tmp_button);
		lvl_button_cur_y_position += lvl_button_height + lvl_button_y_distance;
	}

	// add user levels
	DIR *dir;
	struct dirent *ent;
	lvl_button_cur_y_position = 150;
	std::string dir_name(Settings::singleton()->game_home_path + "/" + Settings::singleton()->dirname_levels);
	if ((dir = opendir(dir_name.c_str())) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir(dir)) != NULL)
		{
			const std::string filename = std::string(ent->d_name);
			const std::string f_ext = Settings::singleton()->fname_ext_user;
			if(filename.length() > f_ext.length() && filename.compare(filename.length() - f_ext.length(), f_ext.length(), f_ext) == 0)
			{
				GUI_Button* tmp_button = new GUI_Button(std::string(ent->d_name), lvl_button_x_pos_user, lvl_button_cur_y_position, lvl_button_width_user, lvl_button_height);
				buttons.push_back(tmp_button);
				lvl_button_cur_y_position += lvl_button_height + lvl_button_y_distance;
			}
		}
		closedir (dir);
	}
	else
	{
		/* could not open directory */
		ERROR_PRINT("could not open dir %s\n", dir_name.c_str());
		perror ("");
	}

	back_button = new GUI_Button("Back", 0, 0, 200, 80, RES_INDEPENDENT);
	buttons.push_back(back_button);

	new_level_button = new GUI_Button("Level Editor", 800, 0, 200, 80, RES_INDEPENDENT);
	new_level_button->set_visible(false);
	buttons.push_back(new_level_button);

	tooltip_text = new GUI_Button("Enter a name. Press \"Return\" to finish.", window_w/2 -500/2, window_h-25, 500, 25, CENTER_LOWER);
	tooltip_text->set_visible(false);
	tooltip_text->set_background(false);
	buttons.push_back(tooltip_text);

	new_lvl_name_text = new GUI_Button(" ", window_w/2 -200/2, window_h-50-15, 200, 25, CENTER_LOWER);
	new_lvl_name_text->set_visible(false);
	new_lvl_name_text->set_background(false);
	buttons.push_back(new_lvl_name_text);

	// setup level base path
	std::string game_home_path = Settings::singleton()->game_home_path;
	std::string dirname_levels = Settings::singleton()->dirname_levels;
	level_base_path = game_home_path +"/"+ dirname_levels +"/";
}

GUI_Menu_Levels::~GUI_Menu_Levels()
{
	for (size_t i = 0; i < buttons.size(); i++)
		delete buttons[i];
}

void GUI_Menu_Levels::draw()
{
//	event_window_resize();
	IDraw_Move_Zoom* sdl = Settings::singleton()->sdl_display;
	sdl->draw_background_image(Settings::singleton()->full_path_to_bgimage_file);

	unsigned int num_of_levels_passed =  Settings::singleton()->number_of_levels_passed();
#ifdef DEBUG_MENU
	num_of_levels_passed = predefined_levels_num;
#endif
#ifndef DEBUG_MENU
	if(!create_levels_unlocked && num_of_levels_passed > level_editor_threshold)
#endif
		create_levels_unlocked = true;
	if(create_levels_unlocked)
		new_level_button->set_visible(true);

	// draw all buttons
	for(size_t i = 0; i < buttons.size(); ++i)
	{
		// for predefined levels: show only levels that are passed + 3
		if(i < predefined_levels_num)
		{
			// FIXME move as much logic as possible out of draw() functions
			if(i < num_of_levels_passed + 3)
			{
				if(Settings::singleton()->is_level_passed((unsigned int)i+1))
				{
					buttons[i]->set_highlight(true);
					sdl->draw_button(buttons[i]);
					// draw stars
					float distance_to_stars = 21.0f;
					for(size_t s = 1; s <= Settings::singleton()->get_level_stars((unsigned int)i+1); ++s)
					{
						sdl->draw_pentagram(float(buttons[i]->get_x_pos()) + float(buttons[i]->get_width()) + distance_to_stars*float(s),
							float(buttons[i]->get_y_pos()),
							float(buttons[i]->get_height()),
							Settings::singleton()->sdlc_yellow);
					}
				}
				else
				{
					sdl->draw_button(buttons[i]);
				}
			}
		}
		else
		{
			sdl->draw_button(buttons[i]);
		}
	}
}

void GUI_Menu_Levels::event_mouse_click(GUI_Manager* guimanager, int button, int button_state, int x, int y)
{
	unsigned int num_of_levels_passed =  Settings::singleton()->number_of_levels_passed();
#ifdef DEBUG_MENU
	// set to max to view all levels for debug
	num_of_levels_passed = predefined_levels_num;
#endif

	if(!create_levels_unlocked && num_of_levels_passed > level_editor_threshold)
		create_levels_unlocked = true;

	if(!button_state && button == 0)
	{
		GUI_Component* component = nullptr;
		unsigned int counter = 0;
		unsigned int level_number = 0xdeadbeef; // init with never used val; dec: 3735928559
		for (Button_list::iterator i = buttons.begin (); i != buttons.end (); ++i)
		{
			if((*i)->is_at(x, y)){
				component = *i;
				level_number = counter;
			}
			counter++;
		}

		// as level buttons are first in Button_list:
		// test if button clicked is a predefined level button
		if(level_number < predefined_levels_num)
		{
			// test if level is already visible
			if(level_number < num_of_levels_passed + 3 )
			{
				std::string filename_ext = Settings::singleton()->fname_ext_level;

				char tmp_filename[10];
				snprintf(tmp_filename, 10, "lvl%03d", level_number + 1);
				std::string full_path = level_base_path + tmp_filename + filename_ext;
				guimanager->createGUI_Editor(level_number +1, component->get_text(), full_path); // level 1 is at buttons[0];
				guimanager->currentGUI = guimanager->GUIEDITOR;
			}
		}
		else // maybe other button
		{
			if(component==back_button)
			{
					guimanager->currentGUI = guimanager->GUIMENU_START;
			}
			else
			{
				if(component==new_level_button)
				{
					if(create_levels_unlocked)
					{
						tooltip_text->set_visible(true);
						// clear name string
						new_lvl_name.clear();
						new_lvl_name_text->set_new_text(new_lvl_name);

						new_lvl_name_text->set_visible(true);
						// choose a name; end with enter
					}
				}
				else
				{
					// make sure a button was hit
					if(component!=nullptr && create_levels_unlocked)
					{
						// button clicked was a user level
						std::string full_path = level_base_path + component->get_text();
						guimanager->createGUI_Editor(0, component->get_text(), full_path); // id = 0 means user level
						guimanager->currentGUI = guimanager->GUIEDITOR;
					}
				}
			}
		}

		// maybe window was resized; update gui object positions
		guimanager->event_resize();
	}
}

void GUI_Menu_Levels::event_keyboard(const char* utf8_key)
{
	const size_t max_name_len = 20;
	std::string utf8_string = utf8_key;

	// for simplicity: only allow alnum and "_-.+"
	if(utf8_string.size() == 1 &&
			new_lvl_name.size() < max_name_len &&
			utf8_string.find_first_not_of("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.+") == std::string::npos)
	{
		new_lvl_name.append(utf8_key);
	}
	if(utf8_string.compare("Space") == 0 && new_lvl_name.size() < max_name_len)
		new_lvl_name.append("_");
	if(utf8_string.compare("Backspace") == 0 && new_lvl_name.size() > 0)
	{
		// this will only work with one Byte UTF-8 (ASCII)
		new_lvl_name.pop_back();
	}
	if(utf8_string.compare("Return") == 0){
		tooltip_text->set_visible(false);
		new_lvl_name_text->set_visible(false);
		// create a new default level...
		Level* new_level = new Level();
		// set filename
		std::string filename_ext = Settings::singleton()->fname_ext_user;
		std::string full_path = level_base_path + new_lvl_name + filename_ext;
		new_level->set_full_path(full_path);

		new_level->save_to_file();
		delete new_level;
		// add level button to menu
		add_level_button(new_lvl_name + Settings::singleton()->fname_ext_user);
	}
	new_lvl_name_text->set_new_text(new_lvl_name);
}

void GUI_Menu_Levels::add_level_button(const std::string& lvl_name)
{
	// test if a level with this name already exists (and was overwritten)
	bool level_exists = false;
	for (size_t i = 0; i < buttons.size(); i++)
	{
		if(lvl_name.compare(buttons[i]->get_text()) == 0)
			level_exists = true;
	}
	// add new level name
	if(!level_exists)
	{
		GUI_Button* tmp_button = new GUI_Button(lvl_name, lvl_button_x_pos_user, lvl_button_cur_y_position, lvl_button_width_user, lvl_button_height);
		buttons.push_back(tmp_button);
		lvl_button_cur_y_position += lvl_button_height + lvl_button_y_distance;
	}
}

void GUI_Menu_Levels::event_window_resize()
{
	for (Button_list::iterator i = buttons.begin (); i != buttons.end (); ++i)
	{
		(*i)->on_window_resize();
	}
}
