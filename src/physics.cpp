//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <algorithm>
#include <cmath>
#include "settings.hpp"
#include "physics.hpp"
#include "level.hpp"
#include "particle.hpp"
#include "spring.hpp"
#include "error.hpp"
#include "gui_editorsimulator.hpp"
#include "gui_simulator.hpp"
#include "math_utils.hpp"
#include "train.hpp"

Physics::Physics(Level* level)
	:level(level)
{
	const float_32 xpos = -60.0f;
	const float_32 ypos = 0;
	const int num_loc = 1;
	const float_32 loc_weight = 5.0f;
	const int num_car = 4;
	const float_32 car_weight = 3.0f;

	train = new Train(xpos, ypos, num_loc, loc_weight, num_car, car_weight, level);

#ifdef DEBUG
	collision_points_num = 0;
	collision_points = new Vector2d[100];
	for( int i = 0 ; i < 20  ; ++i )
	{
		debug_fastcol_ret[i] = 0;
	}
#endif
}

Physics::~Physics(){
}

void Physics::add_debug_body(float_32 x, float_32 y){
#ifdef DEBUG_SIMULATOR
	train->add_debug_car(x, y, level);
#endif
}

// return true if train is right outside of level
bool Physics::check_level_passed()
{
	if(!train) return false;

	bool passed = true;
	// first test all carriages
	for(int i = 0; i < train->num_car && passed; ++i)
	{
		// all points
		for(int p = 0; p < train->carriages[i]->points_num; ++p)
		{
			if(train->carriages[i]->points[p]->pos.x < float_32(level->width))
				passed = false;
		}
	}

	// test all locomotives
	for(int i = 0; i < train->num_loc && passed; ++i)
	{
		for(int p = 0; p < train->locomotives[i]->points_num; ++p)
		{
			if(train->locomotives[i]->points[p]->pos.x < float_32(level->width))
				passed = false;
		}
	}

	return passed;
}

unsigned int Physics::update(std::vector<Spring*>& broken_springs){
	int return_val = 0;
	// use water friction factor 0.1 - 0.9
	// 0.3 -> like water
	// 0.8 -> like honey
	const float_32 water_friction = 0.3f;

	broken_springs.clear();
	iteration_count++;

	// for very big bridges the simulation start causes more stress (by sudden gravity impact oscillations)
	// than train passing. To avoid this start smoth with additiononal damping (like insinde water)
	const float damping_factor = 2.0f;
	if(iteration_count < 5000 && iteration_count % 4 == 0)
		for (size_t i = 0; i < level->sim_particles.size(); ++i)
			level->sim_particles[i].velocity -= level->sim_particles[i].velocity * level->sim_particles[i].inv_mass * TIME_STEP * water_friction * damping_factor;

	// check if level passed
	if((!level->level_passed) && this->check_level_passed()){
		level->level_passed = true; // this will show text in gui_simulator
		Settings::singleton()->set_level_passed(level->get_id()); // this will unlock further levels and will be written to config
		// calculate stars
		unsigned int percent_budget_left = (level->budget_left * 100) / level->budget ;
		unsigned int maximum_possible_stars = 3;
		unsigned int stars = std::min(percent_budget_left / 16, maximum_possible_stars);
		Settings::singleton()->set_level_stars(level->get_id(), stars);
		if(! Settings::singleton()->is_level_passed(level->get_id())){
			ERROR_PRINT("level (id %d) passed, but couldn't store it.\n", level->get_id());
		}
	}

	// add loc engine force
	if(train)
		this->train_add_forward_force();

	// doing all particle stuff
	for(Particle_iters i = level->sim_particles.begin(); i != level->sim_particles.end(); ++i)
	{
#ifdef DEBUG_NAN
		if( std::isinf(i->pos.x) ||  std::isnan(i->pos.x) ||
				std::isinf(i->pos.y) || std::isnan(i->pos.y) ){
			ERROR_PRINT("ERORR: particle %d has bad floating point numbers (x: %f, y: %f)\n",
							i->id, i->pos.x, i->pos.y);
		}
#endif

		// update all particle positions and velocities
		//i->update(TIME_STEP, level->water_height);
		//=============================================================================================//
		if ( ! i->fixed)
		{
			// add gravity to all particles
			i->add_force(gravity_vec * i->mass);


			// update velocity
			i->velocity += i->totale_force * TIME_STEP * i->inv_mass;

			// friction inside water ...
			if(i->pos.y < level->water_height) {
				i->velocity -= i->velocity * i->inv_mass * TIME_STEP * water_friction;
			}

			// (air) friction (to avoid endless sliding / motion and super fast things)
			const float_32 velocity_norm_squared = i->velocity.x * i->velocity.x + i->velocity.y * i->velocity.y;
			float_32 friction_factor = 1.0f - (velocity_norm_squared - 9000.0f) / 32768.0f; //(mul is not faster here!)
			// limits
			// max 0.99999995f (bigger would be coded as 1.0f)
			if(friction_factor > 0.99999f) friction_factor = 0.99999f;
			if(friction_factor < 0.01f) friction_factor = 0.01f;

			i->velocity *= friction_factor;

			i->pos += i->velocity * TIME_STEP;
			i->totale_force = {0,0}; //clear_force ();

#ifdef DEBUG
			// check for super fast things
			const float_32 max_velocity_squared = 200.0f * 200.0f;
			if (velocity_norm_squared > max_velocity_squared)
			{
				if(i->used_in_train)
					INFO_PRINT("physics: super fast things happen: train particle velocity reached %d\n", int(i->velocity.length()));
				else
					INFO_PRINT("physics: super fast things happen: particle velocity reached %d\n", int(i->velocity.length()));
			}
#endif
		}
		//=============================================================================================//

		// particles vs ground (also train particles)
		this->bounce_particle(*i);
	}


	// update all spring forces
//	for(size_t i = 0; i < level->sim_springs.size(); ++i)
//	{
//	}

	// collision handling

	// particles vs ground (also train particles)
//	for(size_t i = 0; i < level->sim_particles.size(); ++i)
//	{
//	}
	// train vs ground and deck
	this->collide_ground_points_inside_train(); // great new stuff

	this->collide_deck_with_train(); // great new stuff

	// update all positions and velocities
//	for(size_t i = 0; i < level->sim_particles.size(); ++i)
//	{
//	}


	for(Spring_iters s = level->sim_springs.begin(); s != level->sim_springs.end(); ++s)
	{
		// update all spring forces
		// s->update();

		/** calculates forces to particles of this spring or
		 * destroies spring if forces are to high
		 *
		 * Note: This code path is responsible for about 50% of physics runtime!
		 */
		//=============================================================================================//
		// if not destroyed
		if(s->status == Spring::NORMAL)
		{
			Particle* first = s->particles.first;
			Particle* second = s->particles.second;
			// do not init or use objects in this critical path
			//Vector2d spring_vec = first.pos - second.pos;
			const float_32 vec_x = first->pos.x - second->pos.x;
			const float_32 vec_y = first->pos.y - second->pos.y;

			// Calculate the stretchness of the spring, 0.0 if unstretch, else <> 0
			//const float_32 cur_length = spring_vec.length();
			const float_32 cur_length =  std::sqrt(vec_x*vec_x + vec_y*vec_y);
			float_32 stretch = cur_length - s->length;
			if (fabs(stretch * s->inv_length) > s->max_stretch)
			{
				// If the spring is streched above limits, let it get destroyed
				s->status = Spring::DESTROYED;
				if(stretch > 0 )
					s->max_color = 1.0f;
				else
					s->max_color = -1.0f;
			}
			else
			{
				// divided by length, this gives long springs equal strength as short ones
				stretch *= s->stiffness * s->inv_length;

				// const float_32 dterm = (spring_vec.dot(particles.first->velocity - particles.second->velocity) * damping)/spring_vec.length();
				// spring_vec.normalize();
				// const Vector2d force = spring_vec * (stretch + dterm);
				// first.totale_force += -force;
				// second.totale_force += force;

				// optimized form
				const float_32 dterm = ((( vec_x * (first->velocity.x - second->velocity.x) ) + (vec_y * (first->velocity.y - second->velocity.y))) * s->damping)/cur_length;

				const float_32 tmpfloat = (stretch + dterm) / cur_length;

				first->totale_force.x -= vec_x * tmpfloat;
				first->totale_force.y -= vec_y * tmpfloat;
				second->totale_force.x += vec_x * tmpfloat;
				second->totale_force.y += vec_y * tmpfloat;
			}

			// calcualte color
			// ratio                                       //  * 1.1f stretches contrast
			s->cur_color = cur_length * s->inv_length - 1.0f;// / max_stretch) * 1.1f;

			// update max_color
			if(fabs(s->cur_color) > fabs(s->max_color))
				s->max_color = s->cur_color;
		}
		//=============================================================================================//

		// report (copy to broken_springs) any springs that are marked as destroyed
		if (s->status == Spring::DESTROYED)
		{
			if(s->type == Spring::TRAIN || s->type == Spring::COUPLING)
			{
				ERROR_PRINT("Train crashed: [%d, %d] broke at iteration %lu\n",
							s->particles.first->id, s->particles.second->id, iteration_count);
#ifdef DEBUG_SIMULATOR
				return_val |= BROKEN_TRAIN_SPRING;
				broken_springs.push_back(&(*s));
#else
//				s->recalc_length();
#endif
			}
			else
			{
				return_val |= BROKEN_BRIDGE_SPRING;
				broken_springs.push_back(&(*s));
			}
			// set status to "destroyed and reported"
			s->status = Spring::DESTROYED_AND_REPORTED;
		}
	} // end of spring update loop
	// if one coupling is broken, break both
	// if a train spring is broken delete loc/car
	train->check_train_and_couplings();

	if((iteration_count % 1000 == 0) && Settings::singleton()->verbose_cli_output)
			INFO_PRINT("current simulation at iteration %lu\n", iteration_count);

	return return_val;
}

// get nearest ground line segment (not fast and simple: ground_left.x <= point.x <= ground_right.x)
// with ground peaks this makes bouncing a lot more stable
void Physics::get_nearest_ground_line_segment(const Vector2d point, Vector2d& ground_left, Vector2d& ground_right)
{
#ifdef DEBUG
	// sanity check
	if(point.x < 0 || point.x > level->width)
		ERROR_PRINT("input point out of level: p.x = %f\n", point.x);
#endif
	const int index_left = int( point.x / 5);
	const int index_right = int( point.x / 5)+1;

	const Vector2d left  = Vector2d( float_32( index_left * 5 ), level->ground[index_left]);
	const Vector2d right = Vector2d( float_32( index_right * 5), level->ground[index_right]);
	float_32 distance_square = Math_utils::get_distance_square_point_line_segment(point, left, right);

	// set return points to default value
	ground_left.x = float_32( index_left * 5 );
	ground_left.y = level->ground[index_left];

	ground_right.x = float_32(index_right * 5);
	ground_right.y = level->ground[index_right];

	// test one segment further to the left
	if(index_left-1 >= 0){
		Vector2d ground_far_left = Vector2d( float_32( (index_left - 1)*5 ), level->ground[index_left-1]);
		float_32 distance_square_left = Math_utils::get_distance_square_point_line_segment(point, ground_far_left, left);
		// if distance_square is smaller
		if( distance_square_left < distance_square ) {
			// set return values for nearer ground segment
			ground_right = ground_left;
			ground_left = ground_far_left;
			distance_square = distance_square_left;
		}
	}

	// test one segment further to the right
	if(index_right < level->ground_len -1){
		Vector2d ground_far_right = Vector2d( float_32( (index_right+1)*5 ), level->ground[index_right+1]);
		float_32 distance_square_right = Math_utils::get_distance_square_point_line_segment(point, right, ground_far_right);
		if( distance_square_right < distance_square ) {
			// set return values for nearer ground segment
			ground_left = ground_right;
			ground_right = ground_far_right;
		}
	}
}

// check if spring / train particle is inside ground
void Physics::bounce_particle(Particle& p)
{
	// TODO (this is slow! otimize it)
	if(p.used_in_train)
	{
		// 1)check for rare double crossing condition
		//   (train and ground are crossing but no point is inside eatch other;
		//   this can happen with concave polygons as ground is)
		Spring* s1 = nullptr;
		Spring* s2 = nullptr;
		train->get_springs_to_p(&p, s1, s2);
		// both polygon lines have double crossings with ground and ground point is not inside polygon
		Vector2d ground_before, ground_center, ground_after;
		if(level->s_double_cross_with_same_ground(s1, s2, ground_before, ground_center, ground_after))
		{
			// force point to polygon center
			// Vector2d train_points[10];
			// int train_points_num;
			// train->get_train_points_by_p(p,train_points, train_points_num);
			// Vector2d center = Math_utils::center_of_polygon(train_points, train_points_num);
			// Vector2d force = center - p->pos;
			Vector2d force;
			if(p.pos.x < ground_center.x)
				force = - Math_utils::get_normal(ground_after - ground_center);
			else
				force = - Math_utils::get_normal(ground_center - ground_before);

			force.normalize();
			INFO_PRINT("double cross with same ground: force [%f, %f]\n", force.x, force.y);
			p.add_force(force * 50.0f);
		}
	}

	// if no collision happend: return
	if(! level->p_inside_ground(p.pos))
		return;

	bool inside_level = false;
	if(p.pos.x > 0 && p.pos.x < float_32(level->width))
	{
#ifdef DEBUG
		const int index_left = int( p.pos.x / 5);
		const int index_right = int( p.pos.x / 5)+1;
		if(index_left > level->ground_len || index_right > level->ground_len || index_left < 0 || index_right < 0)
			ERROR_PRINT("ERROR: index out of bound[0-%d]: il=%d ir=%d p->pos.x=%f\n", level->ground_len, index_left, index_right, p.pos.x);
#endif
		inside_level = true;
	}

	// particle is inside the ground
	Vector2d ground_left, ground_right, collision_normal;

	if(inside_level)
	{
		// in rare cases (sharp ground peak) the particle must collide with neighbour ground segment
		this->get_nearest_ground_line_segment(p.pos, ground_left, ground_right);
		collision_normal = Math_utils::get_normal(ground_right - ground_left);
		collision_normal.normalize(); // get vector with length == 1
	}
	else
	{
		collision_normal = {0, -1.0};
	}

	float_32 vel_along_normal = p.velocity.dot(collision_normal);

	// if particle is already moving outside (vel_along_normal < 0) or too slowly moving outside
	//   with (vel_along_normal < 0.0f) bridge particles become very bouncy
	if(vel_along_normal < -1.0f)
		return;

	// penetration depth (measured to the ground line above;
	//   this is not correct if ground point with sharp edge is near)
	float_32 pen_depth;
	if(inside_level)
		pen_depth = Math_utils::get_distance_point_line_segment(p.pos, ground_right, ground_left);
	else
		pen_depth = std::fabs(p.pos.y);

	const float_32 hardness = 550.0f; // makes train softly moving along ground

	// float_32 impulse_scalar = 2.0f * particle_collision_damping  / (1 / p->mass); // * vel_along_normal
	// p->velocity -= collision_normal * impulse_scalar; // more realistic but not smooth and soft

	// check for ground in train condition
	if(p.used_in_train)
	{
		Vector2d train_points[Locomotive::points_num];
		int train_points_num;
		train->get_train_points_by_p(&p,train_points, train_points_num);
		Vector2d center = Math_utils::center_of_polygon(train_points, train_points_num);

		// if force goes away from center of polygon something is wrong...
		if(Vector2d::distance_square(center - collision_normal, p.pos) < Vector2d::distance_square(center, p.pos))
		{
			// invert force
			pen_depth = -1.0f;
#ifdef DEBUG_SIMULATOR
			if( (-collision_normal * hardness * pen_depth).length_square() > 3000.0f*3000.0f)
				ERROR_PRINT("big inv force: %f\n", (-collision_normal * hardness * pen_depth).length());
			else
				ERROR_PRINT("inv force: %f\n", (-collision_normal * hardness * pen_depth).length());
#endif
		}
	}

	p.add_force(-collision_normal * hardness * pen_depth) ;
#ifdef DEBUG_SIMULATOR
	if(hardness * pen_depth > 2000.0f)
	{
		ERROR_PRINT("something is wrong: added force %f; pen_depth: %f; particle id: %d\n", hardness * pen_depth, pen_depth, p.id);
	}
#endif
}

void Physics::collide_ground_points_inside_train()
{
	for(int i=0; i < train->num_loc; ++i)
		collide_ground_with_polygon(train->locomotives[i]->points, 5, train->locomotives[i]->springs);

	for(int i=0; i< train->num_car; ++i)
		collide_ground_with_polygon(train->carriages[i]->points, 4, train->carriages[i]->springs);
}

void Physics::collide_deck_with_train()
{
	for(int i=0; i < train->num_loc; ++i)
		aabb_col_deck_with_polygon(train->locomotives[i]->points, 5, train->locomotives[i]->springs);

	for(int i=0; i< train->num_car; ++i)
		aabb_col_deck_with_polygon(train->carriages[i]->points, 4, train->carriages[i]->springs);
}

// get collisions of ground vertexes / lines inside or crossing a train polygon
void Physics::collide_ground_with_polygon(Particle** train_points, int train_points_num, Spring** train_springs)
{
	// using AABB (axis aligned bounding box) around locomotive / carriage
	//   without max_y as ground can only be below polygon (not completley above)
	float_32 aabb_min_x = train_points[0]->pos.x;
	float_32 aabb_max_x = train_points[0]->pos.x;
	float_32 aabb_min_y = train_points[0]->pos.y;

	for(int i=1 ;i<train_points_num ; ++i)
	{
		if( train_points[i]->pos.x < aabb_min_x) aabb_min_x = train_points[i]->pos.x;
		if( train_points[i]->pos.x > aabb_max_x) aabb_max_x = train_points[i]->pos.x;
		if( train_points[i]->pos.y < aabb_min_y) aabb_min_y = train_points[i]->pos.y;
	}

	// get one ground index more to left and right
	const int min_ground_index = int(floor( aabb_min_x / 5.0f ));
	const int max_ground_index = int( ceil( aabb_max_x / 5.0f ));

	// copy all train particle positions
	Vector2d* train_points_tmp = new Vector2d[train_points_num];
	for( int i=0 ; i<train_points_num ; ++i )
		train_points_tmp[i] = train_points[i]->pos;

	// position approach
	// check if ground point is inside train polygon
	for(int i = std::max(min_ground_index,0); i < std::min(max_ground_index, level->ground_len); ++i)
	{
		Vector2d ground_point = Vector2d(float_32(i*5), level->get_ground_height_at(float_32(i)*5.0f));
		// is ground point in train polygon?
		if(Math_utils::point_in_polygon(ground_point, train_points_tmp, train_points_num) )
		{
			// ground point is in train polygon
			float_32 pen_depth_sq;
			int index_of_nearest_segment;
			// if single ground peak is penetrating use always line with both collision points of peak (not nearest)
			int train_line_index = level->get_double_cross_train_line(i, train_points_tmp, train_points_num);
			if(train_line_index != -1)
			{
				// no ground double cross on a train line segment
				pen_depth_sq = Math_utils::get_distance_square_point_line_segment(ground_point, train_points_tmp[train_line_index], train_points_tmp[(train_line_index+1)%train_points_num]);
				index_of_nearest_segment = train_line_index;
#ifdef DEBUG_SIMULATOR
				// for the debug // FIXME delete this code block FIXME
				if(pen_depth_sq > 20.0f)
				{
					// some thing is wrong find better matchin train lines...
					ERROR_PRINT("something is very wrong: pen_depth: %f; index_of: %d\n", pen_depth_sq, index_of_nearest_segment);
					float_32 D_pen_depth_sq = Math_utils::get_distance_square_point_line_segment(ground_point, train_points_tmp[train_points_num-1] , train_points_tmp[0]);
					index_of_nearest_segment = train_points_num -1;
					ERROR_PRINT("index(dist_sq): %d(%f)\n", train_points_num-1,  D_pen_depth_sq);
					for (int i = 1; i < train_points_num; i++)
					{
						float_32 dist_sq = Math_utils::get_distance_square_point_line_segment(ground_point, train_points_tmp[i-1], train_points_tmp[i]);
						ERROR_PRINT("index(dist_sq): %d(%f)\n", i-1,  dist_sq);
					}
				}
#endif
			}
			else
			{
				// this is the usual case:
				// get nearest line and square pentration depth (as force will be depend on this)
				pen_depth_sq = Math_utils::get_distance_square_point_line_segment(ground_point, train_points_tmp[train_points_num-1] , train_points_tmp[0]);
				index_of_nearest_segment = train_points_num -1;
				for (int i = 1; i < train_points_num; i++)
				{
					float_32 dist_sq = Math_utils::get_distance_square_point_line_segment(ground_point, train_points_tmp[i-1], train_points_tmp[i]);
					if( dist_sq < pen_depth_sq)
					{
						pen_depth_sq = dist_sq;
						index_of_nearest_segment = i-1;
					}
				}
			}
			const float_32 sloppy_pen_depth_sq = 1.73f; //1.7320508010f;
			if(train_line_index != -1 || pen_depth_sq > sloppy_pen_depth_sq)
			{
				if(train_line_index == -1)
					pen_depth_sq -= sloppy_pen_depth_sq;
				// sending impulse in relation to the distance to particles (of loc / car)
				Particle* p1 = train_springs[index_of_nearest_segment]->particles.first;
				Particle* p2 = train_springs[index_of_nearest_segment]->particles.second;

				float_32 dist_sq_to_p1 = Vector2d::distance_square(ground_point, p1->pos);
				float_32 dist_sq_to_p2 = Vector2d::distance_square(ground_point, p2->pos);

				float_32 len_train_spring = Vector2d::distance_square(p1->pos, p2->pos);

				// factor have to be exchanged (long distance -> small effect)
				float_32 factor_p1 = dist_sq_to_p2 / len_train_spring; // 0.0 - 1.0
				float_32 factor_p2 = dist_sq_to_p1 / len_train_spring; // 1.0 - 0.0

				Vector2d collision_normal =  Math_utils::get_normal(train_points_tmp[index_of_nearest_segment] - train_points_tmp[(index_of_nearest_segment+1) % train_points_num]);
				collision_normal.normalize(); // get vector with length == 1
#ifdef DEBUG_SIMULATOR
				if( (collision_normal * pen_depth_sq * factor_p1).length() > 3.0f ||  (collision_normal * pen_depth_sq * factor_p2).length() >3.0f)
				{
					INFO_PRINT("position correction: len: %f %f;\npen_sq: %f f1: %f f2:%f train_line_index: %d\n",
							(collision_normal * pen_depth_sq * factor_p1).length(),
							(collision_normal * pen_depth_sq * factor_p2).length(),
							pen_depth_sq, factor_p1, factor_p2, train_line_index);
				}
#endif
				p1->pos -= collision_normal * pen_depth_sq * factor_p1;
				p2->pos -= collision_normal * pen_depth_sq * factor_p2;

				//nparticle->pos -= collision_normal * pen_depth_sq ;
				//nparticle->add_force(-collision_normal * pen_depth_sq * 100.0f);
			}
		}
	}
	delete[] train_points_tmp;
}

// call per polygon (train locomotive / carriage)
void Physics::aabb_col_deck_with_polygon(Particle** train_points, uint_fast8_t train_points_num, Spring** train_springs){
	// fast preselection using AABB (axis aligned bounding box) around locomotive / carriage
	float_32 aabb_min_x = train_points[0]->pos.x;
	float_32 aabb_max_x = train_points[0]->pos.x;
	float_32 aabb_min_y = train_points[0]->pos.y;
	float_32 aabb_max_y = train_points[0]->pos.y;

	for(int i=1 ;i<train_points_num ; ++i) {
		if( train_points[i]->pos.x < aabb_min_x) aabb_min_x = train_points[i]->pos.x;
		if( train_points[i]->pos.x > aabb_max_x) aabb_max_x = train_points[i]->pos.x;
		if( train_points[i]->pos.y < aabb_min_y) aabb_min_y = train_points[i]->pos.y;
		if( train_points[i]->pos.y > aabb_max_y) aabb_max_y = train_points[i]->pos.y;
	}

	Vector2d p1, p2;
	Spring* col_springs[MAX_COL_SPRINGS]; // maximal collisions of deck springs with train
	unsigned char col_springs_num = 0;
	// iterate over all deck springs (indices are saved in vector sim_deck_springs_i)
	for (size_t i = 0; i < level->sim_deck_springs_i.size(); ++i)
	{
		Spring& s = level->sim_springs[level->sim_deck_springs_i[i]];
		// only if not destroyed
		if(s.status == Spring::NORMAL)
		{
			p1 = (s).particles.first->pos;
			p2 = (s).particles.second->pos;

			// both points of spring outside of the same side of bounding box?
			bool outside =  (p1.x > aabb_max_x && p2.x > aabb_max_x) || (p1.x < aabb_min_x && p2.x < aabb_min_x) ||
							(p1.y > aabb_max_y && p2.y > aabb_max_y) || (p1.y < aabb_min_y && p2.y < aabb_min_y);
			if ( ! outside) {
				col_springs[col_springs_num++] = &(s);
			}
			// prevent buffer overflow
			if (col_springs_num == MAX_COL_SPRINGS){
				INFO_PRINT("max collision springs (%d) reached!\n", MAX_COL_SPRINGS);
				break;
			}
		}
	}

	if (col_springs_num != 0)
	{
		// check further and collide
		bool fast_collision_handling = false;
		if(col_springs_num == 2 || col_springs_num == 1)
		{
			// try to use a fast collision response algorithm for usual cases
//			int return_val = fast_col_deck_with_polygon(train_points, train_points_num, train_springs, col_springs, col_springs_num);
			int return_val = 1;
#ifdef DEBUG
			debug_fastcol_ret[return_val]++;
#endif
			//return_val = 1;
			if(return_val == 0)
			{
				fast_collision_handling = true;
#ifdef DEBUG
				debug_fastcol_count++;
#endif
			}
		}

		// check if collision was handled by fast algorithm
		if( ! fast_collision_handling)
		{
			// use slow allround algorithm
			col_deck_with_polygon(train_points, train_points_num, train_springs, col_springs, col_springs_num);
#ifdef DEBUG
			debug_col_count++;
#endif
		}
	}
}

// returns vector of subdiv points (with abolute position)
void Physics::create_subdiv_points(const Vector2d& base, const Vector2d& line, Vector2d subdiv_points[], int& subdiv_points_num) const
{
	float_32 line_length = line.length();
	subdiv_points_num = int(ceil(line_length)); // div implicit subdevision length = 1;

//#ifdef DEBUG
	if(subdiv_points_num >= MAX_SPRING_SUBDIVS || subdiv_points_num <= 0 )
		ERROR_PRINT("This will corrupt the stack!\n"
				"subdiv_points_num not in [0, MAX_SPRING_SUBDIVS]:  %d out of [0, %d]: set MAX_SPRING_SUBDIVS higher or avoid too long springs (see settings->max_spring_len)\n",
				subdiv_points_num, MAX_SPRING_SUBDIVS);
//#endif

	// vector to add
	// line.normalize() * SUBDIV_LEN
	Vector2d vec_add = line;
	vec_add /= line_length ; // * implicit subdevision length = 1;

	for(int i = 0; i < subdiv_points_num; ++i){
		subdiv_points[i] = base + vec_add * float_32(i);
	}
	subdiv_points[subdiv_points_num++] = base + line;
}

void Physics::calc_and_apply_collision_forces(Spring** train_springs, Particle** poly_particles, const int poly_vertices_num, const Vector2d& subdiv_point_inside, Spring* col_deck_spring)
{
	Vector2d poly_points[Locomotive::points_num];
	for( int i=0 ; i<poly_vertices_num ; ++i )
		poly_points[i] = poly_particles[i]->pos;

	// get square pentration depth as force will be depend on this
	float_32 pen_depth_sq = Math_utils::get_distance_square_point_line_segment(subdiv_point_inside, poly_points[poly_vertices_num-1], poly_points[0]);
	int index_of_nearest_segment = poly_vertices_num -1;
	for (int i = 1; i < poly_vertices_num; i++)
	{
		float_32 dist_sq = Math_utils::get_distance_square_point_line_segment(subdiv_point_inside, poly_points[i-1], poly_points[i]);
		if( dist_sq < pen_depth_sq)
		{
			pen_depth_sq = dist_sq;
			index_of_nearest_segment = i-1;
		}
	}

	// FORCE to TRAIN
	// get DECK Particles to add this force
	float_32 len_to_p1 = Vector2d::distance(poly_particles[index_of_nearest_segment]->pos, subdiv_point_inside);
	float_32 len_to_p2 = Vector2d::distance(poly_particles[(index_of_nearest_segment+1)%poly_vertices_num]->pos, subdiv_point_inside);

	//float_32 len_train_spring = Vector2d::distance(poly_particles[index_of_nearest_segment]->pos, poly_particles[(index_of_nearest_segment+1)%poly_vertices_num]->pos);
	float_32 len_train_spring = train_springs[index_of_nearest_segment]->length;
	// abweichung von mehr als 1%
	////if(std::fabs(len_train_spring - train_springs[index_of_nearest_segment]->length) / len_train_spring > 0.01)
	////INFO_PRINT("len_train_sping calc: %f   stored: %f\n", len_train_spring, train_springs[index_of_nearest_segment]->length);

	// factor have to be exchanged (long distance -> small effect)
	float_32 factor_p1 = len_to_p2 / len_train_spring; // 0.0 - 1.0
	float_32 factor_p2 = len_to_p1 / len_train_spring; // 1.0 - 0.0

	// force direction
	// Vector2d center = Math_utils::center_of_polygon(poly_points, poly_vertices_num);
	// Vector2d force = subdiv_point_inside - center;        // old version (rects are looking a bit "round" and moving sidewards...)
	//
	// polygon points must have clockwise order! // is much better than above
	Vector2d force = Math_utils::get_normal(poly_points[index_of_nearest_segment] - poly_points[(index_of_nearest_segment+1)%poly_vertices_num]);

	// set force strength:
	// first normalize() to 1.0
	force.normalize();

	// multiply with:
	//   pen_depth_sq (square of penetration depth) makes collision a bit harder (squaring this even more)
	//   (1/(factor_p1 * factor_p2)) makes edges of the polygon harder (otherwise penetration around a vertex is a lot deeper)
	const float_32 poly_edge_correction =  std::fmax(factor_p1 * factor_p2, 0.005f); // limit force multiplication to 200 (=1/0.005)
	force *= pen_depth_sq * pen_depth_sq / poly_edge_correction;

	// force to Train Particles
	poly_particles[index_of_nearest_segment]->totale_force -=  force * factor_p1 * 5.0f;
	poly_particles[(index_of_nearest_segment+1)%poly_vertices_num]->totale_force -= force * factor_p2 * 5.0f;

	// FORCE to DECK
	// get DECK Particles to add this force
	len_to_p1 = Vector2d::distance(col_deck_spring->particles.first->pos, subdiv_point_inside);
	len_to_p2 = Vector2d::distance(col_deck_spring->particles.second->pos, subdiv_point_inside);

//	float_32 len_col_spring = Vector2d::distance(col_deck_spring->particles.first->pos, col_deck_spring->particles.second->pos);
	float_32 len_col_spring = col_deck_spring->length;

	// factor have to be exchanged (long distance -> small effect)
	factor_p1 = len_to_p2 / len_col_spring;
	factor_p2 = len_to_p1 / len_col_spring;

	// force to DECK spring
	col_deck_spring->particles.first->totale_force += force * factor_p1;
	col_deck_spring->particles.second->totale_force += force * factor_p2;
#ifdef DEBUG_FORCES
	col_deck_spring->particles.first->debug_force_1  += force * factor_p1;
	col_deck_spring->particles.second->debug_force_1 += force * factor_p2;
#endif
}

// new idea: subsection all springs and force inside points outwards (of course there will be no jumping arround etc.)
void Physics::col_deck_with_polygon(Particle** train_points, uint_fast8_t train_points_num, Spring** train_springs, Spring** col_deck_springs, uint_fast8_t col_deck_springs_num)
{
	// copy all train particle positions
	Vector2d train_points_tmp[Locomotive::points_num];
	for( int i=0 ; i<train_points_num ; ++i )
		train_points_tmp[i] = train_points[i]->pos;

	Vector2d subdiv_points[MAX_SPRING_SUBDIVS]; // put it on the stack; for performance
	int subdiv_points_num = 0;

	for( unsigned char s=0; s < col_deck_springs_num; ++s)
	{
		create_subdiv_points(col_deck_springs[s]->particles.first->pos,
				col_deck_springs[s]->particles.second->pos - col_deck_springs[s]->particles.first->pos,
				subdiv_points, subdiv_points_num);

		// intercept points per polygon segment
		for( int i=0; i < subdiv_points_num; ++i)
		{
			// optimize
			// if( in bounding box)
			{
				if(Math_utils::point_in_polygon(subdiv_points[i], train_points_tmp, train_points_num)){
					calc_and_apply_collision_forces(train_springs, train_points, train_points_num, subdiv_points[i], col_deck_springs[s]);
				}
			}
		}
	}
}

void Physics::train_add_forward_force()
{
	// add force to all locomotives
	// FIXME only if ground or rails are near
	for(int i=0; i < train->num_loc; ++i)
	{
		// test if locomotives are functional (not upside down or under water)
		unsigned char particles_under_water = 0;
		for(int p=0; p < 5; ++p)
		{
			if(train->locomotives[i]->points[p]->pos.y <= level->water_height)
				particles_under_water++;
		}
		// not under water?
		if(particles_under_water <= 2)
		{
			// train not upside down?
			if(train->locomotives[i]->points[3]->pos.y > train->locomotives[i]->points[2]->pos.y)
			{
				// train not beyond level width + 10000
				if(train->locomotives[i]->points[0]->pos.x < float_32(level->width) + 10000.0f)
				{
					// force vector is parallel to downside of the locomotive
					Vector2d force(train->locomotives[i]->points[1]->pos.x - train->locomotives[i]->points[2]->pos.x,
								train->locomotives[i]->points[1]->pos.y - train->locomotives[i]->points[2]->pos.y);

					// get direction (independent of locomotive size)
					force.normalize();

					// test if train is already at velocity_target
					float_32 avg_velocity = 0;
					avg_velocity += Math_utils::get_normal_projection(train->locomotives[i]->points[0]->velocity, force).length();
					avg_velocity += Math_utils::get_normal_projection(train->locomotives[i]->points[1]->velocity, force).length();
					avg_velocity += Math_utils::get_normal_projection(train->locomotives[i]->points[2]->velocity, force).length();
					avg_velocity += Math_utils::get_normal_projection(train->locomotives[i]->points[3]->velocity, force).length();
					avg_velocity += Math_utils::get_normal_projection(train->locomotives[i]->points[4]->velocity, force).length();
					avg_velocity = avg_velocity / 5.0f;

					if(avg_velocity < train->velocity_target) {
						force = force * train->tractive_power * TIME_STEP;
						train->locomotives[i]->points[0]->add_force( force );
						train->locomotives[i]->points[1]->add_force( force );
						train->locomotives[i]->points[2]->add_force( force );
					}
					// train uses breaks? ....
					// else force = force * TIME_STEP * -break_power;

					// if on bridge, add here responsive negative force
				}
			}
		}
	}
}

