//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm> // remove and remove_if for vector
#include <cmath>
#include <string>
#include <lua.hpp>
#include <lauxlib.h>
#include "vector2d.hpp"
#include "particle.hpp"
#include "spring.hpp"
#include "level.hpp"
#include "math_utils.hpp"
#include "train.hpp"
#include "error.hpp"
#include "settings.hpp"
#include "physics.hpp"
#include "hash.hpp"

// create a default level (new user level)
Level::Level()
{
	id = 0;
	budget = 10000;
	budget_left = 0;
	level_passed = false;
	editable = true;
	width = 560;
	custom_deck = true;
	ground = new float_32[width/5+1];
	ground_len = width/5+1;
	for( int i = 0 ; i < ground_len ; ++i ){
		if( i<=16 || i>=96 )
			ground[i] = 0.0f;
		else
			ground[i] = -80.0f;
	}
	// fixed anchors
	particles.push_back(new Particle(particle_id_count++, Vector2d(16*5, 0), 1, true, 0)); // 0 for not used in Train
	particles.push_back(new Particle(particle_id_count++, Vector2d(96*5, 0), 1, true, 0)); // 0 for not used in Train
}

Level::Level(int id, const std::string name, const std::string full_path)
	: id(id), name(name), full_path(full_path)
{
	custom_deck = true;
	budget = 0;
	budget_left = 0;
	level_passed = false;
	if(id == 0)
		editable = true;
	else
		editable = false;

//	std::string game_home_path = Settings::singleton()->game_home_path;
//	std::string dirname_levels = Settings::singleton()->dirname_levels;
//	// levels with id = 0 are user levels
//	if(id == 0)
//	{
//		// => full_path = name
//		this->name = name;
//		char tmp_filename[200];
//		snprintf(tmp_filename, 200, "%s/%s/%s",game_home_path.c_str(), dirname_levels.c_str(), name.c_str());
//		full_path = tmp_filename;
//	}
//	else
//	{
//		char tmp_filename[200];
//		snprintf(tmp_filename, 200, "%s/%s/lvl%03d%s",game_home_path.c_str(), dirname_levels.c_str(), id, Settings::singleton()->fname_ext_level.c_str());
//		full_path = tmp_filename;
//		char tmp_name[200];
//		snprintf(tmp_name, 200, "Level %d", id);
//		this->name = tmp_name;
//	}

	if(this->load_from_file()) {
		ERROR_PRINT("Error:  Couldn't load level \"%s\" (id: %d).\n", full_path.c_str(), id);
		load_failed = true;
	}
	else {
		load_failed = false;
	}
}

Level::~Level(){
	for (size_t i = 0; i < springs.size(); i++)
		delete springs[i];

	for (size_t i = 0; i < particles.size(); i++)
		delete particles[i];

	if(ground_len > 0)
		delete[] ground;
}

// copy constructor (deep copy)
Level::Level(const Level& level_to_copy)
{
	id = level_to_copy.id;
	name = level_to_copy.name;
	full_path = level_to_copy.full_path;
	width = level_to_copy.width;

	// copy ground
	ground_len = width/5+1;
	ground = new float_32[ground_len];
	// set all to zero
	for(int i = 0 ; i < ground_len ; ++i )
		ground[i] = level_to_copy.ground[i];

	water_height = level_to_copy.water_height;
	budget = level_to_copy.budget;
	budget_left = level_to_copy.budget_left;

	for(std::size_t p=0; p<level_to_copy.particles.size(); ++p)
	{
		//INFO_PRINT("Particle\t{%d, %d, %d, %f, %d},\n", (*p)->id, (int)(*p)->pos.x, (int)(*p)->pos.y, (*p)->mass, (*p)->fixed);
		Particle* new_p = new Particle(level_to_copy.particles[p]->id, Vector2d(level_to_copy.particles[p]->pos.x, level_to_copy.particles[p]->pos.y), (level_to_copy.particles[p])->mass, (level_to_copy.particles[p])->fixed, 0); // 0 for not used in Train
		particles.push_back(new_p);
	}
	for(std::size_t s=0; s<level_to_copy.springs.size(); ++s)
	{
		int firstid = level_to_copy.springs[s]->particles.first->id;
		int secondid = level_to_copy.springs[s]->particles.second->id;
		Particle* p1 = nullptr;
		Particle* p2 = nullptr;
		for (Particle_iter p = particles.begin(); p != particles.end(); ++p)
		{
			if((*p)->id == firstid)
				p1 = *p;

			if((*p)->id == secondid)
				p2 = *p;
		}
		//INFO_PRINT("Spring\t{%d, %d, %d},\n", (*s)->particles.first->id, (*s)->particles.second->id, (*s)->type);
		if( p1 != nullptr && p2 != nullptr)
			this->add_spring(p1, p2, level_to_copy.springs[s]->stiffness, level_to_copy.springs[s]->damping, level_to_copy.springs[s]->max_stretch, level_to_copy.springs[s]->type );
		else
			ERROR_PRINT("%s\n", "ERROR: particle id for p1 or p2 not found");
	}

	// TODO // springs and particles for simulation
	// TODO // if simulation starts they are copied from springs and particles
	// TODO // to sim_springs and sim_particles
	// TODO // then simulation ends max_colors there is copied back
	// TODO std::vector<Particle> sim_particles;
	// TODO typedef std::vector<Particle>::iterator Particle_iters;
	// TODO std::vector<Spring> sim_springs;
	// TODO typedef std::vector<Spring>::iterator Spring_iters;
	// TODO // indices of deck springs of vector sim_springs
	// TODO std::vector<uint_fast32_t> sim_deck_springs_i;

	level_passed = level_to_copy.level_passed;
	editable = level_to_copy.editable;
	custom_deck = level_to_copy.custom_deck;
	load_failed = level_to_copy.load_failed;
}

void Level::free_after_simulation()
{
	// clear vectors
	sim_springs.clear();
	sim_particles.clear();
	sim_deck_springs_i.clear();

	level_passed = false;
}

void Level::copy_for_simulation()
{
	// delete all old simulation data
	this->free_after_simulation();

	const int train_particles_num = 100;
	sim_particles.reserve(particles.size() + train_particles_num);
	sim_particles.resize(particles.size());
	// copy particles
	for(size_t i = 0; i < particles.size(); ++i)
	{
//		Particle* new_p = new Particle(**p);
		sim_particles[i].id            = particles[i]->id;
		sim_particles[i].pos           = particles[i]->pos;
		sim_particles[i].velocity      = particles[i]->velocity;
		sim_particles[i].mass          = particles[i]->mass;
		sim_particles[i].inv_mass      = 1.0f / particles[i]->mass;
		sim_particles[i].fixed         = particles[i]->fixed;
		sim_particles[i].used_in_train = particles[i]->used_in_train;
		sim_particles[i].totale_force  = {0,0};
		sim_particles[i].spring_links = 0;

//ifdef DEBUG_BLUB   // check for double particles (error in editor, load or store); uncomment ifdef if bielebridge is stable
		for(size_t j = 0; j < particles.size(); ++j)
		{
			// search only already copied particles
			if( j < i)
			{
				if( sim_particles[i].pos.x == sim_particles[j].pos.x && sim_particles[i].pos.y == sim_particles[j].pos.y )
					ERROR_PRINT("ERROR: 2 particles with same location; this is most likely an error in the editor; simulation will do strange things. ID: %d, %d\n", sim_particles[i].id, sim_particles[j].id);
				// TODO: merge particles, good software is self-healing
			}
		}
//endif
	}

	const int train_springs_num = 100;
	sim_springs.reserve(springs.size() + train_springs_num);
	sim_springs.resize(springs.size());
	for (size_t s = 0; s < springs.size(); ++s)
	{
		Particle* p1 = nullptr;
		Particle* p2 = nullptr;

		// search all particles
		// leads to exponential slowness
		for (size_t i = 0; i < sim_particles.size() && (p1==nullptr || p2==nullptr) ; ++i)
		{
			if(sim_particles[i].id == springs[s]->particles.first->id)
			{
				p1 = &(sim_particles[i]);
			}

			if(sim_particles[i].id == springs[s]->particles.second->id)
			{
				p2 = &(sim_particles[i]);
			}
		}
#ifdef DEBUG
		if(p1 == nullptr || p2 == nullptr)
			ERROR_PRINT("p1 (%p) or p2 (%p) not found\n", (void*) p1, (void*) p2);
#endif

		sim_springs[s].particles.first  = p1;
		sim_springs[s].particles.second = p2;
		sim_springs[s].stiffness        = springs[s]->stiffness;
		sim_springs[s].damping          = springs[s]->damping;
		sim_springs[s].max_stretch      = springs[s]->max_stretch;
		sim_springs[s].type             = springs[s]->type;
		sim_springs[s].status           = Spring::NORMAL;
		sim_springs[s].length           = (float_32) fabs((p1->pos - p2->pos).length());
		sim_springs[s].inv_length       = 1.0f / (float_32) fabs((p1->pos - p2->pos).length());
	}

	// save all deck spring indices
	sim_deck_springs_i.clear();
	for (size_t s = 0; s < sim_springs.size(); ++s)
	{
		if(sim_springs[s].type == Spring::DECK)
		{
			sim_deck_springs_i.push_back( (uint_fast32_t) s);
		}
	}
}

// add a particle to the level
Particle* Level::add_particle (const Vector2d& pos, const float_32 mass, const bool fixed, const bool used_in_train)
{
	// prevent double adding particles
	for (size_t i = 0; i < particles.size(); i++){
		if(pos == particles[i]->pos){
			if(fixed)
				particles[i]->fixed = true;
			return particles[i];
		}
	}

	Particle* p = new Particle(particle_id_count++, pos, mass, fixed, used_in_train);
	particles.push_back(p);
	return p;
}

Particle* Level::add_sim_particle (const Vector2d& arg_pos, float_32 m, bool fixed, bool used_in_train)
{
	Particle p = Particle(particle_id_count++, arg_pos, m, fixed, used_in_train);
	p.inv_mass = 1.0f/p.mass;
	sim_particles.push_back(p);
	return &(sim_particles.back());
}

void Level::delete_particle(Particle* p, bool force)
{	// don't remove fixed particles
	if(!force && p->fixed) return;

	// delete all conected springs and at least the particle itself

	// this prevents a program hang if something went wrong...
	unsigned int hang_counter = 9999; // limits also max links per particle
	while(p->spring_links > 0 && hang_counter != 0)
	{
		hang_counter--;
		for(size_t i = 0; i < springs.size(); ++i)
		{
			if(springs[i]->particles.first == p || springs[i]->particles.second == p)
			{
				Particle* p1 = springs[i]->particles.first;
				Particle* p2 = springs[i]->particles.second;

				delete springs[i];
				// to avoid invalid following array indices
				// replace element with last element, after that pop_back()
				springs[i] = springs.back();
				springs.pop_back();
				// recheck element from springs.back();
				if(i > 0) i--;

				if(p1->spring_links < 1 && p1 != p) delete_particle(p1);
				if(p2->spring_links < 1 && p2 != p) delete_particle(p2);
			}
		}
	}
	if(hang_counter == 0){
		ERROR_PRINT("detected particle (id %d) with too high link count %d.\n", p->id, p->spring_links);
	}
	if(hang_counter < 9998){
		INFO_PRINT("hang_counter %d showes that multiple cycles there needed.\n", hang_counter);
	}

	for(size_t i = 0; i < particles.size(); ++i)
	{
		if(particles[i] == p)
		{
			delete particles[i];
			// to avoid invalid following array indices
			// replace element with last element, after that pop_back()
			particles[i] = particles.back();
			particles.pop_back();
			return;
		}
	}
}

Spring* Level::add_spring(Particle* p1, Particle* p2, float_32 stiffness, float_32 damping, float_32 max_stretch, Spring::Spring_Type type)
{
	if(p1 == p2){
		ERROR_PRINT("adding Spring with same particles [%f,%f] and [%f,%f] of type %d\n", p1->pos.x, p1->pos.y, p2->pos.x, p2->pos.y, type);
		return nullptr;
	}

	Spring* new_spring = new Spring(p1, p2, stiffness, damping, max_stretch, type);
	// prevent double adding springs
	for (size_t i = 0; i < springs.size(); i++)
	{
		if(*(springs[i]) == *new_spring)
		{
			// if spring already exists, delete it
			delete new_spring;
			return springs[i];
		}
	}

//#ifdef DEBUG
	int subdiv_points_num = int( float( ceil(new_spring->length) )* (1.0f + Settings::singleton()->spring_max_stretch) + 2.0f);
	if(subdiv_points_num >= Physics::MAX_SPRING_SUBDIVS)
		ERROR_PRINT("subdiv_points_num >= MAX_SPRING_SUBDIVS: %d >= %d: this might corrupt the stack at simulation!\n"
				"set MAX_SPRING_SUBDIVS higher or avoid too long springs (see settings->max_spring_len)\n",
				subdiv_points_num, Physics::MAX_SPRING_SUBDIVS);
//#endif

	// no similar spring found, add it
	springs.push_back(new_spring);
	return new_spring;
}

Spring* Level::add_sim_spring(Particle* p1, Particle* p2, float_32 stiffness, float_32 damping, float_32 max_stretch, Spring::Spring_Type type)
{
	Spring new_spring = Spring(p1, p2, stiffness, damping, max_stretch, type);
	sim_springs.push_back(new_spring);

	// this is strange but have to be done for train springs...
	sim_springs.back().length = (float_32) fabs((p1->pos - p2->pos).length()) ;
	sim_springs.back().inv_length = 1.0f / (float_32) fabs((p1->pos - p2->pos).length()) ;

	return &(sim_springs.back());
}

void Level::delete_spring(Spring* s)
{
	if(!s) return;

	Particle* p1 = s->particles.first;
	Particle* p2 = s->particles.second;

	for (Spring_iter i = springs.begin (); i != springs.end (); ++i)
	{
		if (*i == s)
		{
			delete *i;
			springs.erase(i);
			break;
		}
	}

	if(p1->spring_links < 1) delete_particle(p1);
	if(p2->spring_links < 1) delete_particle(p2);
}

int Level::load_from_file()
{
	struct lua_State* L = luaL_newstate();
	if(luaL_dofile(L, full_path.c_str()))
	{
		ERROR_PRINT("%s\n", lua_tostring(L, -1));
		lua_close(L);
		return 1;
	}
	if( ! lua_istable(L, -1))
	{
		ERROR_PRINT("Table expected, got `%s`\n", lua_typename(L, lua_type(L, -1)));
		lua_close(L);
		return 1;
	}

/* for extended debug file reading
#define STACKDUMP top = lua_gettop(L);\
	for(int i=1; i<=top; i++){\
		int t = lua_type(L, i);\
		switch(t){\
				case LUA_TSTRING: 	{ printf("`%s` ", lua_tostring(L, i)); break;}\
				case LUA_TBOOLEAN: 	{ printf(lua_toboolean(L, i)? "true " : "false "); break;}\
				case LUA_TNUMBER: 	{ printf("%g ", lua_tonumber(L, i)); break;}\
				case LUA_TTABLE: 	{ printf("table "); break; }\
				default: { printf("(unk: %s) ", lua_typename(L, t)); break;}\
			}\
		}\
		printf("\n");
	int top;
*/

	lua_getfield(L, -1, "id");
	int id_from_file = (int) lua_tointeger(L, -1);
	if(id != id_from_file) {
		ERROR_PRINT("Error: Level id from file '%s' not as expected. Got %d, expected %d\n", full_path.c_str(), id_from_file, id );
	}
	lua_pop(L, 1);

	lua_getfield(L, -1, "width");
	width = (int) lua_tointeger(L, -1);
	if(width > Settings::singleton()->max_level_width) {
		ERROR_PRINT("Error: Level width is %d. This is bigger than max_level_width (%d). Using this.\n",
				width, Settings::singleton()->max_level_width );
		width = Settings::singleton()->max_level_width;
	}
	lua_pop(L, 1);

	lua_getfield(L, -1, "water_height");
	water_height = (float_32) lua_tonumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "budget");
	budget = (int) lua_tointeger(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "custom_deck");
	custom_deck = (bool) lua_toboolean(L, -1);
	lua_pop(L, 1);

	int num;
#define FOR_ALL_ITEMS_IN(table) \
	lua_getfield(L, -1, # table);\
	num = 1;\
	lua_pushnumber(L, num);\
	lua_gettable(L, -2);\
	while(lua_type(L, -1))

	// get all particles
	FOR_ALL_ITEMS_IN(particles)
	{
		int id=0, posx=0, posy=0, fixed=0;
		float_32 mass=0;
		for( int item=1; item <=5 ; ++item)
		{
			lua_pushnumber(L, item);
			lua_gettable(L, -2);
			switch(item)
			{
				case 1: id    = (int) lua_tointeger(L, -1); break;
				case 2: posx  = (int) lua_tointeger(L, -1); break;
				case 3: posy  = (int) lua_tointeger(L, -1); break;
				case 4: mass  = (float_32) lua_tonumber(L, -1); break;
				case 5: fixed = (int) lua_tointeger(L, -1); break;
			}
			lua_pop(L, 1);
		}
		// printf("particle: %d, [%d, %d], %f, %d\n", id, posx, posy, mass, fixed);
		Particle* new_p = new Particle(id, Vector2d((float_32) posx, (float_32) posy), mass, fixed, 0); // 0 for not used in Train
		particles.push_back(new_p);
		if( id > particle_id_count) particle_id_count = id;
		lua_pop(L, 1);

		lua_pushnumber(L, ++num);
		lua_gettable(L, -2);
	}
	lua_pop(L, 2);
	particle_id_count++;

	// get all springs
	// load defaults
	float_32 stiffness = Settings::singleton()->spring_stiffness;
	float_32 damping = Settings::singleton()->spring_damping;
	float_32 max_stretch = Settings::singleton()->spring_max_stretch;
	FOR_ALL_ITEMS_IN(springs)
	{
		int firstid=0, secondid=0, type=0;
		Spring::Spring_Type spring_type;
		for( int item=1; item <=3 ; ++item)
		{
			lua_pushnumber(L, item);
			lua_gettable(L, -2);
			switch(item)
			{
				case 1: firstid  = (int) lua_tointeger(L, -1); break;
				case 2: secondid = (int) lua_tointeger(L, -1); break;
				case 3: type = (int) lua_tointeger(L, -1); break;
			}
			lua_pop(L, 1);
		}
		// printf("spring: %d, %d\n", firstid, secondid);
		// search for particle ids
		Particle* p1=0;
		Particle* p2=0;
		for (Particle_iter p = particles.begin(); p != particles.end() && (p1==0 || p2==0); ++p)
		{
			if((*p)->id == firstid)
				p1 = *p;

			if((*p)->id == secondid)
				p2 = *p;
		}
		if( p1==0 ) fprintf(stderr, "Error: Spring between not existing particle: %d\n", firstid);
		if( p2==0 ) fprintf(stderr, "Error: Spring between not existing particle: %d\n", secondid);
		switch(type)
		{
			case 0: spring_type = Spring::BRIDGE; break;
			case 1: spring_type = Spring::DECK; break;
			default:
					fprintf(stderr, "Error: unknowen type of spring %d. Using BRIDGE.\n", type);
					spring_type = Spring::BRIDGE;
		}

		add_spring(p1, p2, stiffness, damping, max_stretch, spring_type );
		lua_pop(L, 1);

		lua_pushnumber(L, ++num);
		lua_gettable(L, -2);
	}
	lua_pop(L, 2);

	// get all groundpoints
	ground = new float_32[width/5+1];
	ground_len = width/5+1;
	// set all to zero
	for(int i = 0 ; i < ground_len ; ++i )
		ground[i] = 70.0f;

	FOR_ALL_ITEMS_IN(ground)
	{
		float_32 gpos = (float_32) lua_tonumber(L, -1);
		lua_pop(L, 1);
		// printf("\t%f,\n", gpos);
		ground[num-1] = (float_32) gpos;

		lua_pushnumber(L, ++num);
		lua_gettable(L, -2);
		if( num > width/5 +2)
		{
			fprintf(stderr, "Warnig: too many ground positions, will ignor all superfluous points.\n");
			break;
		}
	}
	lua_pop(L, 2);
	if(num < ground_len)
	{
		fprintf(stderr, "Warnig: too few ground positions, will fill missing points with zero.\n");
		for( int i = num-1 ; i < ground_len ; ++i )
			ground[i] = 0;
	}
	lua_close(L);
	return 0;
}

int Level::save_to_file()
{
	std::string full_filename; // with path included
	const size_t tmp_filename_size = 255; // FIXME use dynamic memory
	char tmp_filename[tmp_filename_size];

	std::string game_home_path = Settings::singleton()->game_home_path;
	std::string dirname_levels = Settings::singleton()->dirname_levels;
	// decide if predefined level is saved:
	if(id == 0)
	{
		if(full_path.length() >= tmp_filename_size){
			char tmp[tmp_filename_size+1]; // FIXME use dynamic memory
			std::snprintf(tmp, tmp_filename_size, "%s", full_path.c_str());
			ERROR_PRINT("full_path %s... is too long.\n", tmp);
			return 1;
		}
		if(full_path.length() == 0){
			ERROR_PRINT("%s\n", "full_path is empty.");
			return 1;
		}
		std::snprintf(tmp_filename, tmp_filename_size, "%s", full_path.c_str());
		full_filename = tmp_filename;
	}
	else
	{
		std::snprintf(tmp_filename, tmp_filename_size, "%s/%s/lvl%03d%s",game_home_path.c_str(), dirname_levels.c_str(), id, Settings::singleton()->fname_ext_level.c_str());
		full_filename = tmp_filename;
	}

	INFO_PRINT("Save level to file %s\n", full_filename.c_str());

	FILE* file = fopen(full_filename.c_str(), "w");
	if( ! file )
	{
		ERROR_PRINT("Can not open file %s to save level!\n", full_filename.c_str());
		return 1;
	}
	// some value as place for checksum (use the same for reading)
	fprintf(file, "-- 0000000000000000000000000000000000000000000000000000000000000000\n");

	fprintf(file, "-- this file will be parsed by the Lua interpreter,\n-- so I do not have to care about this ;-)\n\n");
	fprintf(file, "return{\n");
	fprintf(file, "\tid=%d,\n",id);
	fprintf(file, "\twidth=%d,\n",width);
	fprintf(file, "\twater_height=%d,\n", (int) water_height);
	fprintf(file, "\tbudget=%d,\n", budget);
	if(custom_deck)
		fprintf(file, "\tcustom_deck=true,\n");
	else
		fprintf(file, "\tcustom_deck=false,\n");

	fprintf(file, "\tparticles={\n--\t\t{id, pos.x, pox.y, mass, fixed},\n");
	for (Particle_iter p = particles.begin(); p != particles.end(); ++p)
	{
		fprintf(file, "\t\t{%d, %d, %d, %f, %d},\n", (*p)->id, (int)(*p)->pos.x, (int)(*p)->pos.y, (*p)->mass, (*p)->fixed);
	}
	fprintf(file, "\t},\n");

	fprintf(file, "\tsprings={\n--\t\t{firstID, secondID, type},\n");
	for (Spring_iter s = springs.begin (); s != springs.end (); ++s)
	{
		fprintf(file, "\t\t{%d, %d, %d},\n", (*s)->particles.first->id, (*s)->particles.second->id, (*s)->type);
	}
	fprintf(file, "\t},\n");

	fprintf(file, "\tground={\n");
	for( int i=0, g=0; i<= width; i+=5,++g)
	{
		fprintf(file, "\t\t%d,\n",(int) ground[g]);
	}
	fprintf(file, "\t},\n");

	fprintf(file, "}\n");
	fclose(file);

	// reopen rw for reading and writing checksum
	// checksums (to prevent level renaming or budget fraud):  not used yet
	file = fopen(full_filename.c_str(), "r+");
	if( ! file )
	{
		ERROR_PRINT("Error: Can not open file %s to save checksum!", full_filename.c_str());
		return 1;
	}

	// get file size
	fseek(file, 0L, SEEK_END);
	long int file_size = ftell(file);
	if (file_size == -1L)
    {
		ERROR_PRINT("ftell() failed for file %s\n", full_filename.c_str());
		fclose(file);
		return 1;
    }
	uint8_t* file_buffer;
	file_buffer = new uint8_t[file_size];

	// seek back to the beginning:
	fseek(file, 0L, SEEK_SET);
	// copy file to buffer
	unsigned char ch;
	size_t count = 0;
	while(fread(&ch, 1, 1, file) == 1)
	{
		file_buffer[count] = ch;
		count++;
	}

	// hash file buffer
	Hash* hash = new Hash();
	int hash_return_value;
	uint8_t hash_value[32];
	size_t data_bit_len = (size_t) file_size * 8;
	hash_return_value = (int) hash->Hash_256(256, (const uint8_t*) file_buffer, data_bit_len, (uint8_t*) hash_value);
	if(hash_return_value != SKEIN_SUCCESS)
	{
		ERROR_PRINT("skein hash returned %d\n", hash_return_value);
	}

	// ouput hash
	fseek(file, 0L, SEEK_SET);
	fprintf(file, "-- ");
	for (int i=0;i < 32;i++)
	{
		fprintf(file, "%02x",  hash_value[i]);
	}

	fclose(file);
	delete file_buffer;
	delete hash;

	return 0;
}

void Level::clear_bridge()
{
	while (!springs.empty())
	{
		springs.pop_back();
	}
	// removes all not fixed particles
	particles.erase(
			std::remove_if( particles.begin(),
							particles.end(),
							[](Particle* p){return !p->fixed;} // lambda function
							),
			particles.end() );

	// set spring links to 0 for all remaing particles
	// renumber all fixed particles
	for(size_t i = 0; i < particles.size(); ++i)
	{
		particles[i]->spring_links = 0;
		particles[i]->id = (unsigned int)i + 1;
	}
}

Particle* Level::get_nearest_particle(const Vector2d& pos, std::vector<Particle*>& p_vec)
{
	Particle_iter test = p_vec.begin();
	if( test != p_vec.end() )
	{
		Particle* return_particle = *(p_vec.begin());
		float_32 min_dist_square = Vector2d::distance_square(pos, return_particle->pos);

		for (Particle_iter i = p_vec.begin(); i != p_vec.end(); ++i) {
			if( Vector2d::distance_square(pos, (*i)->pos) < min_dist_square){
				return_particle = (*i);
				min_dist_square = Vector2d::distance_square(pos, (*i)->pos);
			}
		}
		return return_particle;
	}
	else
	{
		return nullptr;
	}
}

void Level::copy_particles_and_springs_inside_rect(const float_32 x, const float_32 y, const float_32 w, const float_32 h)
{
	copy_particles.clear();
	copy_springs.clear();

	// calc rect with w and h bigger 0
	float_32 rect_x = x;
	float_32 rect_y = y;
	float_32 rect_w = w;
	float_32 rect_h = h;
	if(w < 0)
	{
		rect_x += w;
		rect_w = -w;
	}
	if(h < 0)
	{
		rect_y += h;
		rect_h = -h;
	}

	for(size_t i = 0; i < particles.size(); ++i)
	{
		// inside rect?
		if( particles[i]->pos.x > rect_x && particles[i]->pos.x < rect_x + rect_w &&
				particles[i]->pos.y > rect_y && particles[i]->pos.y < rect_y + rect_h)
		{
			// copy
			Particle* copied_particle = new Particle(*particles[i]);
			// fixed particles have to be moveable for copy & paste
			copied_particle->fixed = false;
			copy_particles.push_back(copied_particle);
		}
	}
	for(size_t i = 0; i < springs.size(); ++i)
	{
		// both end points inside rect?
		const Particle* first_p = springs[i]->particles.first;
		const Particle* second_p = springs[i]->particles.second;
		if( first_p->pos.x > rect_x && first_p->pos.x < rect_x + rect_w && first_p->pos.y > rect_y && first_p->pos.y < rect_y + rect_h &&
			second_p->pos.x > rect_x && second_p->pos.x < rect_x + rect_w && second_p->pos.y > rect_y && second_p->pos.y < rect_y + rect_h)
		{
			// copy
			Spring* copied_spring = new Spring(*springs[i]);
			copy_springs.push_back(copied_spring);
		}
	}

	// update pointers to copied particles
	// (exponential slow!)
	for(size_t i = 0; i < copy_springs.size(); ++i)
	{
#ifdef DEBUG
		Particle* old_first_ptr = copy_springs[i]->particles.first;
		Particle* old_second_ptr = copy_springs[i]->particles.second;
#endif
		for(size_t p = 0; p < copy_particles.size(); ++p)
		{
			if(copy_springs[i]->particles.first->pos == copy_particles[p]->pos)
				copy_springs[i]->particles.first = copy_particles[p];

			if(copy_springs[i]->particles.second->pos == copy_particles[p]->pos)
				copy_springs[i]->particles.second = copy_particles[p];
		}
#ifdef DEBUG
		// test if both pointers were updated
		if(  old_first_ptr == copy_springs[i]->particles.first || old_second_ptr == copy_springs[i]->particles.second)
			ERROR_PRINT("Couldn't update particle pointers (ids %d, %d)\n", old_first_ptr->id, old_second_ptr->id );
#endif
	}
}

void Level::free_copied_particles_and_springs()
{
	for(size_t i = 0; i < copy_springs.size(); ++i)
		delete copy_springs[i];

	for(size_t i = 0; i < copy_particles.size(); ++i)
		delete copy_particles[i];

	// clear vectors
	copy_springs.clear();
	copy_particles.clear();
}

void Level::delete_springs_from_area(const float_32 x, const float_32 y, const float_32 w, const float_32 h)
{
	// calc rect with w and h bigger 0
	float_32 rect_x = x;
	float_32 rect_y = y;
	float_32 rect_w = w;
	float_32 rect_h = h;
	if(w < 0)
	{
		rect_x += w;
		rect_w = -w;
	}
	if(h < 0)
	{
		rect_y += h;
		rect_h = -h;
	}

	// removes all springs inside area
	size_t del_count;
	do
	{
		del_count = 0;
		for(size_t i = 0; i < springs.size(); ++i)
		{
			// inside rect?
			if( springs[i]->particles.first->pos.x  > rect_x && springs[i]->particles.first->pos.x  < rect_x + rect_w &&
				springs[i]->particles.first->pos.y  > rect_y && springs[i]->particles.first->pos.y  < rect_y + rect_h &&
				springs[i]->particles.second->pos.x > rect_x && springs[i]->particles.second->pos.x < rect_x + rect_w &&
				springs[i]->particles.second->pos.y > rect_y && springs[i]->particles.second->pos.y < rect_y + rect_h )
			{
				// delete from level
				this->delete_spring(springs[i]);
				del_count++;
			}
		}
	} // TODO: this is ugly, rewrite with remove_if + lambda function
	while(del_count > 0);
}

void Level::add_copied_particles_and_springs()
{
	bool fixed = false;
	bool used_in_train = false;
	Particle* first;
	Particle* second;
	Spring* spring;
	for(size_t i = 0; i < copy_springs.size(); ++i)
	{
		spring = copy_springs[i];
		// check if particles are inside ground (only anchors are allowed)
		const float_32 small_distance = 0.001f;
		if( ((! this->p_inside_ground(spring->particles.first->pos)) || this->anchor_near(spring->particles.first->pos, small_distance)) &&
				((! this->p_inside_ground(spring->particles.second->pos)) || this->anchor_near(spring->particles.second->pos, small_distance)))
		{
			// add both particles
			first  = this->add_particle(spring->particles.first->pos,  spring->particles.first->mass, fixed, used_in_train);
			second = this->add_particle(spring->particles.second->pos, spring->particles.second->mass, fixed, used_in_train);
			if(first != nullptr && second != nullptr)
			{
				this->add_spring(first, second, spring->stiffness, spring->damping, spring->max_stretch, spring->type);
			}
		}
	}
}

void Level::move_copied_particles_and_springs_to(const float_32 x, const float_32 y)
{
	if(copy_particles.size() < 1)
		return;

	const Vector2d target_pos = {x, y};
	// get the first particle as focus point
	// calculate difference to target point
	const Vector2d diff_vec = target_pos - copy_particles[0]->pos;

	// move all particles (springs too)
	for(size_t i = 0; i < copy_particles.size(); ++i)
		copy_particles[i]->pos += diff_vec;
}

unsigned int Level::get_new_springs_num(const float_32 x, const float_32 y)
{
	unsigned int count = 0;

	// tmp vars
	Particle* first_p_in_level;
	Particle* second_p_in_level;
	Spring* spring_to_copy;

	// check if a spring at this position is already present
	for(size_t i = 0; i < copy_springs.size(); ++i)
	{
		spring_to_copy = copy_springs[i];
		first_p_in_level = nullptr;
		second_p_in_level= nullptr;
		// only if both particles of a spring are already present, spring maybe not new
		for(size_t p = 0; p < particles.size(); p++)
		{
			// search already present particles
			if(spring_to_copy->particles.first->pos == particles[p]->pos)
			{
				first_p_in_level = particles[p];
			}
			if(spring_to_copy->particles.second->pos == particles[p]->pos)
			{
				second_p_in_level = particles[p];
			}
		}
		// only test spring itself if both particles already present
		if(first_p_in_level != nullptr && second_p_in_level != nullptr)
		{
			bool spring_found_in_level = false;
			for(size_t s = 0; s < springs.size(); s++)
			{
				if((springs[s]->particles.first == first_p_in_level && springs[s]->particles.second == second_p_in_level) ||
					(springs[s]->particles.first == second_p_in_level && springs[s]->particles.second == first_p_in_level))
				{
					spring_found_in_level = true;
				}
			}
			if( ! spring_found_in_level)
				count++;
		}
		else
		{
			// definitely new
			count++;
		}
	}
	return count;
}

void Level::flip_x_copied_particles_and_springs()
{
	if(copy_particles.size() < 2)
		return;

	// get min and max x
	float_32 min = copy_particles[0]->pos.x;
	float_32 max = copy_particles[0]->pos.x;
	for(size_t i = 1; i < copy_particles.size(); ++i)
	{
		if(copy_particles[i]->pos.x > max)
			max = copy_particles[i]->pos.x;

		if(copy_particles[i]->pos.x < min)
			min = copy_particles[i]->pos.x;
	}
	// get half of width and align it to grid
	float_32 center = min + (max - min) / 2.0f;
	center = (float_32) Math_utils::round_to(center, 10);

	// mirror all particles at center
	for(size_t i = 0; i < copy_particles.size(); ++i)
	{
		copy_particles[i]->pos.x += 2*(center - copy_particles[i]->pos.x);
			max = copy_particles[i]->pos.x;
	}
}

// FIXME use this function thenever possible (it is well tested)
/// @param x position
/// @return y position of ground at given x position (even outside of level)
float_32 Level::get_ground_height_at(const float_32 x_pos) const
{
	// left or right outside level
	if(x_pos <= 0.0f)
		return ground_left_outside;

	if(x_pos >= (float_32(ground_len) -1.0f) * 5.0f)
		return ground_right_outside;

	// do interpolation (Cross-multiplication)
	const int left_ground_index = int(floor( x_pos / 5.0f ));
	const int right_ground_index = int(ceil( x_pos / 5.0f ));

	float_32 left_ground_height, right_ground_height;
	if(right_ground_index <= width/5 && right_ground_index >= 0)
		right_ground_height = ground[right_ground_index];
	else // out of level boundaries
	{
		if(right_ground_index < 0)
			right_ground_height = ground_left_outside;
		else
			right_ground_height = ground_right_outside;
	}
	if(left_ground_index <= width/5 && left_ground_index >= 0)
		left_ground_height = ground[left_ground_index];
	else // out of level boundaries
	{
		if(left_ground_index < 0)
			left_ground_height = ground_left_outside;
		else
			left_ground_height = ground_right_outside;
	}

	const float_32 height_difference = right_ground_height - left_ground_height;
	const float_32 slope = height_difference / 5.0f;

	const float_32 x_distance_to_lower = x_pos - float_32(left_ground_index * 5);

	return left_ground_height + x_distance_to_lower * slope;
}

bool Level::anchor_near(const Vector2d& pos, const float_32 distance)
{
	Particle_iter test = particles.begin();
	if( test != particles.end() )
	{
		float_32 distance_sq = distance * distance;
		for (Particle_iter i = particles.begin(); i != particles.end(); ++i) {
			if((*i)->fixed && Vector2d::distance_square(pos, (*i)->pos) < distance_sq){
				return true;
			}
		}
	}
	return false;
}

// returns if a point p is on a spring (of current level)
// return false if it is on a end point
bool Level::on_spring_line(const Vector2d& pos)
{
	// if point is closer than this distance it counts as on spring
	const float_32 DISTANCE_SQ = 1.0f; // square of distance

	bool on_spring = false;
	float_32 x1,y1,x2,y2;
	for (Spring_iter s = springs.begin (); s != springs.end() && !on_spring; ++s)
	{
		// fast bounding box test
		x1 = (*s)->particles.first->pos.x;
		y1 = (*s)->particles.first->pos.y;
		x2 = (*s)->particles.second->pos.x;
		y2 = (*s)->particles.second->pos.y;

		if(!(pos.x < x1 && pos.x < x2) && !(pos.x > x1 && pos.x > x2) &&
				!(pos.y < y1 && pos.y < y2) && !(pos.y > y1 && pos.y > y2))
		{
			// particle is inside bounding box, now do the real test
			// is it on the line but not near the end points
			if(Math_utils::get_distance_square_point_line_segment(pos, (*s)->particles.first->pos, (*s)->particles.second->pos) < DISTANCE_SQ
					&& Vector2d::distance_square(pos, (*s)->particles.first->pos) > DISTANCE_SQ
					&& Vector2d::distance_square(pos, (*s)->particles.second->pos) > DISTANCE_SQ ){
				on_spring = true;
			}
		}
	}
	return on_spring;
}

int Level::fit_ground_index_to_boundaries(const int index) const
{
	int valid_index = Math_utils::max(index, 0); // avoid negativ index
	return Math_utils::min(valid_index, ground_len-1); // avoid to high index
}

/// @return true if point is inside ground
bool Level::p_inside_ground(const Vector2d& p) const
{
	return p.y < this->get_ground_height_at(p.x);
}

void Level::copy_max_colors_after_simulation()
{
	// sanity check
	if(sim_springs.size() >= springs.size())
	{
		for (size_t i = 0; i < springs.size(); i++)
			springs[i]->set_max_color_raw( sim_springs[i].get_max_color_raw());
	}
	else
		ERROR_PRINT("%s\n","Can not copy colors if sim_springs.size() < springs.size(). Do not call this function with this level state.");
}

bool Level::s_double_cross_with_same_ground(const Spring* s1, const Spring* s2, Vector2d& ground_before, Vector2d& ground_center ,Vector2d& ground_after) const
{
	if(s1 == nullptr || s2 == nullptr)
		return false;

	// check if both ends are above ground (this is a precondition)
	if(this->p_inside_ground(s1->particles.first->pos) || this->p_inside_ground(s1->particles.second->pos))
		return false;
	if(this->p_inside_ground(s2->particles.first->pos) || this->p_inside_ground(s2->particles.second->pos))
		return false;

	// get all ground points in between
	const float_32 min_x = std::fmin(s1->particles.first->pos.x, s1->particles.second->pos.x);
	const float_32 max_x = std::fmax(s1->particles.first->pos.x, s1->particles.second->pos.x);

	const int min_ground_index = int(floor( min_x / 5.0f ));
	const int max_ground_index = int( ceil( max_x / 5.0f ));

	int ground_cross = 0;
	for(int i = min_ground_index+1; i <= max_ground_index; ++i)
	{
		float_32 gl_start_x = float_32((i-1)*5);
		float_32 gl_end_x = float_32(i*5);
		Vector2d ground_line_start = Vector2d(gl_start_x, this->get_ground_height_at(gl_start_x));
		Vector2d ground_line_end = Vector2d(gl_end_x, this->get_ground_height_at(gl_end_x));

		if(Math_utils::line_segments_cross(s1->particles.first->pos, s1->particles.second->pos, ground_line_start, ground_line_end) &&
			Math_utils::line_segments_cross(s2->particles.first->pos, s2->particles.second->pos, ground_line_start, ground_line_end))
		{
			if(ground_cross ==0)
			{
				ground_before = ground_line_start;
				ground_center = ground_line_end;
			}
			if(ground_cross ==1)
			{
				if(ground_center == ground_line_start)
					ground_after = ground_line_end;
				else
					ERROR_PRINT("%s\n", "didnt expect this");
			}
			ground_cross++;
		}
	}
	if(ground_cross == 2)
	{
		return true;
	}
	else
		return false;
}

int Level::get_double_cross_train_line(const int ground_index, const Vector2d* train_points, const int train_points_num) const
{
	int line_index_ret = -1;
	if(ground_index <= 0 || ground_index >= ground_len -1)
		return line_index_ret;

	// test line before and after gound_index
	Vector2d ground_before = Vector2d(float_32(ground_index -1) * 5.0f, ground[ground_index -1]);
	Vector2d ground_center = Vector2d(float_32(ground_index   ) * 5.0f, ground[ground_index   ]);
	Vector2d ground_after = Vector2d( float_32(ground_index +1) * 5.0f, ground[ground_index +1]);

	for(int i = 0; i < train_points_num; ++i)
	{
		// both ground lines cross same train line segment?
		if(Math_utils::line_segments_cross(ground_before, ground_center, train_points[i], train_points[(i+1)%train_points_num]) &&
				Math_utils::line_segments_cross(ground_center, ground_after, train_points[i], train_points[(i+1)%train_points_num]))
		{
			// save and return line index
			line_index_ret = i;
		}
	}
	return line_index_ret;
}

void Level::adjust_level_width(const int increase)
{
	// get new level array
	int new_width = width + increase;
	int new_ground_len = new_width/5+1;
	float_32* new_ground = new float_32[new_width/5+1];

	// copy old to new and set new ground points to 0
	for( int i = 0 ; i < new_ground_len ; ++i )
	{
		if(i < ground_len)
			new_ground[i] = ground[i];
		else
			new_ground[i] = 0;
	}

	delete[] ground;
	ground = new_ground;
	width = new_width;
	ground_len = new_ground_len;
}

void Level::level_shift_left(const int amount)
{
	// move ground ///////////////////////////////////////
	int grid_points = amount / 10;
	// move level to the left
	if(grid_points < 0)
	{
		for( int i = ground_len -1 ; i >= 0  ; --i )
		{
			if(i+(grid_points*2) >= 0)
				ground[i] = ground[i+(grid_points*2)];
			else
				ground[i] = 0;
		}
	}
	else // move it to the right
	{
		for( int i = 0 ; i < ground_len - 1; ++i )
		{
			if(i+(grid_points*2) <= ground_len - 1)
				ground[i] = ground[i+(grid_points*2)];
			else
				ground[i] = 0;
		}
	}

	// move particles ///////////////////////////////////////

	// recalc maybe rounded move distance
	int move_dist = grid_points * 10;
	Vector2d move_vec = Vector2d( float_32(move_dist), 0);

	for( size_t i = 0 ; i < particles.size() ; ++i )
	{
		particles[i]->pos -= move_vec;
	}
}

