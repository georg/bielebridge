//  bielebridge -- A free software bridge construction game
//  Copyright (C) 2008-2015 Georg Gottleuber (dev@bielebridge.net)
//
//
//  This file is part of bielebridge
//
//  bielebridge is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  bielebridge is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with bielebridge.  If not, see <http://www.gnu.org/licenses/>.


#include <SDL2/SDL.h>
#include "game.hpp"
#include "error.hpp"
#include "gui_manager.hpp"
#include "settings.hpp"
#include "version.cmake.hpp"

#include <GL/gl.h>
#include <GL/glu.h>

Game::Game(SDL_Renderer* renderer, SDL_Window* window)
	:renderer(renderer), window(window)
{
	guimanager = new GUI_Manager();

	frame_counter = 0;
	timems_last_reset = 0;;
	current_fps = 0;

	// Game status
	exit = false;
}

void Game::loop()
{
	// time since SDL_Init in ms
	Uint32 timems_loop_start;
	Uint32 timems_loop_end;

	// limit frames per second (and cpu load)
	int fps_target = Settings::singleton()->fps_target;
	int ms_between_frames = 1000/fps_target;
	// limit frames per second (and cpu load)
	int delay_time;
	while(!exit)
	{
		timems_loop_start = SDL_GetTicks();
		input();
		update();
		render();
		timems_loop_end = SDL_GetTicks();

		// keep fps target
		fps_target = Settings::singleton()->fps_target;
		ms_between_frames = 1000/fps_target;
		delay_time = ms_between_frames - (timems_loop_end - timems_loop_start);
		if( delay_time > 0 )
		{
			SDL_Delay(delay_time);
		}
		else
		{
			// TODO: framerate is below fps_target
			// give a warning if 20% below fps_target
		}
	}
}

void Game::input()
{
	SDL_Event event;
	mouse_x_ptr = &(Settings::singleton()->mouse_x);
	mouse_y_ptr = &(Settings::singleton()->mouse_y);

	int* window_width_ptr  = &(Settings::singleton()->window_width);
	int* window_height_ptr = &(Settings::singleton()->window_height);

	while(SDL_PollEvent(&event))
	{
		// Track mouse movement
		if (event.type == SDL_MOUSEMOTION)
		{
			SDL_GetMouseState(mouse_x_ptr, mouse_y_ptr);
		}
		// user clicks on the close button, or receive a SIGTERM on POSIX
		if (event.type == SDL_QUIT)
		{
			Settings::singleton()->save();
			exit = true;
		}
		// Exit with 'ESCAPE'
		if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)
		{
			Settings::singleton()->save();
			exit = true;
		}
		// Display help with F1
		if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_F1)
		{
			guimanager->user_pressed_help();
		}
		if (event.type == SDL_KEYDOWN)
		{
			const char* utf8_key = SDL_GetKeyName(event.key.keysym.sym);
			// test if left or right shift
			std::string utf8_string = utf8_key;
			if(utf8_string.compare("Left Shift") == 0 || utf8_string.compare("Right Shift") == 0)
			{
				this->keyboard_input_uppercase = true;
			}

			DEBUG_PRINT("pressed key \'%s\'\n", utf8_key);

			if((this->keyboard_input_uppercase == false) && (utf8_string.size() == 1))
			{
				std::string lowercase_string;
				lowercase_string += static_cast<char>(std::tolower(*utf8_key));
				guimanager->event_keyboard(lowercase_string.c_str());
			}
			else
			{
				guimanager->event_keyboard(utf8_key);
			}
		}
		if (event.type == SDL_KEYUP)
		{
			std::string utf8_string = SDL_GetKeyName(event.key.keysym.sym);
			if(utf8_string.compare("Left Shift") == 0 || utf8_string.compare("Right Shift") == 0)
			{
				this->keyboard_input_uppercase = false;
			}
		}

		// Zoom with mouse wheel
		if (event.type == SDL_MOUSEWHEEL)
		{
			guimanager->event_mouse_wheel(event.wheel.y);
		}
		// Mouse button clicked
		if (event.type == SDL_MOUSEBUTTONDOWN)
		{
			switch (event.button.button)
			{
				// Left mouse button clicked
				case SDL_BUTTON_LEFT:
					Settings::singleton()->mouse_left_down = true;
					guimanager->event_mouse_click(0, 0, *mouse_x_ptr, *mouse_y_ptr);
					break;
				case SDL_BUTTON_RIGHT:
					Settings::singleton()->mouse_right_down = true;
					guimanager->event_mouse_click(2, 0, *mouse_x_ptr, *mouse_y_ptr);
					break;
				default:
					break;
			}
		}
		// Mouse button released
		if (event.type == SDL_MOUSEBUTTONUP)
		{
			switch (event.button.button)
			{
				case SDL_BUTTON_LEFT:
					Settings::singleton()->mouse_left_down = false;
					break;
				case SDL_BUTTON_RIGHT:
					Settings::singleton()->mouse_right_down = false;
					break;
				default:
					break;
			}
		}
		// Window resize -> set new window w/h, recalc button positions
		if( event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED )
		{
			// update window w/h
			SDL_GetWindowSize(window, window_width_ptr, window_height_ptr);
			// if opengl: change Viewport
			if(Settings::singleton()->video_mode == Settings::OPENGL_SDL)
			{
				int window_w = Settings::singleton()->window_width;
				int window_h = Settings::singleton()->window_height;
				glMatrixMode(GL_PROJECTION);
				glLoadIdentity();
				glViewport(0,0,window_w, window_h);
				glOrtho(0,window_w,window_h,0,0,128);
			}
			// tell current GUI about resize (it can recalc button positions, etc)
			guimanager->event_resize();
		}
	}
}

void Game::update()
{
	guimanager->update();
}

// Render objects on screen
void Game::render()
{
	guimanager->draw();

	// display new contend by swapping buffers
	if(Settings::singleton()->video_mode == Settings::OPENGL_SDL)
    	SDL_GL_SwapWindow(window);
	else
		SDL_RenderPresent(renderer);

	////////////////////////////////////////////////////////////////
	// some fps statistics inside window title
	frame_counter++;
	// one sec passed, save fps, display them in window title
	if( timems_last_reset + 1000 < SDL_GetTicks() ) {
		current_fps = frame_counter;
		char* window_title = Settings::singleton()->window_title;
		snprintf(window_title, 100, "%s  --  %d fps",PACKAGE_NAME " " PACKAGE_VERSION , current_fps);
		SDL_SetWindowTitle(window, window_title);
		timems_last_reset = SDL_GetTicks();
		frame_counter = 0;
	}
}

