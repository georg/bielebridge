#!/bin/bash

# build bielebridge

# test if build dir is already there
if [[ -d ./build ]] ; then
	# build dir is present: just call make
	cd build && make && ctest -VV && cd ..
else
	# create build dir and init cmake
	mkdir build && cd build && cmake .. && VERBOSE=1 make -j $(nproc) && cd .. && echo "successfully compiled build/bielebridge"
fi
